#include <sstream>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TokenType.hpp"
#include "../../../Lib/Src/TokenValue.hpp"
#include "../../../Lib/Src/IRCompiler/IRCompiler.hpp"

TEST_CASE("ir_bool_arith", "[IRBoolArith]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("ir_bool_arith_and")
    {
      std::stringstream source;
      source << "true && false";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::BOOL == instrs.at(0)->type);
      REQUIRE(false == instrs.at(0)->value.b);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::BOOL == instrs.at(1)->type);
      REQUIRE(true == instrs.at(1)->value.b);

      REQUIRE(bk::IROpCode::AND == instrs.at(2)->opcode);
    }

  SECTION("ir_bool_arith_and")
    {
      std::stringstream source;
      source << "true && false";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::BOOL == instrs.at(0)->type);
      REQUIRE(false == instrs.at(0)->value.b);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::BOOL == instrs.at(1)->type);
      REQUIRE(true == instrs.at(1)->value.b);

      REQUIRE(bk::IROpCode::AND == instrs.at(2)->opcode);
    }

  SECTION("ir_bool_arith_or")
    {
      std::stringstream source;
      source << "true || false";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::BOOL == instrs.at(0)->type);
      REQUIRE(false == instrs.at(0)->value.b);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::BOOL == instrs.at(1)->type);
      REQUIRE(true == instrs.at(1)->value.b);

      REQUIRE(bk::IROpCode::OR == instrs.at(2)->opcode);
    }

    SECTION("ir_bool_arith_not")
    {
      std::stringstream source;
      source << "!false";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(2 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::BOOL == instrs.at(0)->type);
      REQUIRE(false == instrs.at(0)->value.b);

      REQUIRE(bk::IROpCode::NOT == instrs.at(1)->opcode);
    }
}
