#include <sstream>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TokenType.hpp"
#include "../../../Lib/Src/TokenValue.hpp"
#include "../../../Lib/Src/IRCompiler/IRCompiler.hpp"

TEST_CASE("ir_ident", "[IRIdent]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("just_ident")
    {
      std::stringstream source;
      source << "salut";
      tree = parser(source.str());
      
      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(2 == instrs.size());
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(0)->type);
      REQUIRE("salut" == instrs.at(0)->value.s);

      REQUIRE(bk::IROpCode::LOAD == instrs.at(1)->opcode);
    }
}
