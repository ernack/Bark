#include <sstream>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TokenType.hpp"
#include "../../../Lib/Src/TokenValue.hpp"
#include "../../../Lib/Src/IRCompiler/IRCompiler.hpp"

TEST_CASE("ir_Numbercmp_lt", "[IRCmp]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("ir_cmp_lt_int")
    {
      std::stringstream source;
      source << "18 < 27";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(27 == instrs.at(0)->value.i);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(1)->type);
      REQUIRE(18 == instrs.at(1)->value.i);

      REQUIRE(bk::IROpCode::LT == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_lt_uint")
    {
      std::stringstream source;
      source << "18u < 27u";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(0)->type);
      REQUIRE(27 == instrs.at(0)->value.ui);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(1)->type);
      REQUIRE(18 == instrs.at(1)->value.ui);

      REQUIRE(bk::IROpCode::LT == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_lt_float")
    {
      std::stringstream source;
      source << "3.12 < 5.15";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(0)->type);
      REQUIRE(5.15f == instrs.at(0)->value.f);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(1)->type);
      REQUIRE(3.12f == instrs.at(1)->value.f);

      REQUIRE(bk::IROpCode::LT == instrs.at(2)->opcode);
    }
}

TEST_CASE("ir_Numbercmp_le", "[IRCmp]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("ir_cmp_le_int")
    {
      std::stringstream source;
      source << "18 <= 27";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(27 == instrs.at(0)->value.i);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(1)->type);
      REQUIRE(18 == instrs.at(1)->value.i);

      REQUIRE(bk::IROpCode::LE == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_le_uint")
    {
      std::stringstream source;
      source << "18u <= 27u";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(0)->type);
      REQUIRE(27 == instrs.at(0)->value.ui);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(1)->type);
      REQUIRE(18 == instrs.at(1)->value.ui);

      REQUIRE(bk::IROpCode::LE == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_le_float")
    {
      std::stringstream source;
      source << "3.12 <= 5.15";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(0)->type);
      REQUIRE(5.15f == instrs.at(0)->value.f);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(1)->type);
      REQUIRE(3.12f == instrs.at(1)->value.f);

      REQUIRE(bk::IROpCode::LE == instrs.at(2)->opcode);
    }
}

TEST_CASE("ir_Numbercmp_gt", "[IRCmp]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("ir_cmp_gt_int")
    {
      std::stringstream source;
      source << "18 > 27";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(27 == instrs.at(0)->value.i);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(1)->type);
      REQUIRE(18 == instrs.at(1)->value.i);

      REQUIRE(bk::IROpCode::GT == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_gt_uint")
    {
      std::stringstream source;
      source << "18u > 27u";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(0)->type);
      REQUIRE(27 == instrs.at(0)->value.ui);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(1)->type);
      REQUIRE(18 == instrs.at(1)->value.ui);

      REQUIRE(bk::IROpCode::GT == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_gt_float")
    {
      std::stringstream source;
      source << "3.12 > 5.15";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(0)->type);
      REQUIRE(5.15f == instrs.at(0)->value.f);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(1)->type);
      REQUIRE(3.12f == instrs.at(1)->value.f);

      REQUIRE(bk::IROpCode::GT == instrs.at(2)->opcode);
    }
}

TEST_CASE("ir_Numbercmp_ge", "[IRCmp]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("ir_cmp_ge_int")
    {
      std::stringstream source;
      source << "18 >= 27";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(27 == instrs.at(0)->value.i);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(1)->type);
      REQUIRE(18 == instrs.at(1)->value.i);

      REQUIRE(bk::IROpCode::GE == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_ge_uint")
    {
      std::stringstream source;
      source << "18u >= 27u";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(0)->type);
      REQUIRE(27 == instrs.at(0)->value.ui);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(1)->type);
      REQUIRE(18 == instrs.at(1)->value.ui);

      REQUIRE(bk::IROpCode::GE == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_ge_float")
    {
      std::stringstream source;
      source << "3.12 >= 5.15";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(0)->type);
      REQUIRE(5.15f == instrs.at(0)->value.f);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(1)->type);
      REQUIRE(3.12f == instrs.at(1)->value.f);

      REQUIRE(bk::IROpCode::GE == instrs.at(2)->opcode);
    }
}

TEST_CASE("ir_Numbercmp_eq", "[IRCmp]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("ir_cmp_eq_int")
    {
      std::stringstream source;
      source << "18 == 27";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(27 == instrs.at(0)->value.i);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(1)->type);
      REQUIRE(18 == instrs.at(1)->value.i);

      REQUIRE(bk::IROpCode::EQ == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_eq_uint")
    {
      std::stringstream source;
      source << "18u == 27u";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(0)->type);
      REQUIRE(27 == instrs.at(0)->value.ui);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(1)->type);
      REQUIRE(18 == instrs.at(1)->value.ui);

      REQUIRE(bk::IROpCode::EQ == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_eq_float")
    {
      std::stringstream source;
      source << "3.12 == 5.15";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(0)->type);
      REQUIRE(5.15f == instrs.at(0)->value.f);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(1)->type);
      REQUIRE(3.12f == instrs.at(1)->value.f);

      REQUIRE(bk::IROpCode::EQ == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_eq_bool")
    {
      std::stringstream source;
      source << "true == false";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::BOOL == instrs.at(0)->type);
      REQUIRE(false == instrs.at(0)->value.b);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::BOOL == instrs.at(1)->type);
      REQUIRE(true == instrs.at(1)->value.b);

      REQUIRE(bk::IROpCode::EQ == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_eq_string")
    {
      std::stringstream source;
      source << "\"coucou\" == \"coco\"";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(0)->type);
      REQUIRE("coco" == instrs.at(0)->value.s);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("coucou" == instrs.at(1)->value.s);

      REQUIRE(bk::IROpCode::EQ == instrs.at(2)->opcode);
    }
}


TEST_CASE("ir_Numbercmp_ne", "[IRCmp]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("ir_cmp_ne_int")
    {
      std::stringstream source;
      source << "18 != 27";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(27 == instrs.at(0)->value.i);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(1)->type);
      REQUIRE(18 == instrs.at(1)->value.i);

      REQUIRE(bk::IROpCode::NE == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_ne_uint")
    {
      std::stringstream source;
      source << "18u != 27u";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(0)->type);
      REQUIRE(27 == instrs.at(0)->value.ui);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(1)->type);
      REQUIRE(18 == instrs.at(1)->value.ui);

      REQUIRE(bk::IROpCode::NE == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_ne_float")
    {
      std::stringstream source;
      source << "3.12 != 5.15";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(0)->type);
      REQUIRE(5.15f == instrs.at(0)->value.f);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(1)->type);
      REQUIRE(3.12f == instrs.at(1)->value.f);

      REQUIRE(bk::IROpCode::NE == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_ne_bool")
    {
      std::stringstream source;
      source << "true != false";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::BOOL == instrs.at(0)->type);
      REQUIRE(false == instrs.at(0)->value.b);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::BOOL == instrs.at(1)->type);
      REQUIRE(true == instrs.at(1)->value.b);

      REQUIRE(bk::IROpCode::NE == instrs.at(2)->opcode);
    }

  SECTION("ir_cmp_ne_string")
    {
      std::stringstream source;
      source << "\"coucou\"!= \"coco\"";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(0)->type);
      REQUIRE("coco" == instrs.at(0)->value.s);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("coucou" == instrs.at(1)->value.s);

      REQUIRE(bk::IROpCode::NE == instrs.at(2)->opcode);
    }
}
