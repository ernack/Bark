#include <sstream>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TokenType.hpp"
#include "../../../Lib/Src/TokenValue.hpp"
#include "../../../Lib/Src/IRCompiler/IRCompiler.hpp"

TEST_CASE("ir_vardecl_int", "[IRVarDecl]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("int_explicit")
    {
      std::stringstream source;
      source << "x : int = 42";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(42 == instrs.at(0)->value.i);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("x" == instrs.at(1)->value.s);
      
      REQUIRE(bk::IROpCode::STORE == instrs.at(2)->opcode);
    }

  SECTION("int_implicit")
    {
      std::stringstream source;
      source << "hello := 39";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(39 == instrs.at(0)->value.i);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("hello" == instrs.at(1)->value.s);
      
      REQUIRE(bk::IROpCode::STORE == instrs.at(2)->opcode);
    }
}

TEST_CASE("ir_vardecl_uint", "[IRVarDecl]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("uint_explicit")
    {
      std::stringstream source;
      source << "x : uint = 42u";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(0)->type);
      REQUIRE(42 == instrs.at(0)->value.ui);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("x" == instrs.at(1)->value.s);
      
      REQUIRE(bk::IROpCode::STORE == instrs.at(2)->opcode);
    }

  SECTION("uint_implicit")
    {
      std::stringstream source;
      source << "hello := 39u";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(0)->type);
      REQUIRE(39 == instrs.at(0)->value.ui);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("hello" == instrs.at(1)->value.s);
      
      REQUIRE(bk::IROpCode::STORE == instrs.at(2)->opcode);
    }
}

TEST_CASE("ir_vardecl_float", "[IRVarDecl]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("float_explicit")
    {
      std::stringstream source;
      source << "x : int = 42.2";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(0)->type);
      REQUIRE(42.2f == instrs.at(0)->value.f);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("x" == instrs.at(1)->value.s);
      
      REQUIRE(bk::IROpCode::STORE == instrs.at(2)->opcode);
    }

  SECTION("float_implicit")
    {
      std::stringstream source;
      source << "hello := 39.3";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(0)->type);
      REQUIRE(39.3f == instrs.at(0)->value.f);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("hello" == instrs.at(1)->value.s);
      
      REQUIRE(bk::IROpCode::STORE == instrs.at(2)->opcode);
    }
}

TEST_CASE("ir_vardecl_bool", "[IRVarDecl]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("bool_explicit")
    {
      std::stringstream source;
      source << "x : bool = true";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::BOOL == instrs.at(0)->type);
      REQUIRE(true == instrs.at(0)->value.b);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("x" == instrs.at(1)->value.s);
      
      REQUIRE(bk::IROpCode::STORE == instrs.at(2)->opcode);
    }

  SECTION("bool_implicit")
    {
      std::stringstream source;
      source << "hello := false";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::BOOL == instrs.at(0)->type);
      REQUIRE(false == instrs.at(0)->value.b);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("hello" == instrs.at(1)->value.s);
      
      REQUIRE(bk::IROpCode::STORE == instrs.at(2)->opcode);
    }
}

TEST_CASE("ir_vardecl_string", "[IRVarDecl]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("string_explicit")
    {
      std::stringstream source;
      source << "x : string = \"bouh\"";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(0)->type);
      REQUIRE("bouh" == instrs.at(0)->value.s);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("x" == instrs.at(1)->value.s);
      
      REQUIRE(bk::IROpCode::STORE == instrs.at(2)->opcode);
    }

  SECTION("string_implicit")
    {
      std::stringstream source;
      source << "hello := \"koala\"";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(0)->type);
      REQUIRE("koala" == instrs.at(0)->value.s);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("hello" == instrs.at(1)->value.s);
      
      REQUIRE(bk::IROpCode::STORE == instrs.at(2)->opcode);
    }
}
