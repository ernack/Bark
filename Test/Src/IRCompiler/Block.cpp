#include <sstream>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TokenType.hpp"
#include "../../../Lib/Src/TokenValue.hpp"
#include "../../../Lib/Src/IRCompiler/IRCompiler.hpp"

TEST_CASE("ir_block", "[IRBlock]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("simple")
    {
      std::stringstream source;
      source << "{ x : int = 42; y : float = 3.2 }";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(6 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(42 == instrs.at(0)->value.i);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("x" == instrs.at(1)->value.s);
      
      REQUIRE(bk::IROpCode::STORE == instrs.at(2)->opcode);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(3)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(3)->type);
      REQUIRE(3.2f == instrs.at(3)->value.f);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(4)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(4)->type);
      REQUIRE("y" == instrs.at(4)->value.s);
      
      REQUIRE(bk::IROpCode::STORE == instrs.at(5)->opcode);
    }
}
