#include <sstream>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TokenType.hpp"
#include "../../../Lib/Src/TokenValue.hpp"
#include "../../../Lib/Src/IRCompiler/IRCompiler.hpp"

TEST_CASE("ir_assign", "[IRAssign]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("assign_int")
    {
      std::stringstream source;
      source << "variable = 4";
      tree = parser(source.str());
      
      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(4 == instrs.at(0)->value.i);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("variable" == instrs.at(1)->value.s);

      REQUIRE(bk::IROpCode::ASSIGN == instrs.at(2)->opcode);
    }

  SECTION("assign_uint")
    {
      std::stringstream source;
      source << "variable = 32u";
      tree = parser(source.str());
      
      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(0)->type);
      REQUIRE(32 == instrs.at(0)->value.ui);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("variable" == instrs.at(1)->value.s);

      REQUIRE(bk::IROpCode::ASSIGN == instrs.at(2)->opcode);
    }

  SECTION("assign_float")
    {
      std::stringstream source;
      source << "variable = 4.2";
      tree = parser(source.str());
      
      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(0)->type);
      REQUIRE(4.2f == instrs.at(0)->value.f);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("variable" == instrs.at(1)->value.s);

      REQUIRE(bk::IROpCode::ASSIGN == instrs.at(2)->opcode);
    }

  SECTION("assign_bool")
    {
      std::stringstream source;
      source << "variable = false";
      tree = parser(source.str());
      
      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::BOOL == instrs.at(0)->type);
      REQUIRE(false == instrs.at(0)->value.b);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("variable" == instrs.at(1)->value.s);

      REQUIRE(bk::IROpCode::ASSIGN == instrs.at(2)->opcode);
    }

  SECTION("assign_string")
    {
      std::stringstream source;
      source << "variable = \"coucou\"";
      tree = parser(source.str());
      
      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(0)->type);
      REQUIRE("coucou" == instrs.at(0)->value.s);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(1)->type);
      REQUIRE("variable" == instrs.at(1)->value.s);

      REQUIRE(bk::IROpCode::ASSIGN == instrs.at(2)->opcode);
    }
}
