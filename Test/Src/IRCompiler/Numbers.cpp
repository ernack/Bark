#include <sstream>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TokenType.hpp"
#include "../../../Lib/Src/TokenValue.hpp"
#include "../../../Lib/Src/IRCompiler/IRCompiler.hpp"

TEST_CASE("ir_number", "[IRNumbers]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("ir_number_int")
    {
      std::stringstream source;
      source << "5";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(1 == instrs.size());
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(5 == instrs.at(0)->value.i);
    }

  SECTION("ir_number_uint")
    {
      std::stringstream source;
      source << "29u";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(1 == instrs.size());
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(0)->type);
      REQUIRE(29 == instrs.at(0)->value.ui);
    }

  SECTION("ir_number_float")
    {
      std::stringstream source;
      source << "7.34";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(1 == instrs.size());
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(0)->type);
      REQUIRE(7.34f == instrs.at(0)->value.f);
    }

  SECTION("ir_number_bool")
    {
      std::stringstream source;
      source << "false";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(1 == instrs.size());
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::BOOL == instrs.at(0)->type);
      REQUIRE(false == instrs.at(0)->value.b);
    }
  
  SECTION("ir_number_string")
    {
      std::stringstream source;
      source << "\"pizza !\"";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(1 == instrs.size());
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::STRING == instrs.at(0)->type);
      REQUIRE("pizza !" == instrs.at(0)->value.s);
    }
}

TEST_CASE("ir_number_arith_add", "[IRNumbers]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("ir_number_arith_add_int")
    {
      std::stringstream source;
      source << "5+ 3";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());

      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(3 == instrs.at(0)->value.i);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(1)->type);
      REQUIRE(5 == instrs.at(1)->value.i);

      REQUIRE(bk::IROpCode::ADD == instrs.at(2)->opcode);
    }

  SECTION("ir_number_arith_add_uint")
    {
      std::stringstream source;
      source << "5u  +3u";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(0)->type);
      REQUIRE(3 == instrs.at(0)->value.ui);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(1)->type);
      REQUIRE(5 == instrs.at(1)->value.ui);

      REQUIRE(bk::IROpCode::ADD == instrs.at(2)->opcode);
    }

    SECTION("ir_number_arith_add_float")
    {
      std::stringstream source;
      source << "5.7+ 3.2";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(0)->type);
      REQUIRE(3.2f == instrs.at(0)->value.f);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(1)->type);
      REQUIRE(5.7f == instrs.at(1)->value.f);

      REQUIRE(bk::IROpCode::ADD == instrs.at(2)->opcode);
    }
}


TEST_CASE("ir_number_arith_sub", "[IRNumbers]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("ir_number_arith_sub_int")
    {
      std::stringstream source;
      source << "5 - 3";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(3 == instrs.at(0)->value.i);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(1)->type);
      REQUIRE(5 == instrs.at(1)->value.i);

      REQUIRE(bk::IROpCode::SUB == instrs.at(2)->opcode);
    }

  SECTION("ir_number_arith_sub_uint")
    {
      std::stringstream source;
      source << "5u  -3u";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(0)->type);
      REQUIRE(3 == instrs.at(0)->value.ui);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(1)->type);
      REQUIRE(5 == instrs.at(1)->value.ui);

      REQUIRE(bk::IROpCode::SUB == instrs.at(2)->opcode);
    }

    SECTION("ir_number_arith_sub_float")
    {
      std::stringstream source;
      source << "5.7- 3.2";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(0)->type);
      REQUIRE(3.2f == instrs.at(0)->value.f);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(1)->type);
      REQUIRE(5.7f == instrs.at(1)->value.f);

      REQUIRE(bk::IROpCode::SUB == instrs.at(2)->opcode);
    }
}

TEST_CASE("ir_number_arith_mul", "[IRNumbers]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("ir_number_arith_mul_int")
    {
      std::stringstream source;
      source << "5* 3";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(3 == instrs.at(0)->value.i);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(1)->type);
      REQUIRE(5 == instrs.at(1)->value.i);

      REQUIRE(bk::IROpCode::MUL == instrs.at(2)->opcode);
    }

  SECTION("ir_number_arith_mul_uint")
    {
      std::stringstream source;
      source << "5u  *3u";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(0)->type);
      REQUIRE(3 == instrs.at(0)->value.ui);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(1)->type);
      REQUIRE(5 == instrs.at(1)->value.ui);

      REQUIRE(bk::IROpCode::MUL == instrs.at(2)->opcode);
    }

    SECTION("ir_number_arith_mul_float")
    {
      std::stringstream source;
      source << "5.7* 3.2";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(0)->type);
      REQUIRE(3.2f == instrs.at(0)->value.f);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(1)->type);
      REQUIRE(5.7f == instrs.at(1)->value.f);

      REQUIRE(bk::IROpCode::MUL == instrs.at(2)->opcode);
    }
}

TEST_CASE("ir_number_arith_div", "[IRNumbers]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("ir_number_arith_div_int")
    {
      std::stringstream source;
      source << "5/ 3";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(3 == instrs.at(0)->value.i);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(1)->type);
      REQUIRE(5 == instrs.at(1)->value.i);

      REQUIRE(bk::IROpCode::DIV == instrs.at(2)->opcode);
    }

  SECTION("ir_number_arith_div_uint")
    {
      std::stringstream source;
      source << "5u  /3u";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(0)->type);
      REQUIRE(3 == instrs.at(0)->value.ui);
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(1)->type);
      REQUIRE(5 == instrs.at(1)->value.ui);

      REQUIRE(bk::IROpCode::DIV == instrs.at(2)->opcode);
    }

    SECTION("ir_number_arith_div_float")
    {
      std::stringstream source;
      source << "5.7 / 3.2";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(0)->type);
      REQUIRE(3.2f == instrs.at(0)->value.f);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::FLOAT == instrs.at(1)->type);
      REQUIRE(5.7f == instrs.at(1)->value.f);

      REQUIRE(bk::IROpCode::DIV == instrs.at(2)->opcode);

    }
}

TEST_CASE("ir_number_arith_mod", "[IRNumbers]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> tree;
  bk::IRCompiler compiler;
  
  SECTION("ir_number_arith_mod_int")
    {
      std::stringstream source;
      source << "5% 3";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(0)->type);
      REQUIRE(3 == instrs.at(0)->value.i);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::INT == instrs.at(1)->type);
      REQUIRE(5 == instrs.at(1)->value.i);
      
      REQUIRE(bk::IROpCode::MOD == instrs.at(2)->opcode);
    }

  SECTION("ir_number_arith_mod_uint")
    {
      std::stringstream source;
      source << "5u  %3u";
      tree = parser(source.str());

      bk::IRInstrs instrs = compiler(*tree);

      REQUIRE(3 == instrs.size());
      
      REQUIRE(bk::IROpCode::PUSH == instrs.at(0)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(0)->type);
      REQUIRE(3 == instrs.at(0)->value.ui);

      REQUIRE(bk::IROpCode::PUSH == instrs.at(1)->opcode);
      REQUIRE(bk::TokenType::UINT == instrs.at(1)->type);
      REQUIRE(5 == instrs.at(1)->value.ui);

      REQUIRE(bk::IROpCode::MOD == instrs.at(2)->opcode);
    }
}

