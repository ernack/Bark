#include <catch.hpp>
#include "../../Lib/Src/Function.hpp"
#include "../../Lib/Src/TokenType.hpp"
#include "../../Lib/Src/Lexer.hpp"
#include "../../Lib/Src/Parser.hpp"
#include "../../Lib/Src/TreeNode.hpp"
#include "../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("function_empty", "[Function]")
{
  bk::Environment env;
  bk::Function fct {env};

  REQUIRE("()" == fct.string());
}

TEST_CASE("function_one_param", "[Function]")
{
  bk::Environment env;
  bk::Function fct {env};

  bk::FunctionParam param;
  param.name = "i";
  param.type = bk::TokenType::INT;
  fct.add_param(param);
  
  REQUIRE("(i:int)" == fct.string());
}

TEST_CASE("function_more_params", "[Function]")
{
  bk::Environment env;
  bk::Function fct {env};

  {
    bk::FunctionParam param;
    param.name = "i";
    param.type = bk::TokenType::INT;
    fct.add_param(param);
  }

  {
    bk::FunctionParam param;
    param.name = "j";
    param.type = bk::TokenType::UINT;
    fct.add_param(param);
  }

  {
    bk::FunctionParam param;
    param.name = "k";
    param.type = bk::TokenType::FLOAT;
    fct.add_param(param);
  }

  {
    bk::FunctionParam param;
    param.name = "l";
    param.type = bk::TokenType::BOOL;
    fct.add_param(param);
  }


  {
    bk::FunctionParam param;
    param.name = "m";
    param.type = bk::TokenType::STRING;
    fct.add_param(param);
  }

  REQUIRE("(i:int,j:uint,k:float,l:bool,m:string)" == fct.string());
}

TEST_CASE("function_just_return", "[Function]")
{
  bk::Environment env;
  bk::Function fct {env};

  SECTION("function_just_return_int")
    {
      fct.set_return_type(bk::TokenType::INT);  
      REQUIRE("():int" == fct.string());
    }

  SECTION("function_just_return_uint")
    {
      fct.set_return_type(bk::TokenType::UINT);  
      REQUIRE("():uint" == fct.string());
    }

  SECTION("function_just_return_float")
    {
      fct.set_return_type(bk::TokenType::FLOAT);  
      REQUIRE("():float" == fct.string());
    }

  SECTION("function_just_return_bool")
    {
      fct.set_return_type(bk::TokenType::BOOL);  
      REQUIRE("():bool" == fct.string());
    }

  SECTION("function_just_return_string")
    {
      fct.set_return_type(bk::TokenType::STRING);  
      REQUIRE("():string" == fct.string());
    }
}


TEST_CASE("function_params_and_return", "[Function]")
{
  bk::Environment env;
  bk::Function fct {env};

  {
    bk::FunctionParam param;
    param.name = "i";
    param.type = bk::TokenType::INT;
    fct.add_param(param);
  }

  {
    bk::FunctionParam param;
    param.name = "j";
    param.type = bk::TokenType::FLOAT;
    fct.add_param(param);
  }
  
  SECTION("function_just_return_int")
    {
      fct.set_return_type(bk::TokenType::INT);  
      REQUIRE("(i:int,j:float):int" == fct.string());
    }

  SECTION("function_just_return_uint")
    {
      fct.set_return_type(bk::TokenType::UINT);  
      REQUIRE("(i:int,j:float):uint" == fct.string());
    }

  SECTION("function_just_return_float")
    {
      fct.set_return_type(bk::TokenType::FLOAT);  
      REQUIRE("(i:int,j:float):float" == fct.string());
    }

  SECTION("function_just_return_bool")
    {
      fct.set_return_type(bk::TokenType::BOOL);  
      REQUIRE("(i:int,j:float):bool" == fct.string());
    }

  SECTION("function_just_return_string")
    {
      fct.set_return_type(bk::TokenType::STRING);  
      REQUIRE("(i:int,j:float):string" == fct.string());
    }
}

TEST_CASE("function_type_string", "[Function]")
{
  bk::Environment env;
  bk::Function fct {env};

  fct.set_return_type(bk::TokenType::UINT);
  {
    bk::FunctionParam param;
    param.name = "i";
    param.type = bk::TokenType::INT;
    fct.add_param(param);
  }

  {
    bk::FunctionParam param;
    param.name = "j";
    param.type = bk::TokenType::UINT;
    fct.add_param(param);
  }

  {
    bk::FunctionParam param;
    param.name = "k";
    param.type = bk::TokenType::FLOAT;
    fct.add_param(param);
  }

  {
    bk::FunctionParam param;
    param.name = "l";
    param.type = bk::TokenType::BOOL;
    fct.add_param(param);
  }


  {
    bk::FunctionParam param;
    param.name = "m";
    param.type = bk::TokenType::STRING;
    fct.add_param(param);
  }

  REQUIRE("(int,uint,float,bool,string)uint" == fct.type_string());
}

TEST_CASE("function_type_string_no_return", "[Function]")
{
  bk::Environment env;
  bk::Function fct {env};

  {
    bk::FunctionParam param;
    param.name = "i";
    param.type = bk::TokenType::INT;
    fct.add_param(param);
  }

  {
    bk::FunctionParam param;
    param.name = "j";
    param.type = bk::TokenType::UINT;
    fct.add_param(param);
  }

  {
    bk::FunctionParam param;
    param.name = "k";
    param.type = bk::TokenType::FLOAT;
    fct.add_param(param);
  }

  {
    bk::FunctionParam param;
    param.name = "l";
    param.type = bk::TokenType::BOOL;
    fct.add_param(param);
  }


  {
    bk::FunctionParam param;
    param.name = "m";
    param.type = bk::TokenType::STRING;
    fct.add_param(param);
  }

  REQUIRE("(int,uint,float,bool,string)" == fct.type_string());
}

TEST_CASE("function_type_string_on_param_return", "[Function]")
{
  bk::Environment env;
  bk::Function fct {env};

  fct.set_return_type(bk::TokenType::STRING);
  {
    bk::FunctionParam param;
    param.name = "i";
    param.type = bk::TokenType::UINT;
    fct.add_param(param);
  }

  REQUIRE("(uint)string" == fct.type_string());
}

TEST_CASE("function_type_string_on_param_no_return", "[Function]")
{
  bk::Environment env;
  bk::Function fct {env};

  {
    bk::FunctionParam param;
    param.name = "i";
    param.type = bk::TokenType::UINT;
    fct.add_param(param);
  }

  REQUIRE("(uint)" == fct.type_string());
}

TEST_CASE("function_type_string_no_param_no_return", "[Function]")
{
  bk::Environment env;
  bk::Function fct {env};

  REQUIRE("()" == fct.type_string());
}

TEST_CASE("function_body_one_expr", "[Function]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  auto root = parser("x : int = 4;");
  
  bk::Environment env;
  bk::Function fct {env};
  fct.set_body(std::move(root));
  
  REQUIRE("VAR_DECL(IDENT(x),TYPE(int),INT(4))" == fct.body_string());
}

TEST_CASE("function_body_more_expr", "[Function]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  auto root = parser("x : int = 4; x = x + 1; ");
  
  bk::Environment env;
  bk::Function fct {env};
  fct.set_body(std::move(root));
  
  REQUIRE("BARK(VAR_DECL(IDENT(x),TYPE(int),INT(4)),ASSIGN(IDENT(x),ADD(IDENT(x),INT(1))))" == fct.body_string());
}

TEST_CASE("function_body_real_return_type", "[Function]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("function_body_real_return_type_int")
    {
      auto root = parser("{ return 5; }");  
      bk::Environment env;
      bk::Function fct {env};
      fct.set_body(std::move(root));  
      REQUIRE(bk::TokenType::INT == *fct.real_return_type());
    }

  SECTION("function_body_real_return_type_uint")
    {
      auto root = parser("{ return 5u; }");  
      bk::Environment env;
      bk::Function fct {env};
      fct.set_body(std::move(root));  
      REQUIRE(bk::TokenType::UINT == *fct.real_return_type());
    }

  SECTION("function_body_real_return_type_float")
    {
      auto root = parser("{ return 3.14; }");  
      bk::Environment env;
      bk::Function fct {env};
      fct.set_body(std::move(root));  
      REQUIRE(bk::TokenType::FLOAT == *fct.real_return_type());
    }

  SECTION("function_body_real_return_type_bool")
    {
      auto root = parser("{ return true; }");  
      bk::Environment env;
      bk::Function fct {env};
      fct.set_body(std::move(root));  
      REQUIRE(bk::TokenType::BOOL == *fct.real_return_type());
    }

  SECTION("function_body_real_return_type_string")
    {
      auto root = parser("{ return \"coucou\"; }");  
      bk::Environment env;
      bk::Function fct {env};
      fct.set_body(std::move(root));  
      REQUIRE(bk::TokenType::STRING == *fct.real_return_type());
    }
}
