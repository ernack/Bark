#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/Token.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TypeController/TypeController.hpp"
#include "../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("typecontroller_nominals_cases", "[TypeControllerNumbersArith]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> root;  
  bk::Environment env;
  bk::TypeController type_controller { env };
  
  SECTION("tc_int")
    {      
      root = parser("5 + 9 - 9 * 2 / 4");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("tc_uint")
    {      
      root = parser("5u + 9u - 9u * 2u / 4u");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("tc_float")
    {      
      root = parser("5.0 + 9.0 - 9.0 * 2.0 / 4.0");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("tc_bool")
    {      
      root = parser("!true && false || true");      
      REQUIRE(true == type_controller.check(*root));
    }
}


TEST_CASE("typecontroller_errors_cases", "[TypeControllerNumbersArith]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> root;  
  bk::Environment env;
  bk::TypeController type_controller { env };

  
  SECTION("tc_err_add_int")
    {
      // uint
      root = parser("5u + 4");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("9 + 7u");      
      REQUIRE(false == type_controller.check(*root));

      // float
      root = parser("5 + 0.2");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("50.7 + 11");      
      REQUIRE(false == type_controller.check(*root));

      // string
      root = parser("5 + \"toto\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"hola\" + 6");      
      REQUIRE(false == type_controller.check(*root));

      // bool
      root = parser("false + 7");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("5 + true");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_err_add_uint")
    {
      // int
      root = parser("5 + 4u");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("9u + 7");      
      REQUIRE(false == type_controller.check(*root));

      // float
      root = parser("5u + 0.2");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("50.7 + 11u");      
      REQUIRE(false == type_controller.check(*root));

      // string
      root = parser("5u + \"toto\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"hola\" + 6u");      
      REQUIRE(false == type_controller.check(*root));

      // bool
      root = parser("false + 7u");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("5u + true");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_err_add_float")
    {
      // int
      root = parser("5 + 4.5");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("9.3 + 7");      
      REQUIRE(false == type_controller.check(*root));

      // uint
      root = parser("5.8 + 0u");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("50u + 11.3");      
      REQUIRE(false == type_controller.check(*root));

      // string
      root = parser("5.9 + \"toto\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"hola\" + 6.9");      
      REQUIRE(false == type_controller.check(*root));

      // bool
      root = parser("false + 7.11");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("3.14 + true");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_err_add_string")
    {
      // int
      root = parser("5 + \"aze\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"fds\" + 7");      
      REQUIRE(false == type_controller.check(*root));

      // uint
      root = parser("5.8 + \"aze\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"aze eza\" + 11.3");      
      REQUIRE(false == type_controller.check(*root));

      // float
      root = parser("5.9 + \"toto\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"hola\" + 6.9");      
      REQUIRE(false == type_controller.check(*root));

      // bool
      root = parser("false + \"azeeza\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"plom\" + true");      
      REQUIRE(false == type_controller.check(*root));
    }
  
  SECTION("tc_err_add_bool")
    {
      // int
      root = parser("5 + false");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("true + 7");      
      REQUIRE(false == type_controller.check(*root));

      // uint
      root = parser("5.8 + false");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("true + 11.3");      
      REQUIRE(false == type_controller.check(*root));

      // float
      root = parser("5.9 + false");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("true + 6.9");      
      REQUIRE(false == type_controller.check(*root));

      // string
      root = parser("true + \"azeeazeza\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"plqsdom\" + false");      
      REQUIRE(false == type_controller.check(*root));
    }

    SECTION("tc_err_bool_arith")
    {
      // int
      root = parser("false || 7");      
      REQUIRE(false == type_controller.check(*root));

      // uint
      root = parser("5u && false");      
      REQUIRE(false == type_controller.check(*root));

      // float
      root = parser("!3.2");      
      REQUIRE(false == type_controller.check(*root));

      // string
      root = parser("false || \"coucou\"");      
      REQUIRE(false == type_controller.check(*root));
    }

    SECTION("tc_err_sub_int")
    {
      // uint
      root = parser("5u - 4");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("9 - 7u");      
      REQUIRE(false == type_controller.check(*root));

      // float
      root = parser("5 - 0.2");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("50.7 - 11");      
      REQUIRE(false == type_controller.check(*root));

      // string
      root = parser("5 - \"toto\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"hola\" - 6");      
      REQUIRE(false == type_controller.check(*root));

      // bool
      root = parser("false - 7");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("5 - true");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_err_sub_uint")
    {
      // int
      root = parser("5 - 4u");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("9u - 7");      
      REQUIRE(false == type_controller.check(*root));

      // float
      root = parser("5u - 0.2");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("50.7 - 11u");      
      REQUIRE(false == type_controller.check(*root));

      // string
      root = parser("5u - \"toto\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"hola\" - 6u");      
      REQUIRE(false == type_controller.check(*root));

      // bool
      root = parser("false - 7u");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("5u - true");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_err_sub_float")
    {
      // int
      root = parser("5 - 4.5");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("9.3 - 7");      
      REQUIRE(false == type_controller.check(*root));

      // uint
      root = parser("5.8 - 0u");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("50u - 11.3");      
      REQUIRE(false == type_controller.check(*root));

      // string
      root = parser("5.9 - \"toto\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"hola\" - 6.9");      
      REQUIRE(false == type_controller.check(*root));

      // bool
      root = parser("false - 7.11");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("3.14 - true");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_err_sub_string")
    {
      // int
      root = parser("5 - \"aze\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"fds\" - 7");      
      REQUIRE(false == type_controller.check(*root));

      // uint
      root = parser("5.8 - \"aze\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"aze eza\" - 11.3");      
      REQUIRE(false == type_controller.check(*root));

      // float
      root = parser("5.9 - \"toto\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"hola\" - 6.9");      
      REQUIRE(false == type_controller.check(*root));

      // bool
      root = parser("false - \"azeeza\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"plom\" - true");      
      REQUIRE(false == type_controller.check(*root));
    }
  
  SECTION("tc_err_sub_bool")
    {
      // int
      root = parser("5 - false");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("true - 7");      
      REQUIRE(false == type_controller.check(*root));

      // uint
      root = parser("5.8 - false");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("true - 11.3");      
      REQUIRE(false == type_controller.check(*root));

      // float
      root = parser("5.9 - false");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("true - 6.9");      
      REQUIRE(false == type_controller.check(*root));

      // string
      root = parser("true - \"azeeazeza\"");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("\"plqsdom\" - false");      
      REQUIRE(false == type_controller.check(*root));
    }

}
