#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/Token.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TypeController/TypeController.hpp"
#include "../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("typecontroller_block", "[TypeControllerBlock]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> root;  
  bk::Environment env;
  bk::TypeController type_controller { env };
  
  SECTION("typecontroller_block_int")
    {      
      root = parser("x0 : int = { 1.0; \"coucou\"; 5};");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("x1 : int = { 1; \"coucou\"; 2u};");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_block_uint")
    {      
      root = parser("x0 : uint = { 1.0; \"coucou\"; 3u};");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("x1 : uint = { 1; \"coucou\"; \"a\"};");      
      REQUIRE(false == type_controller.check(*root));
    }
  
  SECTION("typecontroller_block_float")
    {      
      root = parser("x0 : float = { 1.0; \"coucou\"; 5.2};");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("x1 : float = { 1; \"coucou\"; 2};");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_block_bool")
    {      
      root = parser("x0 : bool = { 1.0; \"coucou\"; true};");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("x1 : bool = { 1; \"coucou\"; 3.12};");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_block_string")
    {      
      root = parser("x0 : string = { 1.0; \"coucou\"; \"e\"};");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("x1 : string = { 1; \"coucou\"; 2u};");      
      REQUIRE(false == type_controller.check(*root));
    }
}
