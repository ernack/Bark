#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/Token.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TypeController/TypeController.hpp"
#include "../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("typecontroller_assign", "[TypeControllerAssign]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> root;  
  bk::Environment env;
  bk::TypeController type_controller { env };
  
  SECTION("typecontroller_assign_int")
    {
      env.declare("x", bk::TokenType::INT, bk::token_value(4));
      
      root = parser("x = 9");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("x = 9u");  
      REQUIRE(false == type_controller.check(*root));

      root = parser("x = 8.4");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x = false");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x = \"camomille\"");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_assign_uint")
    {
      env.declare("x", bk::TokenType::UINT, bk::token_value(4U));
      
      root = parser("x = 9");  
      REQUIRE(false == type_controller.check(*root));

      root = parser("x = 8.4");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x = false");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x = \"camomille\"");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_assign_float")
    {
      env.declare("x", bk::TokenType::FLOAT, bk::token_value(4.2f));
      
      root = parser("x = 9.7");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("x = 9u");  
      REQUIRE(false == type_controller.check(*root));

      root = parser("x = 8");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x = false");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x = \"camomille\"");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_assign_bool")
    {
      env.declare("x", bk::TokenType::BOOL, bk::token_value(true));
      
      root = parser("x = true");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("x = 9u");  
      REQUIRE(false == type_controller.check(*root));

      root = parser("x = 8.4");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x = 7");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x = \"camomille\"");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_assign_string")
    {
      env.declare("x", bk::TokenType::STRING, bk::token_value(std::string("coucou")));
      
      root = parser("x = \"coco\"");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("x = 9u");  
      REQUIRE(false == type_controller.check(*root));

      root = parser("x = 8.4");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x = false");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x = 3");      
      REQUIRE(false == type_controller.check(*root));
    }
}
