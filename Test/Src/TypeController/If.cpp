#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/Token.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TypeController/TypeController.hpp"
#include "../../../Lib/Src/Environment/Environment.hpp"
#include "../../../Lib/Src/Function.hpp"

TEST_CASE("typecontroller_if", "[TypeIf]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> root;
  bk::Environment env;
  bk::TypeController type_controller { env };
  
  SECTION("typecontroller_if_ok")
    {
      root = parser("if true { return 0; } else { return 1; }");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("typecontroller_if_ko_int")
    {
      root = parser("if 3 { return 0; } else { return 1; }");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_if_ko_uint")
    {
      root = parser("if 3u { return 0; } else { return 1; }");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_if_ko_float")
    {
      root = parser("if 3.14 { return 0; } else { return 1; }");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_if_ko_string")
    {
      root = parser("if \"pascontent\" { return 0; } else { return 1; }");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_if_error_")
    {
      root = parser("if error name { return 0; } else { return 1; }");

      REQUIRE(true == type_controller.check(*root));
    }
}
