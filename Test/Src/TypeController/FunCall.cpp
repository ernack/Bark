#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/Token.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TypeController/TypeController.hpp"
#include "../../../Lib/Src/Environment/Environment.hpp"
#include "../../../Lib/Src/Function.hpp"

TEST_CASE("typecontroller_funcall", "[TypeFunCall]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> root;
  bk::Environment env;
  bk::TypeController type_controller { env };
  
  SECTION("typecontroller_funcall_params")
    {
      root = parser("x : (int,float)int = (i : int, f : float) int { return i * 2; }; x(1, 2.3)");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("typecontroller_funcall_bad_param_0")
    {
      root =
	parser("x : (int,float)int = (i : int, f : float) int { return i * 2; }; x(false, 2.3)");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_funcall_bad_params_1")
    {
      root =
	parser("x : (int,float)int = (i : int, f : float) int { return i * 2; }; x(1, false)");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_funcall_return")
    {
      root =
	parser("x : (int,float)int = (i : int, f : float) int"
	       "{ return i * 2; };y : int =  x(1, 2.3);");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("typecontroller_funcall_bad_return")
    {
      root =
	parser("x : (int,float)int = (i : int, f : float) int"
	       "{ return i * 2; };y : float =  x(1, 2.3);");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_funcall_bug")
    {      
      root = parser("f : (int)int = (n : int) int {return n*4;};"
		    "var : int = 4;"
		    "f(var)");

      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("typecontroller_funcall_bug1")
    {       
      root = parser("double : (int)int = (n : int) int { return n * 2; }; double(double(4))");

      REQUIRE(true == type_controller.check(*root));
    }
  
  SECTION("typecontroller_funcall_bug2")
    {       
      std::stringstream ss;
      ss << "twice : (int,(uint)int)int = (n : int, f : (uint)int) int { return f(f(n));};";
      ss << "double : (int)int = (n : int) { return n * 2; };";
      ss << "twice(4, double);";
      root = parser(ss.str());

      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_funcall_bug3")
    {       
      std::stringstream ss;
      ss << "{ f : (int)int = (i : int) int { return i * 2; }; };" << std::endl;
      ss << "f" << std::endl;
      root = parser(ss.str());
      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_funcall_const_write")
    {       
      std::stringstream ss;
      ss << "f : (&&int) = (n : &&int) { n = 7; }" << std::endl;

      root = parser(ss.str());

      REQUIRE(false == type_controller.check(*root));
    }

    SECTION("typecontroller_funcall_const_read")
    {       
      std::stringstream ss;
      ss << "f : (&&int)int = (n : &&int) int { return n + 4; }" << std::endl;

      root = parser(ss.str());
      
      REQUIRE(true == type_controller.check(*root));
    }

    SECTION("typecontroller_funcall_ref")
      {       
	std::stringstream ss;
	ss << "doubler : (&int) = (i : &int) { i = 5; };";
	ss << "a : int = 4;";
	ss << "doubler(a);";
	ss << "a";

      root = parser(ss.str());
      
      REQUIRE(true == type_controller.check(*root));
    }

    SECTION("typecontroller_funcall_factorial")
      {       
	std::stringstream ss;
	ss << "fact : (int)int = (n : int) int {";
	ss << "if n == 0 { return 1; }; return n * fact(n - 1);}";
	ss << "fact(5);";
	  
      root = parser(ss.str());
      
      REQUIRE(true == type_controller.check(*root));
      }

    SECTION("typecontroller_nested")
      {       
	std::stringstream ss;
	
	ss << "f : (int)int = (n : int) int {\n";  
	ss << "doubler : (&int) = (i : &int) {\n";
	ss << "i = 4;\n";
	ss << "};\n";  
	ss << "return doubler(n);\n";
	ss << "};\n";
	ss << "f(8)";
	  
	root = parser(ss.str());
      
	REQUIRE(true == type_controller.check(*root));
      }

    SECTION("typecontroller_bug4")
      {       
	std::stringstream ss;
	
	ss << "f : ()int = () int { return 3; }\n";  
	ss << "a : int = f();\n";
	ss << "a";
	root = parser(ss.str());
      
	REQUIRE(true == type_controller.check(*root));
      }

    SECTION("typecontroller_error_int")
      {       
	std::stringstream ss;
	
	ss << "f := (i : int) int { error 5; }" << std::endl;  
	root = parser(ss.str());
	
	REQUIRE(true == type_controller.check(*root));
      }

    SECTION("typecontroller_error_int_with_float_return")
      {       
	std::stringstream ss;
	
	ss << "f := (i : int) float { error 5; }" << std::endl;  
	root = parser(ss.str());
	
	REQUIRE(true == type_controller.check(*root));
      }

    SECTION("typecontroller_error_int_with_float_return_used")
      {       
	std::stringstream ss;
	
	ss << "f := (i : int) float { error 5; }" << std::endl;
	ss << "x : float = f(42);" << std::endl;  
	root = parser(ss.str());
	
	REQUIRE(true == type_controller.check(*root));
      }

    SECTION("typecontroller_error_ko_uint")
      {       
	std::stringstream ss;
	
	ss << "f := (i : int) int { error 5u; }" << std::endl;  
	root = parser(ss.str());
      
	REQUIRE(false == type_controller.check(*root));
      }

    SECTION("typecontroller_error_ko_float")
      {       
	std::stringstream ss;
	
	ss << "f := (i : int) int { error 5.3; }" << std::endl;  
	root = parser(ss.str());
      
	REQUIRE(false == type_controller.check(*root));
      }

    SECTION("typecontroller_error_ko_bool")
      {       
	std::stringstream ss;
	
	ss << "f := (i : int) int { error true; }" << std::endl;  
	root = parser(ss.str());
      
	REQUIRE(false == type_controller.check(*root));
      }

    SECTION("typecontroller_error_ko_string")
      {       
	std::stringstream ss;
	
	ss << "f := (i : int) int { error \"coucou\"; }" << std::endl;  
	root = parser(ss.str());
      
	REQUIRE(false == type_controller.check(*root));
      }

    SECTION("typecontroller_error_bug_5")
      {       
	std::stringstream ss;
	
	ss << "f := (i : int) int { return i * 2; };" << std::endl;
	ss << "n := f(5);" << std::endl;  
	root = parser(ss.str());
      
	REQUIRE(true == type_controller.check(*root));
      }

    SECTION("typecontroller_mutually_recursive_functions")
      {       
	std::stringstream ss;
	ss << "odd := (n : int) bool {" << std::endl;
	ss << "if n == 0 { return false; }" << std::endl;
	ss << "return even(n - 1);" << std::endl;
	ss << "};" << std::endl;
	ss << "even := (n : int) bool {" << std::endl;
	ss << "if n == 0 { return true; }" << std::endl;
	ss << "return odd(n - 1);" << std::endl;
	ss << "};" << std::endl;

	root = parser(ss.str());
      
	REQUIRE(true == type_controller.check(*root));
      }

    SECTION("typecontroller_bug_6")
      {       
	std::stringstream ss;

	ss << "change_func := (f : &(int)int) {" << std::endl;
	ss << "f = (i : int) int {" << std::endl;
	ss << "return i * 2;" << std::endl;
	ss << "}" << std::endl;
	ss << "};" << std::endl;
	ss << "triple := (n : int) int {" << std::endl;
	ss << "return n * 3;" << std::endl;
	ss << "};" << std::endl;
	ss << "change_func(triple);" << std::endl;
	
	root = parser(ss.str());
      
	REQUIRE(true == type_controller.check(*root));
      }
    
    SECTION("typecontroller_function_function_cref")
      {       
	std::stringstream ss;

	ss << "change_func := (f : &&(int)int) {" << std::endl;
	ss << "f = (i : int) int {" << std::endl;
	ss << "return i * 2;" << std::endl;
	ss << "}" << std::endl;
	ss << "};" << std::endl;
	ss << "triple := (n : int) int {" << std::endl;
	ss << "return n * 3;" << std::endl;
	ss << "};" << std::endl;
	ss << "change_func(triple);" << std::endl;
	
	root = parser(ss.str());
      
	REQUIRE(false == type_controller.check(*root));
      }

}
