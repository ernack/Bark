#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/Token.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TypeController/TypeController.hpp"
#include "../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("typecontroller_var_decl", "[TypeControllerVarDecl]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> root;  
  bk::Environment env;
  bk::TypeController type_controller { env };

  SECTION("tc_var_decl_expr")
    {      
      root = parser("x : int = 4 + 7");
      REQUIRE(true == type_controller.check(*root));

      root = parser("x : int = 3 * 6");      
      REQUIRE(true == type_controller.check(*root));
    }
    
  SECTION("tc_var_decl_int")
    {      
      root = parser("x : int = 4"); // int      
      REQUIRE(true == type_controller.check(*root));

      root = parser("x : int = 4u"); // uint
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : int = 4.2"); // float
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : int = \"hello\""); // string
      REQUIRE(false == type_controller.check(*root));
      
      root = parser("x : int = false"); // bool
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_var_decl_uint")
    {      
      root = parser("x : uint = 4u"); // uint      
      REQUIRE(true == type_controller.check(*root));

      root = parser("x : uint = 4"); // int
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : uint = 4.2"); // float
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : uint = \"hello\""); // string
      REQUIRE(false == type_controller.check(*root));
      
      root = parser("x : uint = false"); // bool
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_var_decl_float")
    {      
      root = parser("x : float = 4.35"); // float      
      REQUIRE(true == type_controller.check(*root));

      root = parser("x : float = 4u"); // uint
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : float = 4"); // int
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : float = \"hello\""); // string
      REQUIRE(false == type_controller.check(*root));
      
      root = parser("x : float = false"); // bool
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_var_decl_string")
    {      
      root = parser("x : string = \"jojo\""); // string
      REQUIRE(true == type_controller.check(*root));

      root = parser("x : string = 4u"); // uint
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : string = 4.2"); // float
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : string = 78"); // int
      REQUIRE(false == type_controller.check(*root));
      
      root = parser("x : string = false"); // bool
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_var_decl_bool")
    {      
      root = parser("x : bool = true"); // bool
      REQUIRE(true == type_controller.check(*root));

      root = parser("x : bool = 4u"); // uint
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : bool = 4.2"); // float
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : bool = \"hello\""); // string
      REQUIRE(false == type_controller.check(*root));
      
      root = parser("x : bool = 7"); // int
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_var_decl_op_bug_0")
    {      
      root = parser("az : int = 4 + 7 ; az");
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("tc_var_decl_op_0")
    {      
      root = parser("x0 : float = 3.4 ; y0 : int = x0 + 3;");
      REQUIRE(false == type_controller.check(*root));
    }
  SECTION("tc_var_decl_op_1")
    {      

      root = parser("x0 : int = 3 ; y0 : int = x0 + 3;");
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("tc_var_decl_op_2")
    {      
      root = parser("x0 : float = 3.4 ; y0 : int = 5 + x0;");
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_var_decl_op_3")
    {            
      root = parser("x0 : int = 3 ; y0 : int = 5 + x0;");
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("tc_var_decl_op_4")
    {      

      root = parser("x1 : int = 3; y1 : uint = 3u + x1;");
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_var_decl_op_5")
    {      
      root = parser("x2 : int = 3; y2 : float = 5.2 + x2;");
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_var_decl_bug_1")
    {      
      root = parser("nb : int = 43; \nif nb < 5 { 4; }");
      REQUIRE(true == type_controller.check(*root));
    }
}
