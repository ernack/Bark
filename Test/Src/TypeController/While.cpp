#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/Token.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TypeController/TypeController.hpp"
#include "../../../Lib/Src/Environment/Environment.hpp"
#include "../../../Lib/Src/Function.hpp"

TEST_CASE("typecontroller_while", "[TypeWhile]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> root;
  bk::Environment env;
  bk::TypeController type_controller { env };
  
  SECTION("typecontroller_while_bool")
    {
      root = parser("i : int = 0; while i < 10  { i = i + 1; };");      
      REQUIRE(true == type_controller.check(*root));
    }

    
  SECTION("typecontroller_while_error_int")
    {
      root = parser("i : int = 0; while 10  { i = i + 1; };");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_while_error_uint")
    {
      root = parser("i : int = 0; while 10u  { i = i + 1; };");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_while_error_float")
    {
      root = parser("i : int = 0; while 7.15  { i = i + 1; };");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_while_error_string")
    {
      root = parser("i : int = 0; while \"coucou\"  { i = i + 1; };");      
      REQUIRE(false == type_controller.check(*root));
    }
	  
  SECTION("typecontroller_while_empty")
    {
      root = parser("i : int = 0; while i < 10  { };");

      REQUIRE(false == type_controller.check(*root));
    }
}
