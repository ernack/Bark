#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/Token.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TypeController/TypeController.hpp"
#include "../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("typecontroller_ok_cmp", "[TypeControllerCmp]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> root;  
  bk::Environment env;
  bk::TypeController type_controller { env };
  
  SECTION("tc_cmp_ok_lt")
    {      
      root = parser("5 < 7");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("5u < 7u");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("3.7 < 2.9");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("tc_cmp_ok_le")
    {      
      root = parser("5 <= 7");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("5u <= 7u");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("3.7 <= 2.9");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("tc_cmp_ok_gt")
    {      
      root = parser("5 > 7");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("5u > 7u");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("3.7 > 2.9");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("tc_cmp_ok_ge")
    {      
      root = parser("5 >= 7");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("5u >= 7u");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("3.7 >= 2.9");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("tc_cmp_ok_eq")
    {      
      root = parser("5 == 7");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("5u == 7u");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("3.7 == 2.9");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("tc_cmp_ok_neq")
    {      
      root = parser("5 != 7");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("5u != 7u");      
      REQUIRE(true == type_controller.check(*root));

      root = parser("3.7 != 2.9");      
      REQUIRE(true == type_controller.check(*root));
    }
}

TEST_CASE("typecontroller_ko_cmp", "[TypeControllerCmp]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> root;  
  bk::Environment env;
  bk::TypeController type_controller { env };
  
  SECTION("tc_cmp_ko_lt")
    {
      // int uint
      root = parser("5u < 7");      
      REQUIRE(false == type_controller.check(*root));

      // int float
      root = parser("5 < 7.0");      
      REQUIRE(false == type_controller.check(*root));

      // uint float
      root = parser("   2.9 < 5u");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_cmp_ko_le")
    {
      // int uint
      root = parser("5u <=   7");      
      REQUIRE(false == type_controller.check(*root));

      // int float
      root = parser("   5 <= 7.0 ");      
      REQUIRE(false == type_controller.check(*root));

      // uint float
      root = parser(" 3u <= 2.9");      
      REQUIRE(false == type_controller.check(*root));
    }
  
  SECTION("tc_cmp_ko_gt")
    {
      // int uint
      root = parser("5u > 7 ");      
      REQUIRE(false == type_controller.check(*root));

      // int float
      root = parser("5> 7.0");      
      REQUIRE(false == type_controller.check(*root));

      // uint float
      root = parser("3u  >2.9");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_cmp_ko_ge")
    {
      // int uint
      root = parser("5u>= 7");      
      REQUIRE(false == type_controller.check(*root));

      // int float
      root = parser("                5            >=  7.0  ");      
      REQUIRE(false == type_controller.check(*root));

      // uint float
      root = parser("3u  >=2.9");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_cmp_ko_eq")
    {
      // int uint
      root = parser(" 5u == 7");      
      REQUIRE(false == type_controller.check(*root));

      // int float
      root = parser("  5 == 7.0");      
      REQUIRE(false == type_controller.check(*root));

      // uint float
      root = parser("      3u == 2.9 ");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("tc_cmp_ko_neq")
    {
      // int uint
      root = parser("5u!= 7");      
      REQUIRE(false == type_controller.check(*root));

      // int float
      root = parser("5 != 7.0");      
      REQUIRE(false == type_controller.check(*root));

      // uint float
      root = parser("3u != 2.9");      
      REQUIRE(false == type_controller.check(*root));
    }
}
