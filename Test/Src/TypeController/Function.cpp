#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/Token.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TypeController/TypeController.hpp"
#include "../../../Lib/Src/Environment/Environment.hpp"
#include "../../../Lib/Src/Function.hpp"

TEST_CASE("typecontroller_function", "[TypeFunction]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> root;
  bk::Environment env;
  bk::TypeController type_controller { env };
  
  SECTION("typecontroller_function_simple")
    {
      root = parser("x : (int)int = (i : int) int { return i * 2; };");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("typecontroller_function_param_mismatch")
    {
      root = parser("x : (uint)int = (i : int) int { return i * 2; };");

      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_function_return_bool")
    {
      root = parser("x : (uint)bool = (i : int) int { return false; };");

      REQUIRE(false == type_controller.check(*root));
    }
    
  SECTION("typecontroller_function_return_mismatch")
    {
      root = parser("x : (uint)bool = (i : int) int { return 2; };");

      REQUIRE(false == type_controller.check(*root));
    }

    SECTION("typecontroller_function_all_params_ok")
    {
      root = parser("x : (uint,int,string)bool = (i : uint, j : int, k : string) bool { return true; };");

      REQUIRE(true == type_controller.check(*root));
    }

    SECTION("typecontroller_function_one_param_ko")
      {
	root = parser("x : (uint,int,string)bool = (i : uint, j : uint, k : string) bool { return true; };");

	REQUIRE(false == type_controller.check(*root));
      }

    SECTION("typecontroller_function_bug_0")
      {
	root = parser("x : (int,float)int = (i : int, f : float) int { return i; };");	
	REQUIRE(true == type_controller.check(*root));
      }

    SECTION("typecontroller_function_bug_no_return")
      {
	root = parser("x : (int) = (i : int) {  };");	
	REQUIRE(true == type_controller.check(*root));
      }
    
}

TEST_CASE("typecontroller_function_wrong_type", "[TypeFunction]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> root;
  bk::Environment env;
  bk::TypeController type_controller { env };
  
  SECTION("typecontroller_function_wrong_int")
    {
      root = parser("x : (int)int = (i : int) uint { return i * 2; };");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : (int)int = (i : int) float { return i * 2; };");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : (int)int = (i : int) bool { return i * 2; };");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : (int)int = (i : int) string { return i * 2; };");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_function_wrong_uint")
    {
      root = parser("x : (int)uint = (i : int) int { return i * 2u; };");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : (int)uint = (i : int) float { return i * 2u; };");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : (int)uint = (i : int) bool { return i * 2u; };");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : (int)uint = (i : int) string { return i * 2u; };");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("typecontroller_function_wrong_float")
    {
      root = parser("x : (int)float = (i : int) uint { return i * 2.0; };");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : (int)float = (i : int) float { return i * 2.0; };");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : (int)float = (i : int) bool { return i * 2.0; };");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : (int)float = (i : int) string { return i * 2.0; };");      
      REQUIRE(false == type_controller.check(*root));
    }

    SECTION("typecontroller_function_wrong_bool")
    {
      root = parser("x : (int)bool = (i : int) uint { return true; };");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : (int)bool = (i : int) float { return true; };");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : (int)bool = (i : int) int { return true; };");      
      REQUIRE(false == type_controller.check(*root));

      root = parser("x : (int)bool = (i : int) string { return true; };");      
      REQUIRE(false == type_controller.check(*root));
    }

    SECTION("typecontroller_function_wrong_string")
      {
	root = parser("x : (int)string = (i : int) uint { return \"coucou\"; };");      
	REQUIRE(false == type_controller.check(*root));

	root = parser("x : (int)string = (i : int) float { return \"coucou\"; };");      
	REQUIRE(false == type_controller.check(*root));

	root = parser("x : (int)string = (i : int) int { return \"coucou\"; };");      
	REQUIRE(false == type_controller.check(*root));

	root = parser("x : (int)string = (i : int) bool { return \"coucou\"; };");      
	REQUIRE(false == type_controller.check(*root));
      }

    SECTION("typecontroller_function_no_output_type")
      {
	root = parser("x : (int)int = (i : int) { return i + 4; };");      
	REQUIRE(false == type_controller.check(*root));
      }

    SECTION("typecontroller_double_return_good_type")
      {
	root = parser("x := (i : int) int { return 5; return 7; };");      
	REQUIRE(true == type_controller.check(*root));
      }

    SECTION("typecontroller_double_return_first_wrong_type")
      {
	root = parser("x : (int)int = (i : int) int { return true; return 7; };");      
	REQUIRE(false == type_controller.check(*root));
      }

    SECTION("typecontroller_double_return_second_wrong_type")
      {
	root = parser("x : (int)int = (i : int) int { return 5; return false; };");      
	REQUIRE(false == type_controller.check(*root));
      }

    SECTION("typecontroller_return_outside_function")
      {
	root = parser("return true;");      
	REQUIRE(false == type_controller.check(*root));
      }
}
