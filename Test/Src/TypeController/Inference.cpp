#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/Token.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TypeController/TypeController.hpp"
#include "../../../Lib/Src/Environment/Environment.hpp"
#include "../../../Lib/Src/Function.hpp"

TEST_CASE("typecontroller_inference", "[TypeInference]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> root;
  bk::Environment env;
  bk::TypeController type_controller { env };
  
  SECTION("typecontroller_inference_int")
    {
      root = parser("x := 45;");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("typecontroller_inference_uint")
    {
      root = parser("x := 45u;");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("typecontroller_inference_bool")
    {
      root = parser("x := true;");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("typecontroller_inference_string")
    {
      root = parser("x := \"coucou\";");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("typecontroller_inference_bad_int")
    {
      root = parser("x := 14; y : int = x;");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("typecontroller_inference_bad_uint")
    {
      root = parser("x := 14u; y : uint = x;");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("typecontroller_inference_bug_0")
    {
      root = parser("f := (n : int) int { return n * 2; };");

      REQUIRE(true == type_controller.check(*root));
    }
}
