#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/Token.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/TypeController/TypeController.hpp"
#include "../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("typecontroller_ident", "[TypeControllerIdent]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::unique_ptr<bk::TreeNode> root;
  bk::Environment env;
  bk::TypeController type_controller { env };
  
  SECTION("type_controller_ident_add_int_int")
    {
      env.declare("thisissparta", bk::TokenType::INT, bk::token_value(76));
      root = parser("thisissparta + 1");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("type_controller_ident_add_int_uint")
    {
      env.declare("thisissparta", bk::TokenType::INT, bk::token_value(76));
      root = parser("thisissparta + 7u");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("type_controller_ident_add_int_float")
    {
      env.declare("thisissparta", bk::TokenType::INT, bk::token_value(76));
      root = parser("thisissparta + 34.7");      
      REQUIRE(false == type_controller.check(*root));
    }

    SECTION("type_controller_ident_add_uint_uint")
    {
      env.declare("thisissparta", bk::TokenType::UINT, bk::token_value(34U));
      root = parser("thisissparta + 2u");      
      REQUIRE(true == type_controller.check(*root));
    }

    SECTION("type_controller_ident_add_uint_float")
    {
      env.declare("thisissparta", bk::TokenType::UINT, bk::token_value(34U));
      root = parser("thisissparta + 218.56");      
      REQUIRE(false == type_controller.check(*root));
    }

    SECTION("type_controller_ident_add_float_float")
      {
	env.declare("thisissparta", bk::TokenType::FLOAT, bk::token_value(27.4f));
	root = parser("thisissparta + 32.7");      
	REQUIRE(true == type_controller.check(*root));
      }

    ////

    SECTION("type_controller_ident_sub_int_int")
    {
      env.declare("thisissparta", bk::TokenType::INT, bk::token_value(76));
      root = parser("thisissparta - 1");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("type_controller_ident_sub_int_uint")
    {
      env.declare("thisissparta", bk::TokenType::INT, bk::token_value(76));
      root = parser("thisissparta - 7u");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("type_controller_ident_sub_int_float")
    {
      env.declare("thisissparta", bk::TokenType::INT, bk::token_value(76));
      root = parser("thisissparta - 34.7");      
      REQUIRE(false == type_controller.check(*root));
    }

    SECTION("type_controller_ident_sub_uint_uint")
    {
      env.declare("thisissparta", bk::TokenType::UINT, bk::token_value(34U));
      root = parser("thisissparta - 2u");      
      REQUIRE(true == type_controller.check(*root));
    }

    SECTION("type_controller_ident_sub_uint_float")
    {
      env.declare("thisissparta", bk::TokenType::UINT, bk::token_value(34U));
      root = parser("thisissparta - 218.56");      
      REQUIRE(false == type_controller.check(*root));
    }

    SECTION("type_controller_ident_sub_float_float")
      {
	env.declare("thisissparta", bk::TokenType::FLOAT, bk::token_value(27.4f));
	root = parser("thisissparta - 32.7");      
	REQUIRE(true == type_controller.check(*root));
      }

    ////
    SECTION("type_controller_ident_mul_int_int")
    {
      env.declare("thisissparta", bk::TokenType::INT, bk::token_value(76));
      root = parser("thisissparta * 1");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("type_controller_ident_mul_int_uint")
    {
      env.declare("thisissparta", bk::TokenType::INT, bk::token_value(76));
      root = parser("thisissparta * 7u");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("type_controller_ident_mul_int_float")
    {
      env.declare("thisissparta", bk::TokenType::INT, bk::token_value(76));
      root = parser("thisissparta * 34.7");      
      REQUIRE(false == type_controller.check(*root));
    }

    SECTION("type_controller_ident_mul_uint_uint")
    {
      env.declare("thisissparta", bk::TokenType::UINT, bk::token_value(34U));
      root = parser("thisissparta * 2u");      
      REQUIRE(true == type_controller.check(*root));
    }

    SECTION("type_controller_ident_mul_uint_float")
    {
      env.declare("thisissparta", bk::TokenType::UINT, bk::token_value(34U));
      root = parser("thisissparta * 218.56");      
      REQUIRE(false == type_controller.check(*root));
    }

    SECTION("type_controller_ident_mul_float_float")
      {
	env.declare("thisissparta", bk::TokenType::FLOAT, bk::token_value(27.4f));
	root = parser("thisissparta * 32.7");      
	REQUIRE(true == type_controller.check(*root));
      }

    ////
    SECTION("type_controller_ident_div_int_int")
    {
      env.declare("thisissparta", bk::TokenType::INT, bk::token_value(76));
      root = parser("thisissparta / 1");      
      REQUIRE(true == type_controller.check(*root));
    }

  SECTION("type_controller_ident_div_int_uint")
    {
      env.declare("thisissparta", bk::TokenType::INT, bk::token_value(76));
      root = parser("thisissparta / 7u");      
      REQUIRE(false == type_controller.check(*root));
    }

  SECTION("type_controller_ident_div_int_float")
    {
      env.declare("thisissparta", bk::TokenType::INT, bk::token_value(76));
      root = parser("thisissparta / 34.7");      
      REQUIRE(false == type_controller.check(*root));
    }

    SECTION("type_controller_ident_div_uint_uint")
    {
      env.declare("thisissparta", bk::TokenType::UINT, bk::token_value(34U));
      root = parser("thisissparta / 2u");      
      REQUIRE(true == type_controller.check(*root));
    }

    SECTION("type_controller_ident_div_uint_float")
    {
      env.declare("thisissparta", bk::TokenType::UINT, bk::token_value(34U));
      root = parser("thisissparta / 218.56");      
      REQUIRE(false == type_controller.check(*root));
    }

    SECTION("type_controller_ident_div_float_float")
      {
	env.declare("thisissparta", bk::TokenType::FLOAT, bk::token_value(27.4f));
	root = parser("thisissparta / 32.7");      
	REQUIRE(true == type_controller.check(*root));
      }

    ////
    SECTION("type_controller_ident_mod_int_int")
      {
	env.declare("thisissparta", bk::TokenType::INT, bk::token_value(76));
	root = parser("thisissparta % 1");      
	REQUIRE(true == type_controller.check(*root));
      }

    SECTION("type_controller_ident_mod_int_uint")
      {
	env.declare("thisissparta", bk::TokenType::INT, bk::token_value(76));
	root = parser("thisissparta % 7u");      
	REQUIRE(false == type_controller.check(*root));
      }


    SECTION("type_controller_ident_mod_uint_uint")
      {
	env.declare("thisissparta", bk::TokenType::UINT, bk::token_value(34U));
	root = parser("thisissparta % 2u");      
	REQUIRE(true == type_controller.check(*root));
      }

}
