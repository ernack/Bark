#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/Evaluator/IREvaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("irevaluator_cmp_bug", "[.IREvaluatorCmp]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::IREvaluator evaluator { env };

  std::stringstream ss;

  ss << "  __plus_ou_moins := (nb_mystere : int) {" << std::endl;
  ss << "  nb : int = 0;" << std::endl;
  ss << "  nb = 4;" << std::endl;
  ss << "  if nb < nb_mystere {" << std::endl;
  ss << "      4;" << std::endl;
  ss << "  }" << std::endl;
  ss << "}" << std::endl;

  ss << "__plus_ou_moins(5);" << std::endl;

  
  tree = parser(ss.str());      
  REQUIRE_NOTHROW( evaluator(*tree) );
}
