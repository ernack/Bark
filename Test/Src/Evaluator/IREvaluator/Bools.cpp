#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/Evaluator/IREvaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("irevaluator_bools", "[IREvaluatorBools]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;
  bk::Environment env;
  bk::IREvaluator evaluator { env };

  SECTION("evaluator_bool_and")
    {
      tree = parser("true && true");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser("false && true");      
      REQUIRE(false == evaluator(*tree).b );

      tree = parser("true && false");      
      REQUIRE(false == evaluator(*tree).b );

      tree = parser("false && false");      
      REQUIRE(false == evaluator(*tree).b );
    }
  
  SECTION("evaluator_bool_or")
    {
      tree = parser("true || true");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser("false || true");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser("true || false");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser("false || false");      
      REQUIRE(false == evaluator(*tree).b );
    }

    SECTION("evaluator_bool_not")
      {
	tree = parser("!true");      
	REQUIRE(false == evaluator(*tree).b );

	tree = parser("!false");      
	REQUIRE(true == evaluator(*tree).b );
      }
}
