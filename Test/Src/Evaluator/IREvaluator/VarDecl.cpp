#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/Evaluator/IREvaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("irevaluator_var_decl", "[IREvaluatorVarDecl]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::IREvaluator evaluator { env };

    SECTION("evaluator_var_decl_ops")
    {
      REQUIRE(0 == env.size());
      
      tree = parser("x : int = 6 * 7");      
      REQUIRE(42 == evaluator(*tree).i );
      REQUIRE(1 == env.size());
    }
    
  SECTION("evaluator_var_decl_types")
    {
      REQUIRE(0 == env.size());
      
      tree = parser("x : int = 42");      
      REQUIRE(42 == evaluator(*tree).i );
      REQUIRE(1 == env.size());
      
      tree = parser("y : bool = false");      
      REQUIRE(false == evaluator(*tree).b );
      REQUIRE(2 == env.size());
      
      tree = parser("z : uint = 39u");      
      REQUIRE(39 == evaluator(*tree).ui );
      REQUIRE(3 == env.size());
      
      tree = parser("w : float = 3.1415");      
      REQUIRE(3.1415f == evaluator(*tree).f );
      REQUIRE(4 == env.size());
      
      tree = parser("t : int = \"popo\"");      
      REQUIRE("popo" == evaluator(*tree).s );
      REQUIRE(5 == env.size());
    }
}
