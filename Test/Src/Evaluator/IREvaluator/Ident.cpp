#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/Evaluator/IREvaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("irevaluator_ident", "[IREvaluatorIdent]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::IREvaluator evaluator { env };

  SECTION("evaluator_ident_int")
    {
      env.declare("hello", bk::TokenType::INT, bk::token_value(42));      
      tree = parser("hello");      
      REQUIRE(42 == evaluator(*tree).i);
    }

    SECTION("evaluator_ident_uint")
    {
      env.declare("whello", bk::TokenType::UINT, bk::token_value(27U));      
      tree = parser("whello");      
      REQUIRE(27U == evaluator(*tree).ui);
    }

    SECTION("evaluator_ident_float")
    {
      env.declare("awhello", bk::TokenType::FLOAT, bk::token_value(3.5f));      
      tree = parser("awhello");      
      REQUIRE(3.5f == evaluator(*tree).f);
    }

    SECTION("evaluator_ident_bool_true")
      {
	env.declare("qzwhello", bk::TokenType::BOOL, bk::token_value(true));      
	tree = parser("qzwhello");      
	REQUIRE(true == evaluator(*tree).b);
      }

    SECTION("evaluator_ident_bool_false")
    {
      env.declare("zwhello", bk::TokenType::BOOL, bk::token_value(false));      
      tree = parser("zwhello");      
      REQUIRE(false == evaluator(*tree).b);
    }

    SECTION("evaluator_ident_string")
    {
      env.declare("gwhello", bk::TokenType::STRING, bk::token_value(std::string("salut")));      
      tree = parser("gwhello");
      REQUIRE("salut" == evaluator(*tree).s);
    }

    SECTION("evaluator_ident_not_found")
    {
      tree = parser("erebor");      
      REQUIRE_THROWS(evaluator(*tree).s);

    }
}
