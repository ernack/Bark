#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/Evaluator/IREvaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("irevaluator_cmp", "[IREvaluatorCmp]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::IREvaluator evaluator { env };

  SECTION("evaluator_lt")
    {      
      tree = parser(" 5 < 3 ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2 < 12 ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 12 < 12 ");      
      REQUIRE(false == evaluator(*tree).b );

      tree = parser(" 5u < 3u ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2u <12u ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 12u < 12u ");      
      REQUIRE(false == evaluator(*tree).b );


      tree = parser(" 5.7 < 3.2 ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2.12 < 12.4 ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 12.7 < 12.7 ");      
      REQUIRE(false == evaluator(*tree).b );

      tree = parser(" 12.7 +1.1 < 12.7 ");      
      REQUIRE(false == evaluator(*tree).b );

    }

  SECTION("evaluator_le")
    {      
      tree = parser(" 5 <= 3 ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2 <= 12 ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 12 <= 12 ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" 5u <= 3u ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2u <= 12u ");
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 12u <= 12u ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" 5.7 <= 3.2 ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2.12 <= 12.4 ");
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 2.12 <= 2.12 ");      
      REQUIRE(true == evaluator(*tree).b );
    }

  SECTION("evaluator_gt")
    {      
      tree = parser(" 5 > 3 ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 2 > 12 ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 12 > 12 ");      
      REQUIRE(false == evaluator(*tree).b );

      tree = parser(" 5u > 3u ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 2u > 12u ");
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 12u > 12u ");      
      REQUIRE(false == evaluator(*tree).b );

      tree = parser(" 5.7 > 3.2 ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 2.12 > 12.4 ");
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2.12 > 2.12 ");      
      REQUIRE(false == evaluator(*tree).b );
    }

  SECTION("evaluator_ge")
    {      
      tree = parser(" 5 >= 3 ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 2 >= 12 ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 12 >= 12 ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" 5u >= 3u ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 2u >= 12u ");
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 12u >= 12u ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" 5.7 >= 3.2 ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 2.12 >= 12.4 ");
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2.12 >= 2.12 ");      
      REQUIRE(true == evaluator(*tree).b );
    }

  SECTION("evaluator_ge")
    {      
      tree = parser(" 5 == 3 ");      
      REQUIRE(false == evaluator(*tree).b );      
      tree = parser(" 5 == 5 ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" 5u == 3u ");      
      REQUIRE(false == evaluator(*tree).b );      
      tree = parser(" 5u == 5u ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" 5.7 == 5.2 ");      
      REQUIRE(false == evaluator(*tree).b );      
      tree = parser(" 5.7 == 5.7 ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" \"abcd\" == \"abc\" ");      
      REQUIRE(false == evaluator(*tree).b );      
      tree = parser(" \"abcd\" == \"abcd\" ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" true == false ");      
      REQUIRE(false == evaluator(*tree).b );      
      tree = parser(" true == true ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" false == false ");      
      REQUIRE(true == evaluator(*tree).b );
    }
}
