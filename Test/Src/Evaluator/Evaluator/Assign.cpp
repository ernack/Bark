#include <iostream>
#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/Evaluator/Evaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("evaluator_assign", "[EvaluatorAssign]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::Evaluator evaluator { env };

  SECTION("evaluator_assign_int")
    {
      env.declare("var", bk::TokenType::INT, bk::token_value(4));
      tree = parser("var = 22");
      evaluator(*tree);
      REQUIRE(22 == env.get_value("var")->i);
    }
}
