#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/Function.hpp"
#include "../../../../Lib/Src/Evaluator/Evaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("evaluator_function", "[EvaluatorFunction]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::Evaluator evaluator { env };

  SECTION("evaluator_function_value")
    {      
      tree = parser("x : (int)int = (i : int) { return i * 2; };");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(nullptr != value.function);
      REQUIRE("(i:int):int" == value.function->string() );
    }

    SECTION("evaluator_function_ref_input")
    {      
      tree = parser("x : (&int)int = (i : &int) { return i * 2; };");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(nullptr != value.function);
      REQUIRE("(&int)int" == value.function->type_string() );
    }
}
