#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TokenType.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/Function.hpp"
#include "../../../../Lib/Src/Evaluator/Evaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("evaluator_funcall", "[EvaluatorFunCall]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::Evaluator evaluator { env };

  SECTION("evaluator_funcall_doubler")
    {      
      tree = parser("f : (int)int = (i : int) { return i * 2; }; f(4)");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(8 == value.i);
    }

  SECTION("evaluator_funcall_return")
    {      
      tree = parser("f : (int)int = (i : int) { return 42; return i * 2; }; f(4)");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(42 == value.i);
    }

  SECTION("evaluator_funcall_factorial")
    {      
      tree = parser("f : (int)int = (n : int) { (n == 0 && (return 1));"
		    " return n * f(n - 1);}; f(5)");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(120 == value.i);
    }

  SECTION("evaluator_funcall_bug0")
    {      
      tree = parser("f : (int)int = (n : int) {return n*4;};"
		    "var : int = 4;"
		    "f(var)");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(16 == value.i);
    }

  SECTION("evaluator_funcall_bug1")
    {
      std::stringstream ss;
      ss << "twice : (int,(int)int)int = (n : int, f : (int)int) { return f(f(n));};";
      ss << "double : (int)int = (n : int) { return n * 2; };";
      ss << "twice(4, double);";
      tree = parser(ss.str());

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(16 == value.i);
    }

  SECTION("evaluator_funcall_ref")
    {
      std::stringstream ss;
      ss << "doubler : (&int) = (n : &int) { n = 42; };";
      ss << "i : int = 2; doubler(i); i";
      tree = parser(ss.str());

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(42 == value.i);
    }

    SECTION("evaluator_funcall_ref_with_get_val")
    {
      std::stringstream ss;
      ss << "doubler : (&int) = (n : &int) { n = n * 2; };";
      ss << "i : int = 2; doubler(i); i";
      tree = parser(ss.str());

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(4 == value.i);
    }

    SECTION("evaluator_funcall_ref_with_get_val2")
    {
      std::stringstream ss;
      ss << "doubler : (&int) = (i : &int) { i = 5; };";
      ss << "a : int = 4;";
      ss << "doubler(a);";
      ss << "a";
      tree = parser(ss.str());

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(5 == value.i);
    }

    SECTION("evaluator_funcall_ref_const_identity")
      {
	std::stringstream ss;
	ss << "doubler : (&&int) = (i : &&int) { return i; };";
	ss << "a : int = 4;";
	ss << "doubler(a);";
	tree = parser(ss.str());

	bk::TokenValue value = evaluator(*tree);
	REQUIRE(4 == value.i);
      }

    
    SECTION("evaluator_funcall_function_ref")
      {
	std::stringstream ss;
	ss << "change_func := (f : &(int)int) {" << std::endl;
	ss << "f = (i : int) int {" << std::endl;
	ss << "return i * 2;" << std::endl;
	ss << "}" << std::endl;
	ss << "};" << std::endl;
	ss << "triple := (n : int) int {" << std::endl;
	ss << "return n * 3;" << std::endl;
	ss << "};" << std::endl;
	ss << "change_func(triple);" << std::endl;
	ss << "triple(5);" << std::endl;
	
	tree = parser(ss.str());

	bk::TokenValue value = evaluator(*tree);
	REQUIRE(10 == value.i);
      }

}
