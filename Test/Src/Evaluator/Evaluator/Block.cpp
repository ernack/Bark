#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/Evaluator/Evaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("evaluator_block", "[EvaluatorBlock]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::Evaluator evaluator { env };

  SECTION("evaluator_block_nested")
    {
      tree = parser("{ x : bool = false; } x;");
      REQUIRE_THROWS(evaluator(*tree));      
    }

  SECTION("evaluator_block_shadowing")
    {
      tree = parser("x : int = 42; { x : bool = false; x}");      
      REQUIRE(false == evaluator(*tree).b);
    }
}
