#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/Evaluator/Evaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("evaluator_types", "[EvaluatorNumbers]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::Evaluator evaluator { env };

  SECTION("evaluator_int")
    {
      tree = parser("54");
      
      REQUIRE(54 == evaluator(*tree).i );
    }

  SECTION("evaluator_negative_int")
    {
      tree = parser("-27");
      
      REQUIRE(-27 == evaluator(*tree).i );
    }

  SECTION("evaluator_uint")
    {
      tree = parser("334u");
      
      REQUIRE(334 == evaluator(*tree).ui );
    }

  SECTION("evaluator_float")
    {
      tree = parser("32.5");
      
      REQUIRE(32.5f == evaluator(*tree).f );
    }
    
  SECTION("evaluator_negative_float")
    {
      tree = parser("-3.1415");
      
      REQUIRE(-3.1415f == evaluator(*tree).f );
    }

  SECTION("evaluator_string")
    {
      tree = parser("\"i love pizzas\"");
      
      REQUIRE("i love pizzas" == evaluator(*tree).s );
    }
}

TEST_CASE("evaluator_nums_arith", "[EvaluatorNumbers]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::Evaluator evaluator { env };

  SECTION("evaluator_add_int")
    {
      tree = parser("54 + 34");
      
      REQUIRE(88 == evaluator(*tree).i );
    }

  SECTION("evaluator_sub_int")
    {
      tree = parser("2 - 1");
      
      REQUIRE(1 == evaluator(*tree).i );
    }

  SECTION("evaluator_mul_int")
    {
      tree = parser("14 * 2");
      
      REQUIRE(28 == evaluator(*tree).i );
    }

  SECTION("evaluator_div_int")
    {
      tree = parser("10 / 2");
      
      REQUIRE(5 == evaluator(*tree).i );
    }

   SECTION("evaluator_add_uint")
    {
      tree = parser("54u + 34u");
      
      REQUIRE(88 == evaluator(*tree).ui );
    }

  SECTION("evaluator_sub_uint")
    {
      tree = parser("2u - 1u");
      
      REQUIRE(1 == evaluator(*tree).ui );
    }

    SECTION("evaluator_mul_uint")
    {
      tree = parser("14u * 2u");
      
      REQUIRE(28 == evaluator(*tree).ui );
    }

    SECTION("evaluator_div_uint")
    {
      tree = parser("10u / 2u");
      
      REQUIRE(5 == evaluator(*tree).ui );
    }

    SECTION("evaluator_add_float")
    {
      tree = parser("54.2   + 34.4");
      
      REQUIRE(88.5f < evaluator(*tree).f );
      REQUIRE(88.7f > evaluator(*tree).f );
    }

  SECTION("evaluator_sub_float")
    {
      tree = parser("   2.2 - 1.0");
      
      REQUIRE(1.2f == evaluator(*tree).f );
    }

  SECTION("evaluator_mul_float")
    {
      tree = parser("   14.2 * 2.0");
      
      REQUIRE(28.4f == evaluator(*tree).f );
    }

    SECTION("evaluator_div_float")
    {
      tree = parser("10.0 / 2.0");
      
      REQUIRE(5.0f == evaluator(*tree).f );
    }

    SECTION("evaluator_sub_negative_int")
      {
	tree = parser("42 - 43");
	
	REQUIRE(-1 == evaluator(*tree).i );
      }
}
