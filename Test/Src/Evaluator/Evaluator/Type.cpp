#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/Evaluator/Evaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("evaluator_type", "[EvaluatorType]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::Evaluator evaluator { env };

  SECTION("evaluator_type_int")
    {
      tree = parser("5");
      evaluator(*tree);
      REQUIRE(bk::TokenType::INT == evaluator.type());
    }

  SECTION("evaluator_type_uint")
    {
      tree = parser("32u");
      evaluator(*tree);
      REQUIRE(bk::TokenType::UINT == evaluator.type());
    }

  SECTION("evaluator_type_float")
    {
      tree = parser(" -34.2");
      evaluator(*tree);
      REQUIRE(bk::TokenType::FLOAT == evaluator.type());
    }

  SECTION("evaluator_type_bool")
    {
      tree = parser("true");
      evaluator(*tree);
      REQUIRE(bk::TokenType::BOOL == evaluator.type());
    }

  SECTION("evaluator_type_string")
    {
      tree = parser("\"papapipipopo\"");
      evaluator(*tree);
      REQUIRE(bk::TokenType::STRING == evaluator.type());
    }

  SECTION("evaluator_type_and")
    {
      tree = parser("true && false");
      evaluator(*tree);
      REQUIRE(bk::TokenType::BOOL == evaluator.type());
    }

  SECTION("evaluator_type_or")
    {
      tree = parser("true || false");
      evaluator(*tree);
      REQUIRE(bk::TokenType::BOOL == evaluator.type());
    }

  SECTION("evaluator_type_not")
    {
      tree = parser(" !false");
      evaluator(*tree);
      REQUIRE(bk::TokenType::BOOL == evaluator.type());
    }
}
