#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/TokenType.hpp"
#include "../../../../Lib/Src/Function.hpp"
#include "../../../../Lib/Src/Evaluator/Evaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("evaluator_while", "[EvaluatorWhile]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::Evaluator evaluator { env };

  SECTION("evaluator_while_false_cond")
    {      
      tree = parser("i := 5; while false { i = i + 1; }");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(false == value.b);
    }

  SECTION("evaluator_while_count_to_ten")
    {      
      tree = parser("i := 0; while i < 10 { i = i + 1; }; i");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(10 == value.i);
    }
}
