#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/Evaluator/Evaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("evaluator_prog", "[EvaluatorProg]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::Evaluator evaluator { env };

    SECTION("evaluator_prog")
    {
      REQUIRE(0 == env.size());
      
      tree = parser("x : int = 6 * 7 ; y : int = 43 ; x - y;");      
      REQUIRE(-1 == evaluator(*tree).i );
      REQUIRE(2 == env.size());
    }
}
