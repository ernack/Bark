#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/Function.hpp"
#include "../../../../Lib/Src/Evaluator/Evaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("evaluator_if", "[EvaluatorIf]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::Evaluator evaluator { env };

  SECTION("evaluator_if_simple_true_cond")
    {      
      tree = parser("if 5 > 2 { 42 }");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(42 == value.i);
    }

  SECTION("evaluator_if_simple_false_cond")
    {      
      tree = parser("if 5 < 2 { 42 }");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(false == value.b);
    }

  SECTION("evaluator_if_else_true_cond")
    {      
      tree = parser("if 5 > 2 { 42 } else { 43 }");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(42 == value.i);
    }

  SECTION("evaluator_if_else_false_cond")
    {      
      tree = parser("if 5 < 2 { 42 } else { 43 }");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(43 == value.i);
    }
  
    SECTION("evaluator_if_error_no_error")
      {
	std::stringstream ss;
	ss << "diviser := (a : float, b : float) float "
	   << "{ if b == 0.0 { error 0; } ; return a/b; };"
	   << "";
	ss << "diviser(3.0, 5.0);";
	ss << "if error diviser { 1 } else { 2 };";
	tree = parser(ss.str());

	bk::TokenValue value = evaluator(*tree);
	REQUIRE(2 == value.i);
      }

    SECTION("evaluator_if_error_then_error")
      {
	std::stringstream ss;
	ss << "diviser := (a : float, b : float) float "
	   << "{ if b == 0.0 { error 0; } ; return a/b; };"
	   << "";
	ss << "diviser(3.0, 0.0);";
	ss << "if error diviser { 1 } else { 2 };";
	tree = parser(ss.str());

	bk::TokenValue value = evaluator(*tree);
	REQUIRE(1 == value.i);
      }

    SECTION("evaluator_if_error_then_error_get_error_code")
      {
	std::stringstream ss;
	ss << "diviser := (a : float, b : float) float "
	   << "{ if b == 0.0 { error 423; } ; return a/b; };"
	   << "";
	ss << "diviser(3.0, 0.0);";
	ss << "if error diviser { _error_id } else { 2 };";
	tree = parser(ss.str());

	bk::TokenValue value = evaluator(*tree);
	REQUIRE(423 == value.i);
      }

    SECTION("evaluator_if_error_no_error_get_error_code")
      {
	std::stringstream ss;
	ss << "diviser := (a : float, b : float) float "
	   << "{ if b == 0.0 { error 423; } ; return a/b; };"
	   << "";
	ss << "diviser(3.0, 5.0);";
	ss << "if error diviser { _error_id } else { _error_id };";
	tree = parser(ss.str());

	REQUIRE_THROWS(evaluator(*tree));
      }
}
