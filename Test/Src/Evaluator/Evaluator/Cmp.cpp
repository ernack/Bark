#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/Evaluator/Evaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("evaluator_cmp", "[EvaluatorCmp]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::Evaluator evaluator { env };

  SECTION("evaluator_lt")
    {      
      tree = parser(" 5 < 3 ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2 < 12 ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 12 < 12 ");      
      REQUIRE(false == evaluator(*tree).b );

      tree = parser(" 5u < 3u ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2u <12u ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 12u < 12u ");      
      REQUIRE(false == evaluator(*tree).b );


      tree = parser(" 5.7 < 3.2 ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2.12 < 12.4 ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 12.7 < 12.7 ");      
      REQUIRE(false == evaluator(*tree).b );

      tree = parser(" 12.7 +1.1 < 12.7 ");      
      REQUIRE(false == evaluator(*tree).b );

    }

  SECTION("evaluator_le")
    {      
      tree = parser(" 5 <= 3 ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2 <= 12 ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 12 <= 12 ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" 5u <= 3u ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2u <= 12u ");
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 12u <= 12u ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" 5.7 <= 3.2 ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2.12 <= 12.4 ");
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 2.12 <= 2.12 ");      
      REQUIRE(true == evaluator(*tree).b );
    }

  SECTION("evaluator_gt")
    {      
      tree = parser(" 5 > 3 ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 2 > 12 ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 12 > 12 ");      
      REQUIRE(false == evaluator(*tree).b );

      tree = parser(" 5u > 3u ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 2u > 12u ");
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 12u > 12u ");      
      REQUIRE(false == evaluator(*tree).b );

      tree = parser(" 5.7 > 3.2 ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 2.12 > 12.4 ");
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2.12 > 2.12 ");      
      REQUIRE(false == evaluator(*tree).b );
    }

  SECTION("evaluator_ge")
    {      
      tree = parser(" 5 >= 3 ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 2 >= 12 ");      
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 12 >= 12 ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" 5u >= 3u ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 2u >= 12u ");
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 12u >= 12u ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" 5.7 >= 3.2 ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" 2.12 >= 12.4 ");
      REQUIRE(false == evaluator(*tree).b );
      tree = parser(" 2.12 >= 2.12 ");      
      REQUIRE(true == evaluator(*tree).b );
    }

  SECTION("evaluator_ge")
    {      
      tree = parser(" 5 == 3 ");      
      REQUIRE(false == evaluator(*tree).b );      
      tree = parser(" 5 == 5 ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" 5u == 3u ");      
      REQUIRE(false == evaluator(*tree).b );      
      tree = parser(" 5u == 5u ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" 5.7 == 5.2 ");      
      REQUIRE(false == evaluator(*tree).b );      
      tree = parser(" 5.7 == 5.7 ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" \"abcd\" == \"abc\" ");      
      REQUIRE(false == evaluator(*tree).b );      
      tree = parser(" \"abcd\" == \"abcd\" ");      
      REQUIRE(true == evaluator(*tree).b );

      tree = parser(" true == false ");      
      REQUIRE(false == evaluator(*tree).b );      
      tree = parser(" true == true ");      
      REQUIRE(true == evaluator(*tree).b );
      tree = parser(" false == false ");      
      REQUIRE(true == evaluator(*tree).b );
    }
}

TEST_CASE("evaluator_cmp_bug", "[EvaluatorCmp]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::Evaluator evaluator { env };

  std::stringstream ss;

  ss << "  __plus_ou_moins := (nb_mystere : int) {" << std::endl;
  ss << "  nb : int = 0;" << std::endl;
  ss << "  nb = 4;" << std::endl;
  ss << "  if nb < nb_mystere {" << std::endl;
  ss << "      4;" << std::endl;
  ss << "  }" << std::endl;
  ss << "}" << std::endl;

  ss << "__plus_ou_moins(5);" << std::endl;

  
  tree = parser(ss.str());      
  REQUIRE_NOTHROW( evaluator(*tree) );

}
