#include <catch.hpp>
#include "../../../../Lib/Src/Parser.hpp"
#include "../../../../Lib/Src/Lexer.hpp"
#include "../../../../Lib/Src/TreeNode.hpp"
#include "../../../../Lib/Src/TokenType.hpp"
#include "../../../../Lib/Src/Function.hpp"
#include "../../../../Lib/Src/Evaluator/Evaluator.hpp"
#include "../../../../Lib/Src/Environment/Environment.hpp"

TEST_CASE("evaluator_inference", "[EvaluatorInference]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  std::shared_ptr<bk::TreeNode> tree;

  bk::Environment env;
  bk::Evaluator evaluator { env };

  SECTION("evaluator_inference_int")
    {      
      tree = parser("x := 5; x");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(5 == value.i);
    }

  SECTION("evaluator_inference_uint")
    {      
      tree = parser("x := 37u + 2u; x");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(39 == value.ui);
    }
  
  SECTION("evaluator_inference_float")
    {      
      tree = parser("x := 2.4 + 1.2; x");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(0.1 >= std::abs(value.f - 3.6f));
    }

  SECTION("evaluator_inference_bool")
    {      
      tree = parser("x := true || false; x");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(true == value.b);
    }

  SECTION("evaluator_inference_string")
    {      
      tree = parser("x := \"salut\"; x");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE("salut" == value.s);
    }

  SECTION("evaluator_inference_function_0")
    {      
      tree = parser("f := (n : int) int { return n * 2; }; f(7)");

      bk::TokenValue value = evaluator(*tree);
      REQUIRE(14 == value.i);
    }

  SECTION("evaluator_inference_int")
    {      
      tree = parser("x := 125; x");

      bk::TokenValue value = evaluator(*tree);

      auto type = env.get_type("x");
      
      REQUIRE(nullptr != type);
      REQUIRE(bk::TokenType::INT == *type);
    }

  SECTION("evaluator_inference_uint")
    {      
      tree = parser("x := 125u; x");

      bk::TokenValue value = evaluator(*tree);

      auto type = env.get_type("x");
      
      REQUIRE(nullptr != type);
      REQUIRE(bk::TokenType::UINT == *type);
    }

  SECTION("evaluator_inference_float")
    {      
      tree = parser("x := 125.4; x");

      bk::TokenValue value = evaluator(*tree);

      auto type = env.get_type("x");
      
      REQUIRE(nullptr != type);
      REQUIRE(bk::TokenType::FLOAT == *type);
    }
  
  SECTION("evaluator_inference_bool")
    {      
      tree = parser("x := false; x");

      bk::TokenValue value = evaluator(*tree);

      auto type = env.get_type("x");
      
      REQUIRE(nullptr != type);
      REQUIRE(bk::TokenType::BOOL == *type);
    }
	
  SECTION("evaluator_inference_string")
    {      
      tree = parser("x := \"coucou\"; x");

      bk::TokenValue value = evaluator(*tree);

      auto type = env.get_type("x");
      
      REQUIRE(nullptr != type);
      REQUIRE(bk::TokenType::STRING == *type);
    }

  SECTION("evaluator_inference_function")
    {      
      tree = parser("x := (n : int) int { return n }; x");

      bk::TokenValue value = evaluator(*tree);

      auto type = env.get_type("x");
      
      REQUIRE(nullptr != type);
      REQUIRE(bk::TokenType::FUNCTION == *type);
    }

    SECTION("evaluator_inference_function_call")
    {      
      tree = parser("f := (n : int) int { return n * 2; }; f(218);");

      bk::TokenValue value = evaluator(*tree);

      REQUIRE(436 == value.i);
    }

}
