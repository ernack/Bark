#include <catch.hpp>
#include "../../../Lib/Src/TokenValue.hpp"
#include "../../../Lib/Src/TokenType.hpp"
#include "../../../Lib/Src/Environment/Environment.hpp"
#include "../../../Lib/Src/Function.hpp"

TEST_CASE("environment_get", "[Environment]")
{
  bk::Environment env;

  bk::TokenValue value;
  value.i = 42;

  REQUIRE(0 == env.size());
  
  env.declare("ma_variable", bk::TokenType::INT, value);
  
  REQUIRE(1 == env.size());
  REQUIRE(42 == env.get_value("ma_variable")->i);
  REQUIRE(bk::TokenType::INT == *env.get_type("ma_variable"));

  bk::TokenValue value2; value2.f = 3.14f;  
  REQUIRE(false == env.declare("ma_variable", bk::TokenType::FLOAT, value2));
  REQUIRE(1 == env.size());
}

TEST_CASE("environment_set", "[Environment]")
{
  bk::Environment env;

  bk::TokenValue value;
  value.i = 42;

  REQUIRE(0 == env.size());
  
  env.declare("ma_variable", bk::TokenType::INT, value);
  
  REQUIRE(1 == env.size());
  
  REQUIRE(42 == env.get_value("ma_variable")->i);
  REQUIRE(bk::TokenType::INT == *env.get_type("ma_variable"));

  bk::TokenValue value2; value2.f = 3.14f;  
  REQUIRE(false == env.declare("ma_variable", bk::TokenType::FLOAT, value2));
  REQUIRE(1 == env.size());

  env.set_value("ma_variable", bk::token_value(4));
  
  REQUIRE(4 == env.get_value("ma_variable")->i);
}

TEST_CASE("environment_block", "[Environment]")
{
  bk::Environment env;

  bk::TokenValue value;
  value.i = 42;

  env.open_block();
  
  env.declare("ma_variable", bk::TokenType::INT, value);

  env.close_block();
  
  REQUIRE(0 == env.size());
  
}

TEST_CASE("environment_block_function", "[Environment]")
{
  bk::Environment env;

  bk::TokenValue value;
  value.function = std::make_shared<bk::Function>(env);

  env.open_block();
  
  env.declare("ma_variable", bk::TokenType::FUNCTION, value);

  env.close_block();
  
  REQUIRE(0 == env.size());
  
}

TEST_CASE("environment_more_blocks", "[Environment]")
{
  bk::Environment env;

  bk::TokenValue value;
  value.function = std::make_shared<bk::Function>(env);

  REQUIRE(0 == env.size());
  env.open_block();
  REQUIRE(0 == env.size());
  env.declare("x", bk::TokenType::INT, bk::token_value(45));
  REQUIRE(1 == env.size());
  env.open_block();
  env.declare("y", bk::TokenType::INT, bk::token_value(45));
  REQUIRE(2 == env.size());
  // env.open_block();
  // env.declare("z", bk::TokenType::INT, bk::token_value(45));
  // REQUIRE(3 == env.size());

  // env.close_block();
  
  // REQUIRE(2 == env.size());
  
  // env.close_block();
  // REQUIRE(1 == env.size());

  // env.close_block();
  // REQUIRE(0 == env.size());
}
