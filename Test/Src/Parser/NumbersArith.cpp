#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("parser_literals", "[ParserNumbersArith]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_literal_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("3965");
      REQUIRE(nullptr != root);
      REQUIRE("INT(3965)" == root->string());
    }

  SECTION("parser_literal_uint")
    {
      std::unique_ptr<bk::TreeNode> root = parser("27u");
      REQUIRE(nullptr != root);
      REQUIRE("UINT(27u)" == root->string());
    }

  SECTION("parser_literal_float")
    {
      std::unique_ptr<bk::TreeNode> root = parser("3.15");
      REQUIRE(nullptr != root);
      REQUIRE("FLOAT(3.15)" == root->string());
    }
}

TEST_CASE("parser_add", "[ParserNumbersArith]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_add_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("1024 + 89");
      REQUIRE(nullptr != root);
      REQUIRE("ADD(INT(1024),INT(89))" == root->string());
    }

  SECTION("parser_add_more_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("1024 + 89 + 3 + 8");
      REQUIRE(nullptr != root);
      REQUIRE("ADD(ADD(ADD(INT(1024),INT(89)),INT(3)),INT(8))" == root->string());
    }
    
  SECTION("parser_add_uint")
    {
      std::unique_ptr<bk::TreeNode> root = parser("27u + 789u");
      REQUIRE(nullptr != root);
      REQUIRE("ADD(UINT(27u),UINT(789u))" == root->string());
    }

  SECTION("parser_add_more_uint")
    {
      std::unique_ptr<bk::TreeNode> root = parser("27u + 789u + 9u + 14u + 5u");
      REQUIRE(nullptr != root);
      REQUIRE("ADD(ADD(ADD(ADD(UINT(27u),UINT(789u)),UINT(9u)),UINT(14u)),UINT(5u))" ==
	      root->string());
    }

  SECTION("parser_add_float")
    {
      std::unique_ptr<bk::TreeNode> root = parser("3.15 + 4.7");
      REQUIRE(nullptr != root);
      REQUIRE("ADD(FLOAT(3.15),FLOAT(4.7))" == root->string());
    }

  SECTION("parser_add_more_float")
    {
      std::unique_ptr<bk::TreeNode> root = parser("3.15 + 4.7 + 7.2");
      REQUIRE(nullptr != root);
      REQUIRE("ADD(ADD(FLOAT(3.15),FLOAT(4.7)),FLOAT(7.2))" == root->string());
    }
}

TEST_CASE("parser_sub", "[ParserNumbersArith]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_sub_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("1024 - 89");
      REQUIRE(nullptr != root);
      REQUIRE("SUB(INT(1024),INT(89))" == root->string());
    }

  SECTION("parser_sub_more_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("1024 - 89 - 3 - 8");
      REQUIRE(nullptr != root);
      REQUIRE("SUB(SUB(SUB(INT(1024),INT(89)),INT(3)),INT(8))" == root->string());
    }
    
  SECTION("parser_sub_uint")
    {
      std::unique_ptr<bk::TreeNode> root = parser("27u - 789u");
      REQUIRE(nullptr != root);
      REQUIRE("SUB(UINT(27u),UINT(789u))" == root->string());
    }

  SECTION("parser_sub_more_uint")
    {
      std::unique_ptr<bk::TreeNode> root = parser("27u - 789u - 9u - 14u - 5u");
      REQUIRE(nullptr != root);
      REQUIRE("SUB(SUB(SUB(SUB(UINT(27u),UINT(789u)),UINT(9u)),UINT(14u)),UINT(5u))" ==
	      root->string());
    }

  SECTION("parser_sub_float")
    {
      std::unique_ptr<bk::TreeNode> root = parser("3.15 - 4.7");
      REQUIRE(nullptr != root);
      REQUIRE("SUB(FLOAT(3.15),FLOAT(4.7))" == root->string());
    }

  SECTION("parser_sub_more_float")
    {
      std::unique_ptr<bk::TreeNode> root = parser("3.15 - 4.7 - 7.2");
      REQUIRE(nullptr != root);
      REQUIRE("SUB(SUB(FLOAT(3.15),FLOAT(4.7)),FLOAT(7.2))" == root->string());
    }
}

TEST_CASE("parser_mod", "[ParserNumbersArith]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  
  std::unique_ptr<bk::TreeNode> root = parser("53 % 812");
  REQUIRE(nullptr != root);
  REQUIRE("MOD(INT(53),INT(812))" == root->string());
}

TEST_CASE("parser_all_ops", "[ParserNumbersArith]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  
  SECTION("parser_all_ops_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("12 + 32 - 45 * 98 / 53 % 812");
      REQUIRE(nullptr != root);
      REQUIRE("SUB(ADD(INT(12),INT(32)),MOD(DIV(MUL(INT(45),INT(98)),INT(53)),INT(812)))" ==
	      root->string());
    }
}

TEST_CASE("parser_par", "[ParserBoolsArith]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  std::unique_ptr<bk::TreeNode> root = parser("(5 + 3) * 2");
  REQUIRE(nullptr != root);
  REQUIRE("MUL(ADD(INT(5),INT(3)),INT(2))" == root->string());
}
