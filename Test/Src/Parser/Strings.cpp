#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("parser_strings", "[ParserStrings]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_simple_string")
    {
      std::unique_ptr<bk::TreeNode> root = parser("\"je pense donc je suis\"");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::STRING == root->type());
      REQUIRE("STRING(je pense donc je suis)" == root->string());
    }
}
