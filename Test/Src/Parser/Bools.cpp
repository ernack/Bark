#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("parser_bool", "[ParserBools]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_true")
    {
      std::unique_ptr<bk::TreeNode> root = parser("true");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::BOOL == root->type());
      REQUIRE("BOOL(true)" == root->string());
    }

  SECTION("parser_false")
    {
      std::unique_ptr<bk::TreeNode> root = parser("false");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::BOOL == root->type());
      REQUIRE("BOOL(false)" == root->string());
    }
}
