#include <iostream>
#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("parser_inference", "[ParserInference]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_inference_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("i := 9;");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::VAR_DECL == root->type());
      REQUIRE("VAR_DECL(IDENT(i),INT(9))" == root->string());
    }

    SECTION("parser_inference_function")
    {
      std::unique_ptr<bk::TreeNode> root = parser("i := (a : int) int {return a;};");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::VAR_DECL == root->type());
      REQUIRE("VAR_DECL(IDENT(i),FUNCTION(TYPE(int),PARAMS(PARAM(IDENT(a),TYPE(int))),RETURN(IDENT(a))))" == root->string());
    }
}
