#include <iostream>
#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("parser_while", "[ParserWhile]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_while_true")
    {
      std::unique_ptr<bk::TreeNode> root = parser("while true { x = x + 1; }");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::WHILE == root->type());
      REQUIRE("WHILE(BOOL(true),ASSIGN(IDENT(x),ADD(IDENT(x),INT(1))))" == root->string());
    }
}
