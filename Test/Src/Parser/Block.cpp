#include <iostream>
#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("parser_block", "[ParserBlock]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_block_empty")
    {
      std::unique_ptr<bk::TreeNode> root = parser("{}");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::BLOCK == root->type());
      REQUIRE("BLOCK" == root->string());
    }

  SECTION("parser_block_one_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("{ 5; }");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::BLOCK == root->type());
      REQUIRE("BLOCK(INT(5))" == root->string());
    }

    
  SECTION("parser_block_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("{4; 5; 6}");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::BLOCK == root->type());
      REQUIRE("BLOCK(INT(4),INT(5),INT(6))" == root->string());      
    }
}
