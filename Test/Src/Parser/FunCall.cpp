#include <iostream>
#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("parser_funcall", "[ParserFunCall]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_funcall_no_arg")
    {
      std::unique_ptr<bk::TreeNode> root = parser("y()");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::FUNCALL == root->type());
      REQUIRE("FUNCALL(IDENT(y))" ==
	      root->string());
    }
    
  SECTION("parser_funcall_one_arg")
    {
      std::unique_ptr<bk::TreeNode> root = parser("x(1u)");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::FUNCALL == root->type());
      REQUIRE("FUNCALL(IDENT(x),UINT(1u))" ==
	      root->string());
    }
    
  SECTION("parser_funcall_more_args")
    {
      std::unique_ptr<bk::TreeNode> root = parser("x(1, 2.3, true, \"hola\", 9u)");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::FUNCALL == root->type());
      REQUIRE("FUNCALL(IDENT(x),INT(1),FLOAT(2.3),BOOL(true),STRING(hola),UINT(9u))" ==
	      root->string());
    }
}
