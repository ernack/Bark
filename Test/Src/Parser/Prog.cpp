#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("parser_prog", "[ParserProg]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_prog")
    {
      std::unique_ptr<bk::TreeNode> root = parser("5 + 7; 2 + 3;");
      REQUIRE(nullptr != root);
      REQUIRE("BARK(ADD(INT(5),INT(7)),ADD(INT(2),INT(3)))" == root->string());
    }
}
