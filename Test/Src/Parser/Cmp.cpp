#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("parser_cmp", "[ParserCmp]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_lt_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57 < 9");
      REQUIRE(nullptr != root);
      REQUIRE("LT(INT(57),INT(9))" == root->string());
    }

  SECTION("parser_le_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57 <= 9");
      REQUIRE(nullptr != root);
      REQUIRE("LE(INT(57),INT(9))" == root->string());
    }

  SECTION("parser_gt_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57 > 9");
      REQUIRE(nullptr != root);
      REQUIRE("GT(INT(57),INT(9))" == root->string());
    }

  SECTION("parser_ge_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57 >= 9");
      REQUIRE(nullptr != root);
      REQUIRE("GE(INT(57),INT(9))" == root->string());
    }

  SECTION("parser_eq_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57 == 9");
      REQUIRE(nullptr != root);
      REQUIRE("EQ(INT(57),INT(9))" == root->string());
    }

  SECTION("parser_neq_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57 != 9");
      REQUIRE(nullptr != root);
      REQUIRE("NEQ(INT(57),INT(9))" == root->string());
    }

  ////
    SECTION("parser_lt_uint")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57u  < 9u");
      REQUIRE(nullptr != root);
      REQUIRE("LT(UINT(57u),UINT(9u))" == root->string());
    }

  SECTION("parser_le_uint")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57u <= 9u");
      REQUIRE(nullptr != root);
      REQUIRE("LE(UINT(57u),UINT(9u))" == root->string());
    }

  SECTION("parser_gt_uint")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57u  > 9u");
      REQUIRE(nullptr != root);
      REQUIRE("GT(UINT(57u),UINT(9u))" == root->string());
    }

  SECTION("parser_ge_uint")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57u  >= 9u ");
      REQUIRE(nullptr != root);
      REQUIRE("GE(UINT(57u),UINT(9u))" == root->string());
    }

  SECTION("parser_eq_uint")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57u == 9u");
      REQUIRE(nullptr != root);
      REQUIRE("EQ(UINT(57u),UINT(9u))" == root->string());
    }

  SECTION("parser_neq_uint")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57u != 9u");
      REQUIRE(nullptr != root);
      REQUIRE("NEQ(UINT(57u),UINT(9u))" == root->string());
    }

  ////

  SECTION("parser_lt_float")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57.4  < 9.4");
      REQUIRE(nullptr != root);
      REQUIRE("LT(FLOAT(57.4),FLOAT(9.4))" == root->string());
    }

  SECTION("parser_le_float")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57.4 <= 9.4");
      REQUIRE(nullptr != root);
      REQUIRE("LE(FLOAT(57.4),FLOAT(9.4))" == root->string());
    }

  SECTION("parser_gt_float")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57.4  > 9.4");
      REQUIRE(nullptr != root);
      REQUIRE("GT(FLOAT(57.4),FLOAT(9.4))" == root->string());
    }

  SECTION("parser_ge_float")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57.4  >= 9.4 ");
      REQUIRE(nullptr != root);
      REQUIRE("GE(FLOAT(57.4),FLOAT(9.4))" == root->string());
    }

  SECTION("parser_eq_float")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57.4 == 9.4");
      REQUIRE(nullptr != root);
      REQUIRE("EQ(FLOAT(57.4),FLOAT(9.4))" == root->string());
    }

  SECTION("parser_neq_float")
    {
      std::unique_ptr<bk::TreeNode> root = parser("57.4 != 9.4");
      REQUIRE(nullptr != root);
      REQUIRE("NEQ(FLOAT(57.4),FLOAT(9.4))" == root->string());
    }

  ////
  
  SECTION("parser_lt_bool")
    {
      std::unique_ptr<bk::TreeNode> root = parser("true  < false");
      REQUIRE(nullptr != root);
      REQUIRE("LT(BOOL(true),BOOL(false))" == root->string());
    }

  SECTION("parser_le_bool")
    {
      std::unique_ptr<bk::TreeNode> root = parser("true <= false");
      REQUIRE(nullptr != root);
      REQUIRE("LE(BOOL(true),BOOL(false))" == root->string());
    }

  SECTION("parser_gt_bool")
    {
      std::unique_ptr<bk::TreeNode> root = parser("true  > false");
      REQUIRE(nullptr != root);
      REQUIRE("GT(BOOL(true),BOOL(false))" == root->string());
    }

  SECTION("parser_ge_bool")
    {
      std::unique_ptr<bk::TreeNode> root = parser("true  >= false ");
      REQUIRE(nullptr != root);
      REQUIRE("GE(BOOL(true),BOOL(false))" == root->string());
    }

  SECTION("parser_eq_bool")
    {
      std::unique_ptr<bk::TreeNode> root = parser("true == false");
      REQUIRE(nullptr != root);
      REQUIRE("EQ(BOOL(true),BOOL(false))" == root->string());
    }

  SECTION("parser_neq_bool")
    {
      std::unique_ptr<bk::TreeNode> root = parser("true != false");
      REQUIRE(nullptr != root);
      REQUIRE("NEQ(BOOL(true),BOOL(false))" == root->string());
    }

  ////


  SECTION("parser_eq_bool")
    {
      std::unique_ptr<bk::TreeNode> root = parser("\"qsd\" ==  \"aze \" ");
      REQUIRE(nullptr != root);
      REQUIRE("EQ(STRING(qsd),STRING(aze ))" == root->string());
    }
    
  SECTION("parser_neq_bool")
    {
      std::unique_ptr<bk::TreeNode> root = parser("\"qsd\" !=  \"aze \" ");
      REQUIRE(nullptr != root);
      REQUIRE("NEQ(STRING(qsd),STRING(aze ))" == root->string());
    }
}
