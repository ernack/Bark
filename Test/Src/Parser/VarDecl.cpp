#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("parser_var_decl", "[ParserVarDecl]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_decl_int")
    {
      std::unique_ptr<bk::TreeNode> root = parser("toto : int = 42");
      REQUIRE(nullptr != root);
      REQUIRE("VAR_DECL(IDENT(toto),TYPE(int),INT(42))" == root->string());
    }
  
  SECTION("parser_decl_uint")
    {
      std::unique_ptr<bk::TreeNode> root = parser("titi : uint = 34u");
      REQUIRE(nullptr != root);
      REQUIRE("VAR_DECL(IDENT(titi),TYPE(uint),UINT(34u))" == root->string());
    }

  SECTION("parser_decl_float")
    {
      std::unique_ptr<bk::TreeNode> root = parser("toto : float = 727.4");
      REQUIRE(nullptr != root);
      REQUIRE("VAR_DECL(IDENT(toto),TYPE(float),FLOAT(727.4))" == root->string());
    }

  SECTION("parser_decl_string")
    {
      std::unique_ptr<bk::TreeNode> root = parser("toto : string = \"coucoucoucou\"");
      REQUIRE(nullptr != root);
      REQUIRE("VAR_DECL(IDENT(toto),TYPE(string),STRING(coucoucoucou))" == root->string());
    }

  SECTION("parser_decl_bool")
    {
      std::unique_ptr<bk::TreeNode> root = parser("toto : bool = true");
      REQUIRE(nullptr != root);
      REQUIRE("VAR_DECL(IDENT(toto),TYPE(bool),BOOL(true))" == root->string());
    }
}
