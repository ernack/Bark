#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("parser_ident", "[ParserIdent]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_ident_alpha")
    {
      std::unique_ptr<bk::TreeNode> root = parser("helloworld");
      REQUIRE(nullptr != root);
      REQUIRE("IDENT(helloworld)" == root->string());
    }

  SECTION("parser_ident_alphanum")
    {
      std::unique_ptr<bk::TreeNode> root = parser("ilovep1zz4");
      REQUIRE(nullptr != root);
      REQUIRE("IDENT(ilovep1zz4)" == root->string());
    }
  
  SECTION("parser_ident_add")
    {
      std::unique_ptr<bk::TreeNode> root = parser("ilovep1zz4 + 3");
      REQUIRE(nullptr != root);
      REQUIRE("ADD(IDENT(ilovep1zz4),INT(3))" == root->string());
    }
}
