#include <iostream>
#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("parser_function", "[ParserFunction]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_function_no_param_no_return")
    {
      std::unique_ptr<bk::TreeNode> root = parser("() {}");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::FUNCTION == root->type());
      REQUIRE("FUNCTION(PARAMS)" == root->string());
    }

  SECTION("parser_function_no_param_but_return")
    {
      std::unique_ptr<bk::TreeNode> root = parser("() int { return 42; }");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::FUNCTION == root->type());
      REQUIRE("FUNCTION(TYPE(int),PARAMS,RETURN(INT(42)))" == root->string());
    }

  SECTION("parser_function_params_and_return")
    {
      std::unique_ptr<bk::TreeNode> root = parser("(x : int, y : int) int { return x + y; }");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::FUNCTION == root->type());
      REQUIRE("FUNCTION(TYPE(int),PARAMS(PARAM(IDENT(x),TYPE(int)),PARAM(IDENT(y),TYPE(int))),RETURN(ADD(IDENT(x),IDENT(y))))" == root->string());
    }

    SECTION("parser_function_params_and_error")
    {
      std::unique_ptr<bk::TreeNode> root = parser("(x : int, y : int) int { error 5; }");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::FUNCTION == root->type());
      REQUIRE("FUNCTION(TYPE(int),PARAMS(PARAM(IDENT(x),TYPE(int)),PARAM(IDENT(y),TYPE(int))),ERROR(INT(5)))" ==
  	      root->string());
    }

    SECTION("parser_function_params_and_return_ref")
    {
      std::unique_ptr<bk::TreeNode> root = parser("(x : &int, y : int) int { return x + y; }");
      REQUIRE(nullptr != root);
      REQUIRE(bk::TreeNodeType::FUNCTION == root->type());
      REQUIRE("FUNCTION(TYPE(int),PARAMS(PARAM_REF(IDENT(x),TYPE(int)),PARAM(IDENT(y),TYPE(int))),RETURN(ADD(IDENT(x),IDENT(y))))" == root->string());
    }

    SECTION("parser_function_params_and_error_ref")
      {
  	std::unique_ptr<bk::TreeNode> root = parser("(x : int, y : &int) int { error 5; }");
  	REQUIRE(nullptr != root);
  	REQUIRE(bk::TreeNodeType::FUNCTION == root->type());
  	REQUIRE("FUNCTION(TYPE(int),PARAMS(PARAM(IDENT(x),TYPE(int)),PARAM_REF(IDENT(y),TYPE(int))),ERROR(INT(5)))" ==
  		root->string());
      }

    SECTION("parser_function_params_and_return_ref_const")
      {
  	std::unique_ptr<bk::TreeNode> root = parser("(x : int, y : &&int) int { return x + y; }");
  	REQUIRE(nullptr != root);
  	REQUIRE(bk::TreeNodeType::FUNCTION == root->type());
  	REQUIRE("FUNCTION(TYPE(int),PARAMS(PARAM(IDENT(x),TYPE(int)),PARAM_CREF(IDENT(y),TYPE(int))),RETURN(ADD(IDENT(x),IDENT(y))))" == root->string());
      }

    SECTION("parser_function_params_and_error_ref_const")
      {
  	std::unique_ptr<bk::TreeNode> root = parser("(x : &&int, y : int) int { error 5; }");
  	REQUIRE(nullptr != root);
  	REQUIRE(bk::TreeNodeType::FUNCTION == root->type());
  	REQUIRE("FUNCTION(TYPE(int),PARAMS(PARAM_CREF(IDENT(x),TYPE(int)),PARAM(IDENT(y),TYPE(int))),ERROR(INT(5)))" ==
  		root->string());
      }

    SECTION("parser_function_decl")
      {
  	std::unique_ptr<bk::TreeNode> root = parser("x : (int)int = (i : int) int {return i * 2;};");
  	REQUIRE(nullptr != root);
  	REQUIRE("VAR_DECL(IDENT(x),TYPE((int)int),FUNCTION(TYPE(int),PARAMS(PARAM(IDENT(i),TYPE(int))),RETURN(MUL(IDENT(i),INT(2)))))" == root->string());
      }

    SECTION("parser_function_error")
      {
  	std::unique_ptr<bk::TreeNode> root = parser("x := (i:int) int { error 5; }");
  	REQUIRE(nullptr != root);
  	REQUIRE("VAR_DECL(IDENT(x),FUNCTION(TYPE(int),PARAMS(PARAM(IDENT(i),TYPE(int))),ERROR(INT(5))))" == root->string());
      }

    SECTION("parser_function_function_ref")
      {
	std::stringstream ss;
	ss << "";

	ss << "change_func := (f : &(int)int) {" << std::endl;
	ss << "f = (i : int) int {" << std::endl;
	ss << "return i * 2;" << std::endl;
	ss << "}" <<  std::endl;
	ss << "};" << std::endl;

	
  	std::unique_ptr<bk::TreeNode> root = parser(ss.str());

	REQUIRE(nullptr != root);
  	REQUIRE("VAR_DECL(IDENT(change_func),FUNCTION(PARAMS(PARAM_REF(IDENT(f),TYPE((int)int))),ASSIGN(IDENT(f),FUNCTION(TYPE(int),PARAMS(PARAM(IDENT(i),TYPE(int))),RETURN(MUL(IDENT(i),INT(2)))))))" == root->string());
      }

    SECTION("parser_function_function_cref")
      {
	std::stringstream ss;
	ss << "";

	ss << "change_func := (f : &&(int)int) {" << std::endl;
	ss << "f = (i : int) int {" << std::endl;
	ss << "return i * 2;" << std::endl;
	ss << "}" <<  std::endl;
	ss << "};" << std::endl;

	
  	std::unique_ptr<bk::TreeNode> root = parser(ss.str());

	REQUIRE(nullptr != root);
  	REQUIRE("VAR_DECL(IDENT(change_func),FUNCTION(PARAMS(PARAM_CREF(IDENT(f),TYPE((int)int))),ASSIGN(IDENT(f),FUNCTION(TYPE(int),PARAMS(PARAM(IDENT(i),TYPE(int))),RETURN(MUL(IDENT(i),INT(2)))))))" == root->string());
      }
}
