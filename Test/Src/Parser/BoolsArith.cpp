#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("parser_bool_and", "[ParserBoolsArith]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  std::unique_ptr<bk::TreeNode> root = parser("true && false");
  REQUIRE(nullptr != root);
  REQUIRE("AND(BOOL(true),BOOL(false))" == root->string());
}

TEST_CASE("parser_bool_or", "[ParserBoolsArith]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  std::unique_ptr<bk::TreeNode> root = parser("false || true");
  REQUIRE(nullptr != root);
  REQUIRE("OR(BOOL(false),BOOL(true))" == root->string());
}

TEST_CASE("parser_bool_not", "[ParserBoolsArith]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  std::unique_ptr<bk::TreeNode> root = parser("!false");
  REQUIRE(nullptr != root);
  REQUIRE("NOT(BOOL(false))" == root->string());
}

TEST_CASE("parser_all_bool_ops", "[ParserBoolsArith]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  std::unique_ptr<bk::TreeNode> root = parser("!false && true || false");
  REQUIRE(nullptr != root);
  REQUIRE("OR(AND(NOT(BOOL(false)),BOOL(true)),BOOL(false))" == root->string());
}
