#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("parser_assign", "[ParserAssign]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_assign")
    {
      std::unique_ptr<bk::TreeNode> root = parser("x = 42");
      REQUIRE(nullptr != root);
      REQUIRE("ASSIGN(IDENT(x),INT(42))" == root->string());
    }

  SECTION("parser_assign_op")
    {
      std::unique_ptr<bk::TreeNode> root = parser("x = 42u + 32u");
      REQUIRE(nullptr != root);
      REQUIRE("ASSIGN(IDENT(x),ADD(UINT(42u),UINT(32u)))" == root->string());
    }
}
