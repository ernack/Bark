#include <iostream>
#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Parser.hpp"
#include "../../../Lib/Src/TreeNode.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("parser_if", "[ParserIf]")
{
  bk::Lexer lexer;
  bk::Parser parser { lexer };

  SECTION("parser_if_simple_if")
    {
      auto root = parser(" if true    { x = x + 4;}");
      
      REQUIRE(nullptr != root);
      
      REQUIRE(bk::TreeNodeType::IF == root->type());
      
      REQUIRE("IF(BOOL(true),ASSIGN(IDENT(x),ADD(IDENT(x),INT(4))))" ==
	      root->string());
    }
  
  SECTION("parser_if_with_else")
    {
      auto root = parser("if 5 <3  {return   false; }else {return   true ; }");
      
      REQUIRE(nullptr != root);
      
      REQUIRE(bk::TreeNodeType::IF == root->type());
      
      REQUIRE("IF(LT(INT(5),INT(3)),RETURN(BOOL(false)),RETURN(BOOL(true)))" ==
	      root->string());
    }

  SECTION("parser_if_err_no_else")
    {
      auto root = parser("if error coucou  {return   false; }");
      
      REQUIRE(nullptr != root);
      
      REQUIRE(bk::TreeNodeType::IF == root->type());
      
      REQUIRE("IF(ERROR(IDENT(coucou)),RETURN(BOOL(false)))" ==
	      root->string());
    }
}
