#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_numbers_add_sub", "[LexerNumbersArith]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("one_addition")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("7 + 31");

      REQUIRE(3 == tokens.size());
      
      REQUIRE(bk::TokenType::INT == tokens.at(0).type());
      REQUIRE(7 == tokens.at(0).int_value());

      REQUIRE(bk::TokenType::ADD == tokens.at(1).type());

      REQUIRE(bk::TokenType::INT == tokens.at(2).type());
      REQUIRE(31 == tokens.at(2).int_value());
    }

  SECTION("one_substraction")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("14 - 2");

      REQUIRE(3 == tokens.size());
      
      REQUIRE(bk::TokenType::INT == tokens.at(0).type());
      REQUIRE(14 == tokens.at(0).int_value());

      REQUIRE(bk::TokenType::SUB == tokens.at(1).type());

      REQUIRE(bk::TokenType::INT == tokens.at(2).type());
      REQUIRE(2 == tokens.at(2).int_value());
    }

  SECTION("additions_and_substractions")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("5 + 7 - 3");
      
      REQUIRE(5 == tokens.size());
      
      REQUIRE(bk::TokenType::INT == tokens.at(0).type());
      REQUIRE(5 == tokens.at(0).int_value());

      REQUIRE(bk::TokenType::ADD == tokens.at(1).type());

      REQUIRE(bk::TokenType::INT == tokens.at(2).type());
      REQUIRE(7 == tokens.at(2).int_value());

      REQUIRE(bk::TokenType::SUB == tokens.at(3).type());

      REQUIRE(bk::TokenType::INT == tokens.at(4).type());
      REQUIRE(3 == tokens.at(4).int_value());
    }

    SECTION("all_operations_on_int")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("1 * 2 + 3 - 4 / 5");
      
      REQUIRE(9 == tokens.size());
      
      REQUIRE(bk::TokenType::INT == tokens.at(0).type());
      REQUIRE(1 == tokens.at(0).int_value());

      REQUIRE(bk::TokenType::MUL == tokens.at(1).type());

      REQUIRE(bk::TokenType::INT == tokens.at(2).type());
      REQUIRE(2 == tokens.at(2).int_value());

      REQUIRE(bk::TokenType::ADD == tokens.at(3).type());

      REQUIRE(bk::TokenType::INT == tokens.at(4).type());
      REQUIRE(3 == tokens.at(4).int_value());

      REQUIRE(bk::TokenType::SUB == tokens.at(5).type());

      REQUIRE(bk::TokenType::INT == tokens.at(6).type());
      REQUIRE(4 == tokens.at(6).int_value());

      REQUIRE(bk::TokenType::DIV == tokens.at(7).type());

      REQUIRE(bk::TokenType::INT == tokens.at(8).type());
      REQUIRE(5 == tokens.at(8).int_value());
    }

    SECTION("all_operations_on_uint")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("1u * 2u + 3u - 4u / 5u");
      
      REQUIRE(9 == tokens.size());
      
      REQUIRE(bk::TokenType::UINT == tokens.at(0).type());
      REQUIRE(1 == tokens.at(0).uint_value());

      REQUIRE(bk::TokenType::MUL == tokens.at(1).type());

      REQUIRE(bk::TokenType::UINT == tokens.at(2).type());
      REQUIRE(2 == tokens.at(2).uint_value());

      REQUIRE(bk::TokenType::ADD == tokens.at(3).type());

      REQUIRE(bk::TokenType::UINT == tokens.at(4).type());
      REQUIRE(3 == tokens.at(4).uint_value());

      REQUIRE(bk::TokenType::SUB == tokens.at(5).type());

      REQUIRE(bk::TokenType::UINT == tokens.at(6).type());
      REQUIRE(4 == tokens.at(6).uint_value());

      REQUIRE(bk::TokenType::DIV == tokens.at(7).type());

      REQUIRE(bk::TokenType::UINT == tokens.at(8).type());
      REQUIRE(5 == tokens.at(8).uint_value());
    }

    SECTION("all_operations_on_float")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("1.2 * 2.4 + 3.6 - 4.8 / 5.10");
      
      REQUIRE(9 == tokens.size());
      
      REQUIRE(bk::TokenType::FLOAT == tokens.at(0).type());
      REQUIRE(1.2f == tokens.at(0).float_value());

      REQUIRE(bk::TokenType::MUL == tokens.at(1).type());

      REQUIRE(bk::TokenType::FLOAT == tokens.at(2).type());
      REQUIRE(2.4f == tokens.at(2).float_value());

      REQUIRE(bk::TokenType::ADD == tokens.at(3).type());

      REQUIRE(bk::TokenType::FLOAT == tokens.at(4).type());
      REQUIRE(3.6f == tokens.at(4).float_value());

      REQUIRE(bk::TokenType::SUB == tokens.at(5).type());

      REQUIRE(bk::TokenType::FLOAT == tokens.at(6).type());
      REQUIRE(4.8f == tokens.at(6).float_value());

      REQUIRE(bk::TokenType::DIV == tokens.at(7).type());

      REQUIRE(bk::TokenType::FLOAT == tokens.at(8).type());
      REQUIRE(5.10f == tokens.at(8).float_value());
    }
}


TEST_CASE("lexer_numbers_mod", "[LexerNumbersArith]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("one_mod")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("9 % 35");

      REQUIRE(3 == tokens.size());
      
      REQUIRE(bk::TokenType::INT == tokens.at(0).type());
      REQUIRE(9 == tokens.at(0).int_value());

      REQUIRE(bk::TokenType::MOD == tokens.at(1).type());

      REQUIRE(bk::TokenType::INT == tokens.at(2).type());
      REQUIRE(35 == tokens.at(2).int_value());
    }
}

TEST_CASE("lexer_numbers_par", "[LexerNumbersArith]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("one_mod")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("(3 + 4) * 6");

      REQUIRE(7 == tokens.size());

      REQUIRE(bk::TokenType::OPEN_PAR == tokens.at(0).type());
      
      REQUIRE(bk::TokenType::INT == tokens.at(1).type());
      REQUIRE(3 == tokens.at(1).int_value());

      REQUIRE(bk::TokenType::ADD == tokens.at(2).type());

      REQUIRE(bk::TokenType::INT == tokens.at(3).type());
      REQUIRE(4 == tokens.at(3).int_value());

      REQUIRE(bk::TokenType::CLOSE_PAR == tokens.at(4).type());
	    
      REQUIRE(bk::TokenType::MUL == tokens.at(5).type());
	    
      REQUIRE(bk::TokenType::INT == tokens.at(6).type());
      REQUIRE(6 == tokens.at(6).int_value());
    }
}
