#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_inference", "[LexerInference]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("lexer_inference_simple")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("i := 5;");
      REQUIRE(4 == tokens.size());

      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::INFOP == tokens.at(1).type());
      REQUIRE(bk::TokenType::INT == tokens.at(2).type());
      REQUIRE(bk::TokenType::SEMICOLON == tokens.at(3).type());
    }
}
