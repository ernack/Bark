#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_string", "[LexerStrings]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("lexer_empty_string")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("\"\"");

      REQUIRE(1 == tokens.size());
      REQUIRE(bk::TokenType::STRING == tokens.back().type());
      REQUIRE("" == tokens.back().string_value());
    }

    SECTION("lexer_one_char_string")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("\"h\"");

      REQUIRE(1 == tokens.size());
      REQUIRE(bk::TokenType::STRING == tokens.back().type());
      REQUIRE("h" == tokens.back().string_value());
    }

    SECTION("lexer_multi_chars_string")
      {
	REQUIRE(0 == tokens.size());
	tokens = lexer("\"hello world\"");

	REQUIRE(1 == tokens.size());
	REQUIRE(bk::TokenType::STRING == tokens.back().type());
	REQUIRE("hello world" == tokens.back().string_value());
      }
}
