#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_while", "[LexerWhile]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("lexer_while")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("while 5 < 3 { }");
      REQUIRE(6 == tokens.size());

      REQUIRE(bk::TokenType::WHILE == tokens.at(0).type());
      REQUIRE(bk::TokenType::INT == tokens.at(1).type());
      REQUIRE(bk::TokenType::LT == tokens.at(2).type());
      REQUIRE(bk::TokenType::INT == tokens.at(3).type());
      REQUIRE(bk::TokenType::OPEN_BRACE == tokens.at(4).type());
      REQUIRE(bk::TokenType::CLOSE_BRACE == tokens.at(5).type());
    }

  SECTION("lexer_while_without_open_brace_space")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("while 5 < 3{ }");
      REQUIRE(6 == tokens.size());

      REQUIRE(bk::TokenType::WHILE == tokens.at(0).type());
      REQUIRE(bk::TokenType::INT == tokens.at(1).type());
      REQUIRE(bk::TokenType::LT == tokens.at(2).type());
      REQUIRE(bk::TokenType::INT == tokens.at(3).type());
      REQUIRE(bk::TokenType::OPEN_BRACE == tokens.at(4).type());
      REQUIRE(bk::TokenType::CLOSE_BRACE == tokens.at(5).type());
    }
}
