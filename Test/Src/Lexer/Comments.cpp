#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_comments", "[LexerComments]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("lexer_line_comment")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer(" ;; 45");
      REQUIRE(0 == tokens.size());
    }

    SECTION("lexer_line_double_comment")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer(" ;; ;; 45");
      REQUIRE(0 == tokens.size());
    }
}
