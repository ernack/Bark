#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_numbers_int", "[LexerNumbers]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("one_digit_int")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("1");

      REQUIRE(1 == tokens.size());
      REQUIRE(bk::TokenType::INT == tokens.back().type());
      REQUIRE(1 == tokens.back().int_value());
    }

  SECTION("two_digits_int")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("37");

      REQUIRE(1 == tokens.size());
      REQUIRE(bk::TokenType::INT == tokens.back().type());
      REQUIRE(37 == tokens.back().int_value());
    }

  SECTION("three_digits_negative_int")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("-273");

      REQUIRE(1 == tokens.size());
      REQUIRE(bk::TokenType::INT == tokens.back().type());
      REQUIRE(-273 == tokens.back().int_value());
    }
}

TEST_CASE("lexer_numbers_uint", "[LexerNumbers]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("one_digit_uint")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("7u");

      REQUIRE(1 == tokens.size());
      REQUIRE(bk::TokenType::UINT == tokens.back().type());
      REQUIRE(7 == tokens.back().uint_value());
    }

  SECTION("three_digit_uint")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("404u");

      REQUIRE(1 == tokens.size());
      REQUIRE(bk::TokenType::UINT == tokens.back().type());
      REQUIRE(404 == tokens.back().uint_value());
    }
}

TEST_CASE("lexer_numbers_float", "[LexerNumbers]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("positive_float")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("3.14159");

      REQUIRE(1 == tokens.size());
      REQUIRE(bk::TokenType::FLOAT == tokens.back().type());
      REQUIRE(3.14159f == tokens.back().float_value());
    }

  SECTION("negative_float")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("-2.73");

      REQUIRE(1 == tokens.size());
      REQUIRE(bk::TokenType::FLOAT == tokens.back().type());
      REQUIRE(-2.73f == tokens.back().float_value());
    }
}
