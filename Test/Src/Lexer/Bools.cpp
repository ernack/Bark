#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_bool", "[LexerBools]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("true")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("true");

      REQUIRE(1 == tokens.size());
      REQUIRE(bk::TokenType::BOOL == tokens.at(0).type());
      REQUIRE(true == tokens.at(0).bool_value());
    }

  SECTION("false")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("false");

      REQUIRE(1 == tokens.size());
      REQUIRE(bk::TokenType::BOOL == tokens.at(0).type());
      REQUIRE(false == tokens.at(0).bool_value());
    }
}
