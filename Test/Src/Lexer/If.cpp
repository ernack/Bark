#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_if", "[LexerIf]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  tokens = lexer("if 1 < 2 { } else {}");
  
  REQUIRE(9 == tokens.size());      
  REQUIRE(bk::TokenType::IF == tokens.at(0).type());
  REQUIRE(bk::TokenType::INT == tokens.at(1).type());
  REQUIRE(bk::TokenType::LT == tokens.at(2).type());
  REQUIRE(bk::TokenType::INT == tokens.at(3).type());
  REQUIRE(bk::TokenType::OPEN_BRACE == tokens.at(4).type());
  REQUIRE(bk::TokenType::CLOSE_BRACE == tokens.at(5).type());
  REQUIRE(bk::TokenType::ELSE == tokens.at(6).type());
  REQUIRE(bk::TokenType::OPEN_BRACE == tokens.at(7).type());
  REQUIRE(bk::TokenType::CLOSE_BRACE == tokens.at(8).type());
}
