#include <iostream>
#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_ident", "[LexerIdent]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("lexer_ident_alphanum")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("Salutations84");

      REQUIRE(1 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE("Salutations84" == tokens.at(0).string_value());
    }

  SECTION("lexer_ident_underscore")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("_Salutations84");

      REQUIRE(1 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE("_Salutations84" == tokens.at(0).string_value());
    }

    SECTION("lexer_ident_num_inside")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("Salut337ations84");

      REQUIRE(1 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE("Salut337ations84" == tokens.at(0).string_value());
    }

    SECTION("lexer_ident_cannot_start_with_digit")
    {
      REQUIRE(0 == tokens.size());
      REQUIRE_THROWS(tokens = lexer("32Salut337ations84"));
    }
}
