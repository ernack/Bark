#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_var_decl_with_type", "[LexerVarDecl]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("lexer_var_decl_with_type_int")
    {
      REQUIRE(0 == tokens.size());
      
      tokens = lexer("x : int = 3");

      REQUIRE(5 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(1).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(2).type());
      REQUIRE(bk::TokenType::ASSIGN == tokens.at(3).type());
      REQUIRE(bk::TokenType::INT == tokens.at(4).type());	    
    }

  SECTION("lexer_var_decl_with_type_uint")
    {
      REQUIRE(0 == tokens.size());
      
      tokens = lexer("y : uint = 5u");

      REQUIRE(5 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(1).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(2).type());
      REQUIRE(bk::TokenType::ASSIGN == tokens.at(3).type());
      REQUIRE(bk::TokenType::UINT == tokens.at(4).type());	    
    }

  SECTION("lexer_var_decl_with_type_float")
    {
      REQUIRE(0 == tokens.size());
      
      tokens = lexer("x : float = 3.7");

      REQUIRE(5 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(1).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(2).type());
      REQUIRE(bk::TokenType::ASSIGN == tokens.at(3).type());
      REQUIRE(bk::TokenType::FLOAT == tokens.at(4).type());	    
    }

  SECTION("lexer_var_decl_with_type_string")
    {
      REQUIRE(0 == tokens.size());
      
      tokens = lexer("z : string = \"coucou\"");

      REQUIRE(5 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(1).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(2).type());
      REQUIRE(bk::TokenType::ASSIGN == tokens.at(3).type());
      REQUIRE(bk::TokenType::STRING == tokens.at(4).type());	    
    }

  SECTION("lexer_var_decl_with_type_bool")
    {
      REQUIRE(0 == tokens.size());
      
      tokens = lexer("b : bool = false");

      REQUIRE(5 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(1).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(2).type());
      REQUIRE(bk::TokenType::ASSIGN == tokens.at(3).type());
      REQUIRE(bk::TokenType::BOOL == tokens.at(4).type());	    
    }
}
