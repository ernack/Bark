#include <iostream>
#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_function", "[LexerFunction]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("lexer_simple")
    {      
      tokens = lexer("(x : int, y : int){ return x + y }");
      REQUIRE(15 == tokens.size());
      REQUIRE(bk::TokenType::OPEN_PAR == tokens.at(0).type());
      REQUIRE(bk::TokenType::IDENT == tokens.at(1).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(2).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(3).type());
      REQUIRE(bk::TokenType::COMMA == tokens.at(4).type());
      REQUIRE(bk::TokenType::IDENT == tokens.at(5).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(6).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(7).type());
      REQUIRE(bk::TokenType::CLOSE_PAR == tokens.at(8).type());
      REQUIRE(bk::TokenType::OPEN_BRACE == tokens.at(9).type());
      REQUIRE(bk::TokenType::RETURN == tokens.at(10).type());
      REQUIRE(bk::TokenType::IDENT == tokens.at(11).type());
      REQUIRE(bk::TokenType::ADD == tokens.at(12).type());
      REQUIRE(bk::TokenType::IDENT == tokens.at(13).type());
      REQUIRE(bk::TokenType::CLOSE_BRACE == tokens.at(14).type());
    }

  SECTION("lexer_error")
    {

      tokens = lexer("(x : int, y : int){ error 4; }");
      REQUIRE(14 == tokens.size());
      
      REQUIRE(bk::TokenType::OPEN_PAR == tokens.at(0).type());
      REQUIRE(bk::TokenType::IDENT == tokens.at(1).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(2).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(3).type());
      REQUIRE(bk::TokenType::COMMA == tokens.at(4).type());
      REQUIRE(bk::TokenType::IDENT == tokens.at(5).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(6).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(7).type());
      REQUIRE(bk::TokenType::CLOSE_PAR == tokens.at(8).type());
      REQUIRE(bk::TokenType::OPEN_BRACE == tokens.at(9).type());
      REQUIRE(bk::TokenType::ERROR == tokens.at(10).type());
      REQUIRE(bk::TokenType::INT == tokens.at(11).type());
      REQUIRE(bk::TokenType::SEMICOLON == tokens.at(12).type());
      REQUIRE(bk::TokenType::CLOSE_BRACE == tokens.at(13).type());
    }

  SECTION("lexer_ref")
    {
      tokens = lexer("(x : &int, y : int){ error 4; }");
      REQUIRE(15 == tokens.size());
      
      REQUIRE(bk::TokenType::OPEN_PAR == tokens.at(0).type());
      REQUIRE(bk::TokenType::IDENT == tokens.at(1).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(2).type());
      REQUIRE(bk::TokenType::REF == tokens.at(3).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(4).type());
      REQUIRE(bk::TokenType::COMMA == tokens.at(5).type());
      REQUIRE(bk::TokenType::IDENT == tokens.at(6).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(7).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(8).type());
      REQUIRE(bk::TokenType::CLOSE_PAR == tokens.at(9).type());
      REQUIRE(bk::TokenType::OPEN_BRACE == tokens.at(10).type());
      REQUIRE(bk::TokenType::ERROR == tokens.at(11).type());
      REQUIRE(bk::TokenType::INT == tokens.at(12).type());
      REQUIRE(bk::TokenType::SEMICOLON == tokens.at(13).type());
      REQUIRE(bk::TokenType::CLOSE_BRACE == tokens.at(14).type());
    }

  SECTION("lexer_cref")
    {
      tokens = lexer("(x : &&int, y : int){ error 4; }");
      REQUIRE(15 == tokens.size());
      
      REQUIRE(bk::TokenType::OPEN_PAR == tokens.at(0).type());
      REQUIRE(bk::TokenType::IDENT == tokens.at(1).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(2).type());
      REQUIRE(bk::TokenType::AND == tokens.at(3).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(4).type());
      REQUIRE(bk::TokenType::COMMA == tokens.at(5).type());
      REQUIRE(bk::TokenType::IDENT == tokens.at(6).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(7).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(8).type());
      REQUIRE(bk::TokenType::CLOSE_PAR == tokens.at(9).type());
      REQUIRE(bk::TokenType::OPEN_BRACE == tokens.at(10).type());
      REQUIRE(bk::TokenType::ERROR == tokens.at(11).type());
      REQUIRE(bk::TokenType::INT == tokens.at(12).type());
      REQUIRE(bk::TokenType::SEMICOLON == tokens.at(13).type());
      REQUIRE(bk::TokenType::CLOSE_BRACE == tokens.at(14).type());
    }

  SECTION("lexer_function_def0")
    {
      tokens = lexer("x : (int)int = (i:int) { return i; };");
      REQUIRE(15 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(1).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(2).type());
      REQUIRE("(int)int" == tokens.at(2).value().s);
    }

  SECTION("lexer_function_def1")
    {
      tokens = lexer("x : (int) = (i:int) { return i; };");
      REQUIRE(15 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(1).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(2).type());
      REQUIRE("(int)" == tokens.at(2).value().s);
    }

    SECTION("lexer_function_def2")
    {
      tokens = lexer("x : () = (i:int) { return i; };");
      REQUIRE(15 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(1).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(2).type());
      REQUIRE("()" == tokens.at(2).value().s);
    }

    SECTION("lexer_function_def3")
    {
      tokens = lexer("x : (int,float,string) = (i:int) { return i; };");
      REQUIRE(15 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(1).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(2).type());
      REQUIRE("(int,float,string)" == tokens.at(2).value().s);
    }

    SECTION("lexer_function_def4")
    {
      tokens = lexer("x : (int,float,string)uint = (i:int) { return i; };");
      REQUIRE(15 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(1).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(2).type());
      REQUIRE("(int,float,string)uint" == tokens.at(2).value().s);
    }

    SECTION("lexer_function_def5")
    {
      tokens = lexer("x : (int,float,&string)uint = (i:int) { return i; };");
      REQUIRE(15 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(1).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(2).type());
      REQUIRE("(int,float,&string)uint" == tokens.at(2).value().s);
    }

    SECTION("lexer_function_def6")
    {
      tokens = lexer("x : (int,&&float,string)uint = (i:int) { return i; };");
      REQUIRE(15 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(1).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(2).type());
      REQUIRE("(int,&&float,string)uint" == tokens.at(2).value().s);
    }
    
    SECTION("lexer_function_nested_function_type")
    {
      tokens = lexer("x : (int,(int)int)uint = (i:int) { return i; };");
      REQUIRE(15 == tokens.size());
      
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::COLON == tokens.at(1).type());
      REQUIRE(bk::TokenType::TYPE == tokens.at(2).type());
      REQUIRE("(int,(int)int)uint" == tokens.at(2).value().s);
    }

    SECTION("lexer_function_ref_to_function")
      {
	tokens = lexer("f := (g : &(int)int) { return 3; }");

	REQUIRE(13 == tokens.size());
      
	REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
	REQUIRE(bk::TokenType::INFOP == tokens.at(1).type());
	REQUIRE(bk::TokenType::OPEN_PAR == tokens.at(2).type());
	REQUIRE(bk::TokenType::IDENT == tokens.at(3).type());
	REQUIRE(bk::TokenType::COLON == tokens.at(4).type());
	REQUIRE(bk::TokenType::REF == tokens.at(5).type());
	REQUIRE(bk::TokenType::TYPE == tokens.at(6).type());
	REQUIRE(bk::TokenType::CLOSE_PAR == tokens.at(7).type());
	
	REQUIRE("(int)int" == tokens.at(6).value().s);
      }

    SECTION("lexer_function_const_ref_to_function")
      {
	tokens = lexer("f := (g : &&(int)int) { return 3; }");

	REQUIRE(13 == tokens.size());
      
	REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
	REQUIRE(bk::TokenType::INFOP == tokens.at(1).type());
	REQUIRE(bk::TokenType::OPEN_PAR == tokens.at(2).type());
	REQUIRE(bk::TokenType::IDENT == tokens.at(3).type());
	REQUIRE(bk::TokenType::COLON == tokens.at(4).type());
	REQUIRE(bk::TokenType::AND == tokens.at(5).type());
	REQUIRE(bk::TokenType::TYPE == tokens.at(6).type());
	REQUIRE(bk::TokenType::CLOSE_PAR == tokens.at(7).type());
	
	REQUIRE("(int)int" == tokens.at(6).value().s);
      }
}
