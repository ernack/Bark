#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_bool_arith", "[LexerBoolsArith]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  REQUIRE(0 == tokens.size());
  tokens = lexer("true && !false || true");

  REQUIRE(6 == tokens.size());
      
  REQUIRE(bk::TokenType::BOOL == tokens.at(0).type());
  REQUIRE(true == tokens.at(0).bool_value());

  REQUIRE(bk::TokenType::AND == tokens.at(1).type());

  REQUIRE(bk::TokenType::NOT == tokens.at(2).type());

  REQUIRE(bk::TokenType::BOOL == tokens.at(3).type());
  REQUIRE(false == tokens.at(3).bool_value());

  REQUIRE(bk::TokenType::OR == tokens.at(4).type());
  
  REQUIRE(bk::TokenType::BOOL == tokens.at(5).type());
  REQUIRE(true == tokens.at(5).bool_value());
}
