#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_assign", "[LexerAssign]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("one_digit_int")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("x = 42");

      REQUIRE(3 == tokens.size());

      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());

      REQUIRE(bk::TokenType::ASSIGN == tokens.at(1).type());

      REQUIRE(bk::TokenType::INT == tokens.at(2).type());
    }
}
