#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_funcall", "[LexerFunCall]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("lexer_funcall_simple")
    {      
      tokens = lexer("x()");
      REQUIRE(3 == tokens.size());
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::OPEN_PAR == tokens.at(1).type());
      REQUIRE(bk::TokenType::CLOSE_PAR == tokens.at(2).type());
    }

  SECTION("lexer_funcall_one_arg")
    {      
      tokens = lexer("x(4)");
      REQUIRE(4 == tokens.size());
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::OPEN_PAR == tokens.at(1).type());
      REQUIRE(bk::TokenType::INT == tokens.at(2).type());
      REQUIRE(bk::TokenType::CLOSE_PAR == tokens.at(3).type());
    }

    SECTION("lexer_funcall_more_args")
    {      
      tokens = lexer("x(4, 6u, 9.3)");
      REQUIRE(8 == tokens.size());
      REQUIRE(bk::TokenType::IDENT == tokens.at(0).type());
      REQUIRE(bk::TokenType::OPEN_PAR == tokens.at(1).type());      
      REQUIRE(bk::TokenType::INT == tokens.at(2).type());
      REQUIRE(bk::TokenType::COMMA == tokens.at(3).type());      
      REQUIRE(bk::TokenType::UINT == tokens.at(4).type());
      REQUIRE(bk::TokenType::COMMA == tokens.at(5).type());
      REQUIRE(bk::TokenType::FLOAT == tokens.at(6).type());      
      REQUIRE(bk::TokenType::CLOSE_PAR == tokens.at(7).type());
    }
}
