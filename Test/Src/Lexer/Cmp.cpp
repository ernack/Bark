#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_cmp", "[LexerCmp]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("lexer_cmp_lt")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("3 < 5");

      REQUIRE(3 == tokens.size());
      
      REQUIRE(bk::TokenType::INT == tokens.at(0).type());
      REQUIRE(bk::TokenType::LT == tokens.at(1).type());
      REQUIRE(bk::TokenType::INT == tokens.at(2).type());            
    }
  SECTION("lexer_cmp_gt")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("3 > 5");
      
      REQUIRE(3 == tokens.size());
      
      REQUIRE(bk::TokenType::INT == tokens.at(0).type());
      REQUIRE(bk::TokenType::GT == tokens.at(1).type());
      REQUIRE(bk::TokenType::INT == tokens.at(2).type());            
    }

  SECTION("lexer_cmp_eq")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("3 == 5");

      REQUIRE(3 == tokens.size());
      
      REQUIRE(bk::TokenType::INT == tokens.at(0).type());
      REQUIRE(bk::TokenType::EQ == tokens.at(1).type());
      REQUIRE(bk::TokenType::INT == tokens.at(2).type());            
    }

  SECTION("lexer_cmp_neq")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("3 != 5");

      REQUIRE(3 == tokens.size());
      
      REQUIRE(bk::TokenType::INT == tokens.at(0).type());
      REQUIRE(bk::TokenType::NEQ == tokens.at(1).type());
      REQUIRE(bk::TokenType::INT == tokens.at(2).type());            
    }

  SECTION("lexer_cmp_le")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("3 <= 5");

      REQUIRE(3 == tokens.size());
      
      REQUIRE(bk::TokenType::INT == tokens.at(0).type());
      REQUIRE(bk::TokenType::LE == tokens.at(1).type());
      REQUIRE(bk::TokenType::INT == tokens.at(2).type());            
    }

  SECTION("lexer_cmp_ge")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("3 >= 5");

      REQUIRE(3 == tokens.size());
      
      REQUIRE(bk::TokenType::INT == tokens.at(0).type());
      REQUIRE(bk::TokenType::GE == tokens.at(1).type());
      REQUIRE(bk::TokenType::INT == tokens.at(2).type());            
    }
}
