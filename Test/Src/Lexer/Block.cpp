#include <vector>
#include <catch.hpp>
#include "../../../Lib/Src/Lexer.hpp"
#include "../../../Lib/Src/Token.hpp"

TEST_CASE("lexer_block", "[LexerBlock]")
{
  bk::Lexer lexer;
  std::vector<bk::Token> tokens;

  SECTION("lexer_block_simple")
    {
      REQUIRE(0 == tokens.size());
      tokens = lexer("{ 4; 5 }");

      REQUIRE(5 == tokens.size());

      REQUIRE(bk::TokenType::OPEN_BRACE == tokens.at(0).type());

      REQUIRE(bk::TokenType::INT == tokens.at(1).type());

      REQUIRE(bk::TokenType::SEMICOLON == tokens.at(2).type());

      REQUIRE(bk::TokenType::INT == tokens.at(3).type());

      REQUIRE(bk::TokenType::CLOSE_BRACE == tokens.at(4).type());
    }
}
