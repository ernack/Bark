cmake_minimum_required(VERSION 3.5)

project(BarkTest)

file(GLOB_RECURSE
  src
  Src/*.cpp)

link_directories("${CMAKE_BINARY_DIR}/Lib")

add_executable(test ${src})

target_link_libraries(test -lbark)
