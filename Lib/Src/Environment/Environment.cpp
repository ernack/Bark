#include <cassert>
#include <iostream>
#include "Environment.hpp"
#include "../Natives/Write.hpp"

namespace bk
{

  void InnerEnv::print()
  {
    if (prev)
      {
	prev->print();
      }
    
    std::cout << "---------------- " << this << std::endl;
    for (auto const& p : records)
      {
	std::cout << p.first << " : " << token_type_to_string(p.second->type) << std::endl;
      }
  }
  
  std::unique_ptr<TokenValue> InnerEnv::get_value(std::string const& name) const
  {
    auto itr = records.find(name);
    if (itr == records.end())
      {
	if (prev) { return prev->get_value(name); }
	return nullptr;
      }

    if (records.at(name)->is_ref || records.at(name)->is_const)
      {
	return get_value(records.at(name)->value.s);
      }
    else
      {
	return std::make_unique<TokenValue>(records.at(name)->value);
      }
  }

  std::unique_ptr<TokenType> InnerEnv::get_type(std::string const& name) const
  {
    auto itr = records.find(name);
    if (itr == records.end())
      {
	if (prev) { return prev->get_type(name); }
	return nullptr;
      }

    if (records.at(name)->is_ref || records.at(name)->is_const)
      {
	return get_type(records.at(name)->value.s);
      }
    else
      {
	return std::make_unique<TokenType>(records.at(name)->type);
      }
  }

  EnvEntry InnerEnv::get_entry(std::string const& name) const
  {
    auto itr = records.find(name);
    if (itr == records.end())
      {
	if (prev) { return prev->get_entry(name); }
	throw std::runtime_error("Env entry " + name + " not found.");
      }

    return *records.at(name);
  }
  
  bool InnerEnv::set_value(std::string const& name, TokenValue value)
  {
    auto itr = records.find(name);

    if (itr == records.end())
      {
	if (prev)
	  {
	    return prev->set_value(name, value);
	  }
	  
	return false;
      }

    if (records[name]->is_ref || records.at(name)->is_const)
      {
	set_value(records[name]->value.s, value);
      }
    else
      {       
	records[name]->value = value;
      }
    
    return true;
  }

  bool InnerEnv::set_ref(std::string const& name, TokenValue value)
  {
        auto itr = records.find(name);

    if (itr == records.end())
      {
	if (prev)
	  {
	    return prev->set_value(name, value);
	  }
	  
	return false;
      }

    records[name]->value = value;    
    return true;
  }

  void InnerEnv::close_block()
  {
    
  }

  size_t InnerEnv::size() const
  {
    size_t sz = records.size();
    if (prev)
      {
	sz += prev->size();
      }

    return sz;
  }
  
  /*explicit*/ Environment::Environment()
  {
    open_block();        
  }

  void Environment::print()
  {
    std::cout << "################" << std::endl;
    if (m_inner_env)
      {
	m_inner_env->print();
      }
  }
  
  bool Environment::declare(std::string const& name,
			    TokenType type,
			    TokenValue value,
			    bool is_ref /* = false */,			    
			    bool is_const /* = false */)
  {    
    if (m_inner_env)
      {
	auto itr = m_inner_env->records.find(name);
	if (itr != m_inner_env->records.end())
	  {
	    return false;
	  }

	EnvEntry entry;
	entry.type = type;
	entry.value = value;
	entry.is_ref = is_ref || is_const;
	entry.is_const = is_const;
	m_inner_env->records[name] = std::make_unique<EnvEntry>(entry);
	return true;
      }

    return false;
  }

  std::unique_ptr<TokenValue> Environment::get_value(std::string const& name) const
  {
    if (!m_inner_env) { return nullptr; }
    return m_inner_env->get_value(name);
  }

  bool Environment::set_value(std::string const& name, TokenValue value)
  {
    if (!m_inner_env) { return false; }
    return m_inner_env->set_value(name, value);
  }

  bool Environment::set_ref(std::string const& name, TokenValue value)
  {
    if (!m_inner_env) { return false; }
    return m_inner_env->set_ref(name, value);
  }
  
  std::unique_ptr<TokenType> Environment::get_type(std::string const& name) const
  {
    if (!m_inner_env) { return nullptr; }
    return m_inner_env->get_type(name);
  }

  EnvEntry Environment::get_entry(std::string const& name) const
  {
    return m_inner_env->get_entry(name);
  }

  size_t Environment::size() const
  {
    if (!m_inner_env) { return 0; }
    return m_inner_env->size();
  }

  void Environment::open_block()
  {
    auto new_block = std::make_unique<InnerEnv>();

    if (m_inner_env)
      {
	new_block->prev = std::move(m_inner_env);
	m_inner_env = std::move(new_block);
      }
    else
      {
	m_inner_env = std::move(new_block);
      }
  }
  
  void Environment::close_block()
  {
    if (m_inner_env && m_inner_env->prev)
      {
	auto p = std::move(m_inner_env->prev);
        m_inner_env = std::make_unique<InnerEnv>();
	m_inner_env->prev = std::move(p);	
      }
    else
      {
	m_inner_env = std::make_unique<InnerEnv>();
      }
  }
  
  /*virtual*/ Environment::~Environment()
  {
    close_block();
  }
}
