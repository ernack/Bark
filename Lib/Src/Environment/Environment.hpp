#ifndef ENVIRONMENT_HPP
#define ENVIRONMENT_HPP
#include <iostream>
#include <memory>
#include <unordered_map>
#include "../TokenType.hpp"
#include "../TokenValue.hpp"

namespace bk
{
  struct EnvEntry
  {
    TokenType type;
    TokenValue value;
    bool is_ref = false;
    bool is_const = false;
  };
  
  struct InnerEnv
  {
    void print();
    std::unique_ptr<TokenValue> get_value(std::string const& name) const;
    std::unique_ptr<TokenType> get_type(std::string const& name) const;
    EnvEntry get_entry(std::string const& name) const;
    bool set_value(std::string const& name, TokenValue value);
    bool set_ref(std::string const& name, TokenValue value);
    void close_block();
    size_t size() const;
    size_t depth() const { if (prev) { return 1 + prev->depth(); } else { return 0; } }

    std::unordered_map<std::string, std::unique_ptr<EnvEntry>> records;
    std::unique_ptr<InnerEnv> prev = nullptr;
  };
  
  class Environment
  {
  public:
    explicit Environment();
    void print();
    bool declare(std::string const& name,
		 TokenType type,
		 TokenValue value,
		 bool is_ref = false,
		 bool is_const = false);
    
    std::unique_ptr<TokenValue> get_value(std::string const& name) const;
    bool set_value(std::string const& name, TokenValue value);
    bool set_ref(std::string const& name, TokenValue value);
    std::unique_ptr<TokenType> get_type(std::string const& name) const;
    EnvEntry get_entry(std::string const& name) const;
    
    void open_block();
    void close_block();
    
    size_t size() const;
    
    virtual ~Environment();
    Environment& operator=(Environment const& environment) = delete;
    Environment(Environment const& environment) = delete;
  private:
    std::unique_ptr<InnerEnv> m_inner_env;
  };
}
#endif // ENVIRONMENT

