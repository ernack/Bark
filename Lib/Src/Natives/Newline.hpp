#ifndef NEWLINE_HPP
#define NEWLINE_HPP

#include "../Function.hpp"
#include "../Environment/Environment.hpp"
#include "../TokenValue.hpp"

namespace bk
{
  class Newline : public Function
  {
  public:
    explicit Newline(Environment& env)
      : Function(env)
    {
      set_return_type(TokenType::BOOL);
      
      set_callback([&env]() -> TokenValue {
		     std::cout << std::endl;
		     return token_value(true);
		   });

    }
    
    virtual ~Newline()
    {
    }
    
    Newline& operator=(Newline const& newline) = delete;
    Newline(Newline const& newline) = delete;
  private:
  };
}
#endif // NEWLINE

