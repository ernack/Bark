#ifndef NATIVEENVIRONMENT_HPP
#define NATIVEENVIRONMENT_HPP

#include "../TokenValue.hpp"
#include "../Environment/Environment.hpp"

#include "Write.hpp"
#include "Read.hpp"
#include "Newline.hpp"

namespace bk
{
  class NativeEnvironment : public Environment
  {
  public:
    explicit NativeEnvironment()
    {
      open_block();
      
      native_loader<WriteInt>("_write_int");
      native_loader<WriteUint>("_write_uint");
      native_loader<WriteFloat>("_write_float");
      native_loader<WriteBool>("_write_bool");
      native_loader<WriteString>("_write_string");

      native_loader<Newline>("_newline");

      native_loader<ReadInt>("_read_int");
      native_loader<ReadUint>("_read_uint");
      native_loader<ReadFloat>("_read_float");
      native_loader<ReadString>("_read_string");
      
    }

    template<typename T>
    void native_loader(std::string name)
    {
      auto fct = std::make_shared<T>(*this);
      TokenValue val;
      val.function = fct;
      declare(name, TokenType::FUNCTION, val);
    }

    virtual ~NativeEnvironment() {}
    NativeEnvironment& operator=(NativeEnvironment const& nativeenvironment) = delete;
    NativeEnvironment(NativeEnvironment const& nativeenvironment) = delete;
  private:
  };
}
#endif // NATIVEENVIRONMENT

