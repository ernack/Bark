#ifndef READ_HPP
#define READ_HPP

#include <string>

#include "../Function.hpp"
#include "../Environment/Environment.hpp"
#include "../TokenValue.hpp"

namespace bk
{
  class ReadInt : public Function
  {
  public:
    explicit ReadInt(Environment& env)
      : Function(env)
    {
      set_return_type(TokenType::INT);
      
      set_callback([&env]() -> TokenValue {
		     int tmp;
		     std::cin >> tmp;
		     return token_value(tmp);
		   });
    }

    
    virtual ~ReadInt()
    {
    }
    ReadInt& operator=(ReadInt const& read) = delete;
    ReadInt(ReadInt const& read) = delete;
  private:
  };

  class ReadUint : public Function
  {
  public:
    explicit ReadUint(Environment& env)
      : Function(env)
    {
      set_return_type(TokenType::UINT);
      
      set_callback([&env]() -> TokenValue {
		     unsigned int tmp;
		     std::cin >> tmp;
		     return token_value(tmp);
		   });
    }

    
    virtual ~ReadUint()
    {
    }
    ReadUint& operator=(ReadUint const& read) = delete;
    ReadUint(ReadUint const& read) = delete;
  private:
  };

  class ReadFloat : public Function
  {
  public:
    explicit ReadFloat(Environment& env)
      : Function(env)
    {
      set_return_type(TokenType::FLOAT);
      
      set_callback([&env]() -> TokenValue {
		     float tmp;
		     std::cin >> tmp;
		     return token_value(tmp);
		   });
    }

    
    virtual ~ReadFloat()
    {
    }
    ReadFloat& operator=(ReadFloat const& read) = delete;
    ReadFloat(ReadFloat const& read) = delete;
  private:
  };

  class ReadString : public Function
  {
  public:
    explicit ReadString(Environment& env)
      : Function(env)
    {
      set_return_type(TokenType::STRING);
      
      set_callback([&env]() -> TokenValue {
		     std::string tmp;
		     std::cin >> tmp;
		     return token_value(tmp);
		   });
    }

    
    virtual ~ReadString()
    {
    }
    ReadString& operator=(ReadString const& read) = delete;
    ReadString(ReadString const& read) = delete;
  private:
  };
}
#endif // READ

