#ifndef WRITE_HPP
#define WRITE_HPP

#include "../Function.hpp"
#include "../Environment/Environment.hpp"
#include "../TokenValue.hpp"

namespace bk
{
  class Environment;
  
  class WriteInt : public Function
  {
  public:
    explicit WriteInt(Environment& env)
      : Function(env)
    {
      FunctionParam p;
      
      p.name = "input";
      p.type = TokenType::INT;
      add_param(p);
      
      set_return_type(TokenType::BOOL);
      
      set_callback([&env]() -> TokenValue {
		     auto val = env.get_value("input");
		     std::cout << val->i;
		     return token_value(true);
		   });
    }
    
    virtual ~WriteInt() {}
    WriteInt& operator=(WriteInt const& write_int) = delete;
    WriteInt(WriteInt const& write_int) = delete;
  private:    
  };


  class WriteUint : public Function
  {
  public:
    explicit WriteUint(Environment& env)
      : Function(env)
    {
      FunctionParam p;
      
      p.name = "input";
      p.type = TokenType::UINT;
      add_param(p);
      
      set_return_type(TokenType::BOOL);
      
      set_callback([&env]() -> TokenValue {
		     auto val = env.get_value("input");
		     std::cout << val->ui;
		     return token_value(true);
		   });
    }
    
    virtual ~WriteUint() {}
    WriteUint& operator=(WriteUint const& write_int) = delete;
    WriteUint(WriteUint const& write_int) = delete;
  private:    
  };

  class WriteFloat : public Function
  {
  public:
    explicit WriteFloat(Environment& env)
      : Function(env)
    {
      FunctionParam p;
      
      p.name = "input";
      p.type = TokenType::FLOAT;
      add_param(p);
      
      set_return_type(TokenType::BOOL);
      
      set_callback([&env]() -> TokenValue {
		     auto val = env.get_value("input");
		     std::cout << val->f;
		     return token_value(true);
		   });
    }
    
    virtual ~WriteFloat() {}
    WriteFloat& operator=(WriteFloat const& write_int) = delete;
    WriteFloat(WriteFloat const& write_int) = delete;
  private:    
  };

  class WriteBool : public Function
  {
  public:
    explicit WriteBool(Environment& env)
      : Function(env)
    {
      FunctionParam p;
      
      p.name = "input";
      p.type = TokenType::BOOL;
      add_param(p);
      
      set_return_type(TokenType::BOOL);
      
      set_callback([&env]() -> TokenValue {
		     auto val = env.get_value("input");
		     std::cout << std::boolalpha << val->b;
		     return token_value(true);
		   });
    }
    
    virtual ~WriteBool() {}
    WriteBool& operator=(WriteBool const& write_int) = delete;
    WriteBool(WriteBool const& write_int) = delete;
  private:    
  };

    class WriteString : public Function
  {
  public:
    explicit WriteString(Environment& env)
      : Function(env)
    {
      FunctionParam p;
      
      p.name = "input";
      p.type = TokenType::STRING;
      add_param(p);
      
      set_return_type(TokenType::BOOL);
      
      set_callback([&env]() -> TokenValue {
		     auto val = env.get_value("input");
		     std::cout << val->s;
		     return token_value(true);
		   });
    }
    
    virtual ~WriteString() {}
    WriteString& operator=(WriteString const& write_int) = delete;
    WriteString(WriteString const& write_int) = delete;
  private:    
  };
}
#endif // WRITE_INT

