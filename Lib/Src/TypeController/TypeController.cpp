#include <cassert>
#include <iostream>
#include <algorithm>
#include "TypeController.hpp"
#include "../TreeNode.hpp"
#include "../Environment/Environment.hpp"
#include "../Function.hpp"
#include "../Token.hpp"

namespace bk
{
  /*explicit*/ TypeController::TypeController(Environment& env)
    : m_env { env }
  {
  }

  std::string TypeController::message() const
  {
    return m_message;
  }
  
  bool TypeController::check(TreeNode const& treenode)
  {
    switch (treenode.type())
      {
      case TreeNodeType::BARK: return check_bark(treenode); break;
      case TreeNodeType::ADD: return check_add(treenode); break;
      case TreeNodeType::SUB: return check_sub(treenode); break;
      case TreeNodeType::MUL: return check_mul(treenode); break;
      case TreeNodeType::DIV: return check_div(treenode); break;
      case TreeNodeType::MOD: return check_mod(treenode); break;
      case TreeNodeType::INT: return check_int(treenode); break;
      case TreeNodeType::UINT: return check_uint(treenode); break;
      case TreeNodeType::FLOAT: return check_float(treenode); break;
      case TreeNodeType::STRING: return check_string(treenode); break;
      case TreeNodeType::BOOL: return check_bool(treenode); break;
      case TreeNodeType::OR: return check_or(treenode); break;
      case TreeNodeType::AND: return check_and(treenode); break;
      case TreeNodeType::NOT: return check_not(treenode); break;
      case TreeNodeType::LT: return check_lt(treenode); break;
      case TreeNodeType::LE: return check_le(treenode); break;
      case TreeNodeType::GT: return check_gt(treenode); break;
      case TreeNodeType::GE: return check_ge(treenode); break;
      case TreeNodeType::NEQ: return check_neq(treenode); break;
      case TreeNodeType::EQ: return check_eq(treenode); break;
      case TreeNodeType::VAR_DECL: return check_var_decl(treenode); break;
      case TreeNodeType::IDENT: return check_ident(treenode); break;
      case TreeNodeType::ASSIGN: return check_assign(treenode); break;
      case TreeNodeType::BLOCK: return check_block(treenode); break;
      case TreeNodeType::FUNCTION: return check_function(treenode); break;
      case TreeNodeType::FUNCALL: return check_funcall(treenode); break;
      case TreeNodeType::RETURN: return check_return(treenode); break;
      case TreeNodeType::ERROR: return check_error(treenode); break;
      case TreeNodeType::IF: return check_if(treenode); break;
      case TreeNodeType::WHILE: return check_while(treenode); break;
      default: throw std::runtime_error("TypeController: unknown node " +
					treenode.child(0).string());
      }    
  }
  
  bool TypeController::check_bark(TreeNode const& treenode)
  {    
    for(size_t i=0; i<treenode.size(); i++)
      {
        if (!check(treenode.child(i)))
	  {
	    return false;
	  }
      }
    
    return true;
  }

  bool TypeController::check_assign(TreeNode const& treenode)
  {
    std::string ident_name = treenode.child(0).value()->s;

    auto itr = m_references.find(ident_name);
    if (itr != m_references.end() && itr->second)
      {
	m_message = ident_name + " is const.";
	return false;
      }

    bool a0 = check(treenode.child(0));

    if (!a0)
      {
	return false;
      }
    
    auto ident_type = m_type_stack.back();
    m_type_stack.pop_back();

    bool a1 = check(treenode.child(1));
    
    if (!a1)
      {
	return false;
      }
    
    auto val_type = m_type_stack.back();
    
    return ident_type == val_type;
  }
  
  bool TypeController::check_var_decl(TreeNode const& treenode)
  {
    std::string name = treenode.child(0).value()->s;
    size_t index = 2;
    TokenType type;
    std::string type_str;

    if (treenode.size() == 2)
      {
	index = 1;
	check(treenode.child(1));

	TreeNodeType t = m_type_stack.back();
	m_type_stack.pop_back();	
	type_str = tree_node_type_to_string(t);


	std::transform(type_str.begin(), type_str.end(), type_str.begin(), tolower);
	
	if (type_str == "function")
	  {
	    Function f {treenode.child(1), m_env};
	    type_str = f.type_string();
	  }
	
      }
    else
      {
	type_str = treenode.child(1).value()->s;
      }
    
    auto val = treenode.child(index).value();
    
    if (treenode.child(index).type() == TreeNodeType::BLOCK)
      {
	auto& v = treenode.child(index);
	val = v.child(v.size() - 1).value();
      }

    else if (treenode.child(index).type() == TreeNodeType::FUNCTION)
      {
	auto function = std::make_shared<Function>(treenode.child(index), m_env);
			
	TokenValue val;
	val.function = function;

	m_env.declare(name, TokenType::FUNCTION, val);
	
        if (!check(treenode.child(index)))
	  {
	    m_message = "Function " + name + " type mismatch : got " +
	      function->type_string() +
	      ", expected " +
	      type_str;

	    return false;
	  }
	
	return type_str == function->type_string();
      }

    else if (treenode.child(index).type() == TreeNodeType::FUNCALL)
      {
	TreeNode const& funcall = treenode.child(index);

	if (!check(funcall))
	  {
	    return false;
	  }
	
	std::string fct_name = funcall.child(0).value()->s;

	std::shared_ptr<Function> fct = m_env.get_value(fct_name)->function;

	std::string fct_type_str = "";
	if (fct->has_return())
	  {
	    TokenType t = fct->return_type();
	    fct_type_str = token_type_to_string(t);
	    std::transform(fct_type_str.begin(),
			   fct_type_str.end(),
			   fct_type_str.begin(),
			   tolower);
	  }        
	
	if (type_str != fct_type_str) { return false; }
	
	m_env.declare(name, fct->return_type(), token_value(0)); 
      }
    else
      {
	check(treenode.child(index));
	TreeNode tn {m_type_stack.back()};
	TokenType tt = token_type_from_tree_node(tn);
	m_env.declare(name, tt, token_value(0));
      }
    
    bool res = check(treenode.child(index));
    
    if (val)
      {
	TokenValue value = *val;

	if (type_str == "int") { type = TokenType::INT; }
	else if (type_str == "uint") { type = TokenType::UINT; }
	else if (type_str == "float") { type = TokenType::FLOAT; }
	else if (type_str == "bool") { type = TokenType::BOOL; }
	else if (type_str == "string") { type = TokenType::STRING; }
	else { throw std::runtime_error("Type Check var decl type error : " + type_str); }
	
	m_env.declare(name, type, value);
	
	TreeNodeType node_type = m_type_stack.back();

	if (type_str == "int" && node_type != TreeNodeType::INT)
	  {
	    return false;
	  }

	if (type_str == "uint" && node_type != TreeNodeType::UINT)
	  {
	    return false;
	  }

	if (type_str == "float" && node_type != TreeNodeType::FLOAT)
	  {
	    return false;
	  }

	if (type_str == "bool" && node_type != TreeNodeType::BOOL)
	  {
	    return false;
	  }

	if (type_str == "string" && node_type != TreeNodeType::STRING)
	  {
	    return false;
	  }
	
	return true;
      }

    return res;
  }
  
  bool TypeController::check_or(TreeNode const& treenode)
  {
    bool lhs = check(treenode.child(0));
    auto lhs_type = m_type_stack.back();
    m_type_stack.pop_back();
    
    bool rhs = check(treenode.child(1));
    auto rhs_type = m_type_stack.back();
    m_type_stack.pop_back();

    m_type_stack.push_back(lhs_type);
    
    bool res = lhs && rhs &&
      lhs_type == rhs_type &&
      lhs_type == TreeNodeType::BOOL;

    if (!res) { m_message = "OR type mismatch"; }
    return res;
  }
  
  bool TypeController::check_and(TreeNode const& treenode)
  {
    bool lhs = check(treenode.child(0));
    auto lhs_type = m_type_stack.back();
    m_type_stack.pop_back();
    
    bool rhs = check(treenode.child(1));
    auto rhs_type = m_type_stack.back();
    m_type_stack.pop_back();

    m_type_stack.push_back(lhs_type);
	
    bool res = lhs && rhs &&
      lhs_type == rhs_type &&
      lhs_type == TreeNodeType::BOOL;

    if (!res) { m_message = "AND type mismatch"; }
    return res;
  }
  
  bool TypeController::check_add(TreeNode const& treenode)
  {
    bool lhs = check(treenode.child(0));
    auto lhs_type = m_type_stack.back();
    m_type_stack.pop_back();
    
    bool rhs = check(treenode.child(1));
    auto rhs_type = m_type_stack.back();
    m_type_stack.pop_back();

    m_type_stack.push_back(lhs_type);
    
    bool res = lhs && rhs &&
      lhs_type == rhs_type &&
      (lhs_type == TreeNodeType::IDENT ||
       lhs_type == TreeNodeType::INT ||
       lhs_type == TreeNodeType::UINT ||
       lhs_type == TreeNodeType::FLOAT);


    if (!res) { m_message = "ADD type mismatch"; }
    return res;
  }
  
  bool TypeController::check_sub(TreeNode const& treenode)
  {
    bool lhs = check(treenode.child(0));
    auto lhs_type = m_type_stack.back();
    m_type_stack.pop_back();
    
    bool rhs = check(treenode.child(1));
    auto rhs_type = m_type_stack.back();
    m_type_stack.pop_back();

    m_type_stack.push_back(lhs_type);
    
    bool res = lhs && rhs &&
      lhs_type == rhs_type &&
      (lhs_type == TreeNodeType::INT ||
       lhs_type == TreeNodeType::UINT ||
       lhs_type == TreeNodeType::FLOAT);

    if (!res) { m_message = "SUB type mismatch"; }
    return res;
  }
  
  bool TypeController::check_mul(TreeNode const& treenode)
  {
    bool lhs = check(treenode.child(0));
    auto lhs_type = m_type_stack.back();
    m_type_stack.pop_back();
    
    bool rhs = check(treenode.child(1));
    auto rhs_type = m_type_stack.back();
    m_type_stack.pop_back();

    m_type_stack.push_back(lhs_type);
    
    bool res = lhs && rhs &&
      lhs_type == rhs_type &&
      (lhs_type == TreeNodeType::INT ||
       lhs_type == TreeNodeType::UINT ||
       lhs_type == TreeNodeType::FLOAT);

    if (!res) { m_message = "MUL type mismatch"; }
    return res;
  }
  
  bool TypeController::check_div(TreeNode const& treenode)
  {
    bool lhs = check(treenode.child(0));
    auto lhs_type = m_type_stack.back();
    m_type_stack.pop_back();
    
    bool rhs = check(treenode.child(1));
    auto rhs_type = m_type_stack.back();
    m_type_stack.pop_back();

    m_type_stack.push_back(lhs_type);
	
    bool res = lhs && rhs &&
      lhs_type == rhs_type &&
      (lhs_type == TreeNodeType::INT ||
       lhs_type == TreeNodeType::UINT ||
       lhs_type == TreeNodeType::FLOAT);

    if (!res) { m_message = "DIV type mismatch"; }
    return res;
  }
  
  bool TypeController::check_mod(TreeNode const& treenode)
  {
    bool lhs = check(treenode.child(0));
    auto lhs_type = m_type_stack.back();
    m_type_stack.pop_back();
    
    bool rhs = check(treenode.child(1));
    auto rhs_type = m_type_stack.back();
    m_type_stack.pop_back();

    m_type_stack.push_back(lhs_type);
    
    bool res = lhs && rhs &&
      lhs_type == rhs_type &&
      (lhs_type == TreeNodeType::INT ||
       lhs_type == TreeNodeType::UINT);

    if (!res) { m_message = "MOD type mismatch"; }
    return res;
  }
  
  bool TypeController::check_not(TreeNode const& treenode)
  {
    bool lhs = check(treenode.child(0));
    auto lhs_type = m_type_stack.back();
    m_type_stack.pop_back();

    m_type_stack.push_back(lhs_type);
    
    bool res = lhs && lhs_type == TreeNodeType::BOOL;

    if (!res) { m_message = "NOT type mismatch"; }
    return res;
  }
  
  bool TypeController::check_int(TreeNode const& treenode)
  {
    m_type_stack.push_back(TreeNodeType::INT);
    return true;
  }
  
  bool TypeController::check_uint(TreeNode const& treenode)
  {
    m_type_stack.push_back(TreeNodeType::UINT);
    return true;
  }
  
  bool TypeController::check_float(TreeNode const& treenode)
  {
    m_type_stack.push_back(TreeNodeType::FLOAT);
    return true;
  }
  
  bool TypeController::check_string(TreeNode const& treenode)
  {
    m_type_stack.push_back(TreeNodeType::STRING);
    return true;
  }
  
  bool TypeController::check_bool(TreeNode const& treenode)
  {
    m_type_stack.push_back(TreeNodeType::BOOL);
    return true;
  }
  
  bool TypeController::check_lt(TreeNode const& treenode)
  {
    bool lhs = check(treenode.child(0));
    auto lhs_type = m_type_stack.back();
    m_type_stack.pop_back();
    
    bool rhs = check(treenode.child(1));
    auto rhs_type = m_type_stack.back();
    m_type_stack.pop_back();

    m_type_stack.push_back(TreeNodeType::BOOL);
    
    bool res = lhs && rhs &&
      lhs_type == rhs_type &&
      (lhs_type == TreeNodeType::UINT ||
       lhs_type == TreeNodeType::INT ||
       lhs_type == TreeNodeType::FLOAT ||
       lhs_type == TreeNodeType::IDENT
       );

    if (!res) { m_message = "< type mismatch"; }
    return res;
  }
  
  bool TypeController::check_le(TreeNode const& treenode)
  {
    bool lhs = check(treenode.child(0));
    auto lhs_type = m_type_stack.back();
    m_type_stack.pop_back();
    
    bool rhs = check(treenode.child(1));
    auto rhs_type = m_type_stack.back();
    m_type_stack.pop_back();

    m_type_stack.push_back(TreeNodeType::BOOL);
    
    bool res = lhs && rhs &&
      lhs_type == rhs_type &&
      (lhs_type == TreeNodeType::UINT ||
       lhs_type == TreeNodeType::INT ||
       lhs_type == TreeNodeType::FLOAT ||
       lhs_type == TreeNodeType::IDENT
       );
    if (!res) { m_message = "< type mismatch"; }
    return res;
  }
  
  bool TypeController::check_gt(TreeNode const& treenode)
  {
    bool lhs = check(treenode.child(0));
    auto lhs_type = m_type_stack.back();
    m_type_stack.pop_back();
    
    bool rhs = check(treenode.child(1));
    auto rhs_type = m_type_stack.back();
    m_type_stack.pop_back();

    m_type_stack.push_back(TreeNodeType::BOOL);
    
    bool res = lhs && rhs &&
      lhs_type == rhs_type &&
      (lhs_type == TreeNodeType::UINT ||
       lhs_type == TreeNodeType::INT ||
       lhs_type == TreeNodeType::FLOAT ||
       lhs_type == TreeNodeType::IDENT
       );
    
    if (!res) { m_message = "> type mismatch"; }
    return res;
  }
  
  bool TypeController::check_ge(TreeNode const& treenode)
  {
    bool lhs = check(treenode.child(0));
    auto lhs_type = m_type_stack.back();
    m_type_stack.pop_back();
    
    bool rhs = check(treenode.child(1));
    auto rhs_type = m_type_stack.back();
    m_type_stack.pop_back();

    m_type_stack.push_back(TreeNodeType::BOOL);
    
    bool res = lhs && rhs &&
      lhs_type == rhs_type &&
      (lhs_type == TreeNodeType::UINT ||
       lhs_type == TreeNodeType::INT ||
       lhs_type == TreeNodeType::FLOAT ||
       lhs_type == TreeNodeType::IDENT
       );
    
    if (!res) { m_message = ">= type mismatch"; }
    return res;
  }
  
  bool TypeController::check_eq(TreeNode const& treenode)
  {
    bool lhs = check(treenode.child(0));
    auto lhs_type = m_type_stack.back();
    m_type_stack.pop_back();
    
    bool rhs = check(treenode.child(1));
    auto rhs_type = m_type_stack.back();
    m_type_stack.pop_back();

    m_type_stack.push_back(TreeNodeType::BOOL);
    
    bool res = lhs && rhs &&
      lhs_type == rhs_type &&
      (lhs_type == TreeNodeType::UINT ||
       lhs_type == TreeNodeType::INT ||
       lhs_type == TreeNodeType::FLOAT ||
       lhs_type == TreeNodeType::BOOL ||
       lhs_type == TreeNodeType::STRING ||
       lhs_type == TreeNodeType::IDENT);

    if (!res) { m_message = "== type mismatch"; }
    return res;
  }
  
  bool TypeController::check_neq(TreeNode const& treenode)
  {
    bool lhs = check(treenode.child(0));
    auto lhs_type = m_type_stack.back();
    m_type_stack.pop_back();
    
    bool rhs = check(treenode.child(1));
    auto rhs_type = m_type_stack.back();
    m_type_stack.pop_back();

    m_type_stack.push_back(TreeNodeType::BOOL);

    bool res = lhs && rhs &&
      lhs_type == rhs_type;

    if (!res) { m_message = "!= type mismatch"; }
    return res;
  }
  
  bool TypeController::check_ident(TreeNode const& treenode)
  {

    std::string name = treenode.value()->s;
    
    std::unique_ptr<TokenType> type = m_env.get_type(name);
    
    if (!type)
      {
	m_message = name + " is not defined.";
	return false;
      }

    switch (*type)
      {
      case TokenType::INT: m_type_stack.push_back(TreeNodeType::INT); break;
      case TokenType::UINT: m_type_stack.push_back(TreeNodeType::UINT); break;
      case TokenType::FLOAT: m_type_stack.push_back(TreeNodeType::FLOAT); break;
      case TokenType::STRING: m_type_stack.push_back(TreeNodeType::STRING); break;
      case TokenType::BOOL: m_type_stack.push_back(TreeNodeType::BOOL); break;
      case TokenType::FUNCTION: m_type_stack.push_back(TreeNodeType::FUNCTION); break;
      default: throw std::runtime_error("TypeController ident type error " +
					token_type_to_string(*type)); break;
      }
    
    auto val = m_env.get_value(name);
    
    Token tok;
    tok.set_type(*type);
    
    m_type_stack.push_back(tree_node_type_from_token(tok));
    
    return true;
  }

  bool TypeController::check_block(TreeNode const& treenode)
  {
    m_env.open_block();
    
    for(size_t i=0; i<treenode.size(); i++)
      {
	if (!check(treenode.child(i)))
	  {
	    return false;
	  }
      }

    m_env.close_block();

    return true;
  }

  bool TypeController::check_function(TreeNode const& treenode)
  {
    std::shared_ptr<Function> f = std::make_shared<Function>(treenode, m_env);

    if (treenode.child(0).type() != TreeNodeType::TYPE)
      {
	if ( f->has_return() )
	  {

	    m_message = "Function Missing output type.";
	    return false;
	  }
      }

    if (treenode.child(0).type() == TreeNodeType::TYPE)
      {
	std::string given_type = treenode.child(0).value()->s;
	std::string current_type = f->return_type_string();
	
	if (current_type.empty() && f->has_return())
	  {
	    current_type = token_type_to_string(f->return_type());
	    std::transform(current_type.begin(), current_type.end(),
			   current_type.begin(), tolower);
	  }

	if (f->has_error() && current_type == "int")
	  {

	  }	
	else if (given_type != current_type)
	  {
	    m_message = "Function definition output type error.";

	    return false;
	  }
      }
    
    assert(f);

    bool res = true;    
    
    for(size_t i=0; i<f->params_count(); i++)
      {
    	FunctionParam param = f->param(i);

	if (param.pass_by != FunctionParamType::VALUE)
	  {
	    m_references[param.name] = param.pass_by == FunctionParamType::CREF;
	  }

    	if (param.type == TokenType::FUNCTION)
    	  {
    	    auto function = std::make_shared<Function>(m_env);
    	    TokenValue val;
    	    val.function = function;
	    

    	    m_env.declare(param.name, param.type, val);
    	  }
    	else
    	  {
    	    m_env.declare(param.name,
    			  param.type,
    			  token_value(std::string("")));
    	  }
      }

    if ( f->has_body() )
      {
	TreeNode const& body = f->body();
	Function* prev = nullptr;
	if (m_current_function) { prev = m_current_function; }
	m_current_function = f.get();
	
	res = check(body);
	
	if (prev) { m_current_function = prev; }
	else { m_current_function = nullptr; }
      }

    if (f->has_return())
      {
    	Token tok {};
    	tok.set_type(f->return_type());
      }

    m_type_stack.push_back(TreeNodeType::FUNCTION);

    return res;
  }

  bool TypeController::check_funcall(TreeNode const& treenode)
  {
    std::string name = treenode.child(0).value()->s;
    
    std::unique_ptr<TokenValue> value = m_env.get_value(name);

    if (!value)
      {
	if (m_current_function && m_current_function->has_return())
	  {
	    Token tok;
	    tok.set_type(m_current_function->return_type());
	    
	    auto t = tree_node_type_from_token(tok);
	    m_type_stack.push_back(t);
	    return true;
	  }
	return false;
      }
    
    if (value && value->function->params_count() != treenode.size() - 1)
      {
	m_message = "Bad argument number in function " + name;
	return false;
      }

    std::vector<TokenType> types;
    
    for(size_t i=1; i<treenode.size(); i++)
      {
        check(treenode.child(i));
        TreeNode t {m_type_stack.back()};
	m_type_stack.pop_back();
	
	types.push_back(token_type_from_tree_node(t));
      }

    for(size_t i=0; i < value->function->params_count(); i++)
      {
	FunctionParam param = value->function->param(i);
	TokenType arg_type = types.at(i);	
	
	if (param.type != arg_type)
	  {
	    m_message = "function " + name + " call error : " +
	      "expected " +
	      token_type_to_string(param.type) + ", got " +
	      token_type_to_string(arg_type) +
	      ".";

	    return false;
	  }
	else if (param.type == TokenType::FUNCTION)
	  {
	    std::string fname = treenode.child(i+1).value()->s;

	    auto f = m_env.get_value(fname)->function;
	    std::string type_0 = param.type_string;
	    std::string type_1 = f->type_string();
	    
	    if (type_0 != type_1)
	      {
		m_message = "function " + name + " : argument error, expected " + f->type_string() +
		  ", got " + param.type_string;

		return false;
	      }
	  }
      }
    Token t;

    if (value->function->has_return())
      {
	t.set_type(value->function->return_type());
	m_type_stack.push_back(tree_node_type_from_token(t));
      }

    return true;
  }

  bool TypeController::check_return(TreeNode const& treenode)
  {
    if (!m_current_function)
      {
    	m_message = "Return only allowed within functions.";
    	return false;
      }

    if (m_current_function && m_current_function->has_return())
      {

	if(!check(treenode.child(0)))
	  {
	    m_message = "Return : invalid expression.";
	    return false;
	  }
	
	TreeNodeType type = m_type_stack.back();
	Token tok;
	tok.set_type(m_current_function->return_type());
	
	if (type != tree_node_type_from_token(tok))
	  {
	    return false;
	  }
      }
    
    return true;
  }

  bool TypeController::check_if(TreeNode const& treenode)
  {
    if (treenode.child(0).type() == TreeNodeType::ERROR)
      {
	return true;
      }
    else
      {
	check(treenode.child(0));

	TreeNodeType t = m_type_stack.back();
	m_type_stack.pop_back();
    
	return t == TreeNodeType::BOOL;
      }
  }

  bool TypeController::check_while(TreeNode const& treenode)
  {
    if (treenode.size() < 2)
      {
	m_message = "Empty while not allowed.";
	return false;
      }
    
    check(treenode.child(0));
    
    TreeNodeType t = m_type_stack.back();
    m_type_stack.pop_back();
    
    return t == TreeNodeType::BOOL;
  }

  bool TypeController::check_error(TreeNode const& treenode)
  {
    check(treenode.child(0));
    
    TreeNodeType t = m_type_stack.back();
    m_type_stack.pop_back();
    
    return t == TreeNodeType::INT;
  }
  
  /*virtual*/ TypeController::~TypeController()
  {
  }
}
