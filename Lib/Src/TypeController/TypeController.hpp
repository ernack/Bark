#ifndef TYPECONTROLLER_HPP
#define TYPECONTROLLER_HPP
#include <unordered_map>
#include <vector>
#include "../TreeNodeType.hpp"

namespace bk
{
  class TreeNode;
  class Environment;
  class Function;
  
  class TypeController
  {
  public:
    explicit TypeController(Environment& env);

    std::string message() const;
    
    bool check(TreeNode const& treenode);
    bool check_bark(TreeNode const& treenode);
    bool check_assign(TreeNode const& treenode);
    bool check_var_decl(TreeNode const& treenode);
    bool check_or(TreeNode const& treenode);
    bool check_and(TreeNode const& treenode);
    bool check_add(TreeNode const& treenode);
    bool check_sub(TreeNode const& treenode);
    bool check_mul(TreeNode const& treenode);
    bool check_div(TreeNode const& treenode);
    bool check_mod(TreeNode const& treenode);
    bool check_not(TreeNode const& treenode);
    bool check_int(TreeNode const& treenode);
    bool check_uint(TreeNode const& treenode);
    bool check_float(TreeNode const& treenode);
    bool check_string(TreeNode const& treenode);
    bool check_bool(TreeNode const& treenode);

    bool check_lt(TreeNode const& treenode);
    bool check_le(TreeNode const& treenode);
    bool check_gt(TreeNode const& treenode);
    bool check_ge(TreeNode const& treenode);
    bool check_eq(TreeNode const& treenode);
    bool check_neq(TreeNode const& treenode);

    bool check_ident(TreeNode const& treenode);
    bool check_block(TreeNode const& treenode);

    bool check_function(TreeNode const& treenode);
    bool check_funcall(TreeNode const& treenode);
    bool check_return(TreeNode const& treenode);
    bool check_if(TreeNode const& treenode);
    bool check_while(TreeNode const& treenode);
    bool check_error(TreeNode const& treenode);
    
    virtual ~TypeController();
    TypeController& operator=(TypeController const& typecontroller) = delete;
    TypeController(TypeController const& typecontroller) = delete;
  private:
    std::vector<TreeNodeType> m_type_stack;
    std::string m_message;
    Environment& m_env;
    std::unordered_map<std::string, bool> m_references;
    std::vector<std::string> m_calls;
    Function* m_current_function = nullptr;
  };
}
#endif // TYPECONTROLLER

