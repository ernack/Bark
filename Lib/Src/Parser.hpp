#ifndef PARSER_HPP
#define PARSER_HPP

#include <vector>
#include <memory>

#include "Token.hpp"

namespace bk
{
  class Lexer;
  class TreeNode;
  
  class Parser
  {
  public:
    explicit Parser(Lexer& lexer);

    std::unique_ptr<TreeNode> operator()(std::string const& source);
    
    virtual ~Parser();
    Parser& operator=(Parser const& parser) = delete;
    Parser(Parser const& parser) = delete;
  private:
    Lexer& m_lexer;
    std::vector<Token> m_tokens;
    size_t m_cursor;

    bool match_token(TokenType token_type);
    
    std::unique_ptr<TreeNode> BARK();
    std::unique_ptr<TreeNode> WHILE();
    std::unique_ptr<TreeNode> IF();
    std::unique_ptr<TreeNode> RETURN();
    std::unique_ptr<TreeNode> ASSIGN();
    std::unique_ptr<TreeNode> VAR_DECL();
    std::unique_ptr<TreeNode> OR();
    std::unique_ptr<TreeNode> AND();
    std::unique_ptr<TreeNode> EQ();
    std::unique_ptr<TreeNode> CMP();
    std::unique_ptr<TreeNode> ADD_SUB();
    std::unique_ptr<TreeNode> MUL_DIV();
    std::unique_ptr<TreeNode> NOT();
    std::unique_ptr<TreeNode> LITERAL();
    std::unique_ptr<TreeNode> BLOCK();
    std::unique_ptr<TreeNode> FUNCTION();
    std::unique_ptr<TreeNode> PARAMS();
    std::unique_ptr<TreeNode> PARAM();
    std::unique_ptr<TreeNode> FUNCALL();

  };
}
#endif // PARSER

