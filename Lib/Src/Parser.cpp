#include <cassert>
#include <iostream>
#include "Parser.hpp"
#include "Lexer.hpp"
#include "TreeNode.hpp"

namespace bk
{
  /*explicit*/ Parser::Parser(Lexer& lexer)
    : m_lexer { lexer }
  {
  }
  
  std::unique_ptr<TreeNode> Parser::operator()(std::string const& source)
  {
    m_tokens = m_lexer(source);
    m_cursor = 0;
    
    return BARK();
  }
  
  /*virtual*/ Parser::~Parser()
  {
  }

  bool Parser::match_token(TokenType token_type)
  {
    if (m_cursor >= m_tokens.size()) { return false; }
    
    if (m_tokens[m_cursor].type() == token_type)
      {
	m_cursor++;
	return true;
      }

    return false;
  }
  
  std::unique_ptr<TreeNode> Parser::BARK()
  {
    std::vector<std::unique_ptr<TreeNode>> nodes;
    
    while(m_cursor < m_tokens.size())
      {
	auto next = WHILE();

	if (!next) { break; }
	nodes.push_back(std::move(next));
	match_token(TokenType::SEMICOLON);
      }
      
    if (nodes.size() > 1)
      {
	auto bark = std::make_unique<TreeNode>(TreeNodeType::BARK);
	for(auto& node : nodes)
	  {
	    bark->add_child(std::move(node));
	  }

	return bark;
      }

    if (nodes.empty()) { return nullptr; }
    
    return std::move(nodes.front());    
  }

  std::unique_ptr<TreeNode> Parser::WHILE()
  {
    size_t saved = m_cursor;

    auto my_while = std::make_unique<TreeNode>(TreeNodeType::WHILE);
    
    if ( !match_token(TokenType::WHILE) )
      {	
	return IF();
      }

    auto condition = WHILE();
    my_while->add_child(std::move(condition));

    if ( !match_token(TokenType::OPEN_BRACE) )
      {
	m_cursor = saved;
	return nullptr;
      }

    auto my_do = BARK();

    if (my_do)
      {
	my_while->add_child(std::move(my_do));
      }
    
    if ( !match_token(TokenType::CLOSE_BRACE) )
      {
	m_cursor = saved;
	return nullptr;
      }
    
    return my_while;
  }
  
  std::unique_ptr<TreeNode> Parser::IF()
  {
    size_t saved = m_cursor;

    auto my_if = std::make_unique<TreeNode>(TreeNodeType::IF);
    
    if ( !match_token(TokenType::IF) )
      {	
	return RETURN();
      }

    auto condition = IF();
    my_if->add_child(std::move(condition));

    if ( !match_token(TokenType::OPEN_BRACE) )
      {
	m_cursor = saved;
	return nullptr;
      }

    auto then = BARK();

    my_if->add_child(std::move(then));
    
    if ( !match_token(TokenType::CLOSE_BRACE) )
      {
	m_cursor = saved;
	return nullptr;
      }
        
    if ( match_token(TokenType::ELSE) )
      {
	if ( !match_token(TokenType::OPEN_BRACE) )
	  {
	    m_cursor = saved;
	    return nullptr;
	  }

	auto my_else = BARK();

	if ( !match_token(TokenType::CLOSE_BRACE) )
	  {
	    m_cursor = saved;
	    return nullptr;
	  }

	my_if->add_child(std::move(my_else));
      }
    
    return my_if;
  }
  
  std::unique_ptr<TreeNode> Parser::RETURN()
  {
    size_t saved = m_cursor;

    if (match_token(TokenType::RETURN))
      {
	auto ret = std::make_unique<TreeNode>(TreeNodeType::RETURN);
	ret->add_child(ASSIGN());
	return ret;
      }

    else if (match_token(TokenType::ERROR))
      {
	auto ret = std::make_unique<TreeNode>(TreeNodeType::ERROR);
	ret->add_child(ASSIGN());
	return ret;
      }

    return ASSIGN();
  }
  
  std::unique_ptr<TreeNode> Parser::ASSIGN()
  {
    size_t saved = m_cursor;
    
    if ( match_token(TokenType::IDENT) && // -2
	 match_token(TokenType::ASSIGN)) // -1
      {
	auto assign = std::make_unique<TreeNode>(TreeNodeType::ASSIGN);

	TokenValue ident_val = m_tokens.at(m_cursor - 2).value();
	auto ident = std::make_unique<TreeNode>(TreeNodeType::IDENT,
						std::make_unique<TokenValue>(ident_val));
	assign->add_child(std::move(ident));
	assign->add_child(VAR_DECL());
	return assign;
      }

    m_cursor = saved;
    return VAR_DECL();
  }
  
  std::unique_ptr<TreeNode> Parser::VAR_DECL()
  {
    size_t saved = m_cursor;
    
    if ( match_token(TokenType::IDENT) && // -4
	 match_token(TokenType::COLON) && // -3
	 match_token(TokenType::TYPE) && // -2
	 match_token(TokenType::ASSIGN)) // -1
      {
	auto tk_ident = m_tokens.at(m_cursor - 4);
	auto tk_type = m_tokens.at(m_cursor - 2);
	
	auto ident = std::make_unique<TreeNode>(TreeNodeType::IDENT,
						std::make_unique<TokenValue>(tk_ident.value()));
	
	auto type = std::make_unique<TreeNode>(TreeNodeType::TYPE,
					       std::make_unique<TokenValue>(tk_type.value()));
		
	auto var_decl = std::make_unique<TreeNode>(TreeNodeType::VAR_DECL);
	var_decl->add_child(std::move(ident));
	var_decl->add_child(std::move(type));

	auto body = OR();
	
	var_decl->add_child(std::move(body));
	return var_decl;
      }

    m_cursor = saved;
    
    if ( match_token(TokenType::IDENT) && // -2
	 match_token(TokenType::INFOP) ) // -1
      {
	auto tk_ident = m_tokens.at(m_cursor - 2);
	auto ident = std::make_unique<TreeNode>(TreeNodeType::IDENT,
						std::make_unique<TokenValue>(tk_ident.value()));
	
		
	auto var_decl = std::make_unique<TreeNode>(TreeNodeType::VAR_DECL);
	var_decl->add_child(std::move(ident));
	auto body = OR();
	
	var_decl->add_child(std::move(body));
	return var_decl;
      }

    m_cursor = saved;
    
    return OR();
  }
  
  std::unique_ptr<TreeNode> Parser::OR()
  {
    auto lhs = AND();

    while (true)
      {	
	if ( match_token(TokenType::OR) )
	  {
	    auto rhs = AND();
	    auto my_or = std::make_unique<TreeNode>(TreeNodeType::OR);

	    my_or->add_child(std::move(lhs));	    
	    my_or->add_child(std::move(rhs));
	    
	    lhs = std::move(my_or);
	  }
	else
	  {
	    break;
	  }
      }
    
    return lhs;
  }
  
  std::unique_ptr<TreeNode> Parser::AND()
  {
    auto lhs = EQ();

    while (true)
      {	
	if ( match_token(TokenType::AND) )
	  {
	    auto rhs = EQ();
	    auto my_and = std::make_unique<TreeNode>(TreeNodeType::AND);

	    my_and->add_child(std::move(lhs));	    
	    my_and->add_child(std::move(rhs));
	    
	    lhs = std::move(my_and);
	  }
	else
	  {
	    break;
	  }
      }
    
    return lhs;
  }

  std::unique_ptr<TreeNode> Parser::EQ()
  {
    auto cmp = CMP();

    if ( match_token(TokenType::EQ) )
      {
	auto node = std::make_unique<TreeNode>(TreeNodeType::EQ);
	node->add_child(std::move(cmp));
	node->add_child(CMP());
	return node;
      }

    if ( match_token(TokenType::NEQ) )
      {
	auto node = std::make_unique<TreeNode>(TreeNodeType::NEQ);
	node->add_child(std::move(cmp));
	node->add_child(CMP());
	return node;
      }

    return cmp;
  }
  
  std::unique_ptr<TreeNode> Parser::CMP()
  {
    auto add_sub = ADD_SUB();

    if ( match_token(TokenType::LT) )
      {
	auto lt = std::make_unique<TreeNode>(TreeNodeType::LT);
	lt->add_child(std::move(add_sub));
	lt->add_child(ADD_SUB());
	return lt;
      }

    if ( match_token(TokenType::LE) )
      {
	auto le = std::make_unique<TreeNode>(TreeNodeType::LE);
	le->add_child(std::move(add_sub));
	le->add_child(ADD_SUB());
	return le;
      }

    if ( match_token(TokenType::GT) )
      {
	auto gt = std::make_unique<TreeNode>(TreeNodeType::GT);
	gt->add_child(std::move(add_sub));
	gt->add_child(ADD_SUB());
	return gt;
      }

    if ( match_token(TokenType::GE) )
      {
	auto ge = std::make_unique<TreeNode>(TreeNodeType::GE);
	ge->add_child(std::move(add_sub));
	ge->add_child(ADD_SUB());
	return ge;
      }
    
    return add_sub;
  }

  std::unique_ptr<TreeNode> Parser::ADD_SUB()
  {
    auto lhs = MUL_DIV();

    while (true)
      {	
	if ( match_token(TokenType::ADD) )
	  {
	    auto rhs = MUL_DIV();
	    auto add = std::make_unique<TreeNode>(TreeNodeType::ADD);

	    add->add_child(std::move(lhs));	    
	    add->add_child(std::move(rhs));
	    
	    lhs = std::move(add);
	  }
	else if ( match_token(TokenType::SUB) )
	  {
	    auto rhs = MUL_DIV();
	    auto sub = std::make_unique<TreeNode>(TreeNodeType::SUB);

	    sub->add_child(std::move(lhs));	    
	    sub->add_child(std::move(rhs));
	    
	    lhs = std::move(sub);
	  }
	else
	  {
	    break;
	  }
      }
    
    return lhs;
  }
  
  std::unique_ptr<TreeNode> Parser::MUL_DIV()
  {
    auto lhs = NOT();

    while (true)
      {	
	if ( match_token(TokenType::MUL) )
	  {
	    auto rhs = NOT();
	    auto mul = std::make_unique<TreeNode>(TreeNodeType::MUL);

	    mul->add_child(std::move(lhs));	    
	    mul->add_child(std::move(rhs));
	    
	    lhs = std::move(mul);
	  }
	else if ( match_token(TokenType::DIV) )
	  {
	    auto rhs = NOT();
	    auto div = std::make_unique<TreeNode>(TreeNodeType::DIV);

	    div->add_child(std::move(lhs));	    
	    div->add_child(std::move(rhs));
	    
	    lhs = std::move(div);
	  }
	else if ( match_token(TokenType::MOD) )
	  {
	    auto rhs = NOT();
	    auto div = std::make_unique<TreeNode>(TreeNodeType::MOD);

	    div->add_child(std::move(lhs));	    
	    div->add_child(std::move(rhs));
	    
	    lhs = std::move(div);
	  }
	else
	  {
	    break;
	  }
      }
    
    return lhs;
  }

  std::unique_ptr<TreeNode> Parser::NOT()
  {
    if ( match_token(TokenType::NOT) )
      {
	auto my_not = std::make_unique<TreeNode>(TreeNodeType::NOT);
	auto literal = LITERAL();
	my_not->add_child(std::move(literal));
	return my_not;
      }

    return LITERAL();
  }
  
  std::unique_ptr<TreeNode> Parser::LITERAL()
  {
    std::unique_ptr<TreeNode> root;

    {
      auto funcall = FUNCALL();
      if (funcall)
	{
	  return funcall;
	}
    }
    
    {
      auto function = FUNCTION();
      if (function)
    	{
    	  return function;
    	}
    }
    
    if (m_tokens.at(m_cursor).type() == TokenType::OPEN_PAR)
      {
	m_cursor++;
	root = BARK();
	assert(m_tokens.at(m_cursor).type() == TokenType::CLOSE_PAR);
	m_cursor++;
	return root;
      }
    
    {
      auto block = BLOCK();
      if (block) { return block; }
    }
    
    switch (m_tokens.at(m_cursor).type())
      {
      case TokenType::INT: m_cursor++;
	{
	  auto val = std::make_unique<TokenValue>();
	  val->i = m_tokens.at(m_cursor - 1).int_value();
	  return std::make_unique<TreeNode>(TreeNodeType::INT, std::move(val));
	}
	
      case TokenType::UINT: m_cursor++;
	{
	  auto val = std::make_unique<TokenValue>();
	  val->ui = m_tokens.at(m_cursor - 1).uint_value();
	  return std::make_unique<TreeNode>(TreeNodeType::UINT, std::move(val));
	}
	
      case TokenType::FLOAT: m_cursor++;
	{
	  auto val = std::make_unique<TokenValue>();
	  val->f = m_tokens.at(m_cursor - 1).float_value();

	  return std::make_unique<TreeNode>(TreeNodeType::FLOAT, std::move(val));
	}
	
      case TokenType::STRING:
	{
	  m_cursor++;
	  auto val = std::make_unique<TokenValue>();
	  val->s = m_tokens.at(m_cursor - 1).string_value();
	  return std::make_unique<TreeNode>(TreeNodeType::STRING, std::move(val));
	}

      case TokenType::BOOL:
	{
	  m_cursor++;
	  auto val = std::make_unique<TokenValue>();
	  val->b = m_tokens.at(m_cursor - 1).bool_value();

	  return std::make_unique<TreeNode>(TreeNodeType::BOOL, std::move(val));
	}

      case TokenType::IDENT:
	{
	  m_cursor++;
	  auto val = std::make_unique<TokenValue>();
	  val->s = m_tokens.at(m_cursor - 1).string_value();
	  return std::make_unique<TreeNode>(TreeNodeType::IDENT, std::move(val));
	}

      default: return nullptr;
      }
  }

  std::unique_ptr<TreeNode> Parser::BLOCK()
  {
    size_t saved = m_cursor;
    
    if ( !match_token(TokenType::OPEN_BRACE) )
      {
	m_cursor = saved;
	return nullptr;
      }
    
    auto root = BARK();
    
    if (!root)
      {
	root = std::make_unique<TreeNode>(TreeNodeType::BLOCK);
      }
    else if (root->type() == TreeNodeType::BARK)
      {	
	root->set_type(TreeNodeType::BLOCK);
      }
    else
      {
	auto tmp = std::make_unique<TreeNode>(TreeNodeType::BLOCK);
	tmp->add_child(std::move(root));
	root = std::move(tmp);
      }
    
    assert(m_tokens.at(m_cursor).type() == TokenType::CLOSE_BRACE);
    m_cursor++;
    return root;
  }

  std::unique_ptr<TreeNode> Parser::FUNCTION()
  {
    size_t saved = m_cursor;
    
    if ( !match_token(TokenType::OPEN_PAR) )
      {
	m_cursor = saved;
	return nullptr;
      }

    auto params = PARAMS();
    if (!params)
      {
    	m_cursor = saved;
    	return nullptr;	
      }

    if ( !match_token(TokenType::CLOSE_PAR) )
      {
	m_cursor = saved;
	return nullptr;
      }

    bool is_returning_void = false;
    
    if (!match_token(TokenType::TYPE))
      {
	is_returning_void = true;
      }

    std::string type_str = m_tokens.at(m_cursor-1).string_value();
    
    if ( !match_token(TokenType::OPEN_BRACE) )
      {
	m_cursor = saved;
	return nullptr;
      }

    auto bark = BARK();

    if ( !match_token(TokenType::CLOSE_BRACE) )
      {
	m_cursor = saved;
	return nullptr;
      }

    auto fct = std::make_unique<TreeNode>(TreeNodeType::FUNCTION);

    if (!is_returning_void)
      {
	auto type_val = std::make_unique<TokenValue>();
	type_val->s = type_str;    
	fct->add_child(std::make_unique<TreeNode>(TreeNodeType::TYPE, std::move(type_val)));
      }
    
    fct->add_child(std::move(params));
    
    if (bark)
      {
        fct->add_child(std::move(bark));
      }
    
    return fct;
  }
  
  std::unique_ptr<TreeNode> Parser::PARAMS()
  {
    size_t saved = m_cursor;

    auto params = std::make_unique<TreeNode>(TreeNodeType::PARAMS);
    
    auto param = PARAM();

    if (!param)
      {
	return std::make_unique<TreeNode>(TreeNodeType::PARAMS);
      }

    params->add_child(std::move(param));
    
    while ( match_token(TokenType::COMMA) )
      {
	auto p = PARAM();
	
	if (p)
	  {
	    params->add_child(std::move(p));
	  }
      }
    
    return params;
  }
  
  std::unique_ptr<TreeNode> Parser::PARAM()
  {
    size_t saved = m_cursor;

    if (!match_token(TokenType::IDENT))
      {
	m_cursor = saved;
	return nullptr;
      }

    auto ident_val = m_tokens.at(m_cursor - 1).value();

    if (!match_token(TokenType::COLON))
      {
	m_cursor = saved;
	return nullptr;
      }

    auto param = std::make_unique<TreeNode>(TreeNodeType::PARAM);
    
    if ( match_token(TokenType::REF) )
      {
	param->set_type(TreeNodeType::PARAM_REF);
      }
    else if ( match_token(TokenType::AND) )
      {
	param->set_type(TreeNodeType::PARAM_CREF);
      }
    
    param->add_child(std::make_unique<TreeNode>
		     (TreeNodeType::IDENT, std::make_unique<TokenValue>(ident_val)));


    if ( !match_token(TokenType::TYPE) )
      {
	m_cursor = saved;
	return nullptr;
      }

    auto type_value = m_tokens.at(m_cursor - 1).value();
    auto type = std::make_unique<TreeNode>(TreeNodeType::TYPE,
					   std::make_unique<TokenValue>(type_value));
    param->add_child(std::move(type));
    
    return param;   
  }

  std::unique_ptr<TreeNode> Parser::FUNCALL()
  {
    size_t saved = m_cursor;

    if (match_token(TokenType::IDENT) && // -2
	match_token(TokenType::OPEN_PAR)) // -1
      {
	auto ident_val = std::make_unique<TokenValue>(m_tokens.at(m_cursor - 2).value());
	
	auto funcall = std::make_unique<TreeNode>(TreeNodeType::FUNCALL);
	funcall->add_child(std::make_unique<TreeNode>(TreeNodeType::IDENT, std::move(ident_val)));
	auto bark = BARK();
	
	if (bark)
	  {
	    funcall->add_child(std::move(bark));

	    while (match_token(TokenType::COMMA))
	      {
		auto b = BARK();
		funcall->add_child(std::move(b));
	      }
	  }

	if (!match_token(TokenType::CLOSE_PAR))
	  {
	    m_cursor = saved;
	    return nullptr;
	  }

	return funcall;
      }

    m_cursor = saved;
    return nullptr;
  }
}
