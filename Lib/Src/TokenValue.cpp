#include <iostream>
#include "TokenValue.hpp"
#include "Function.hpp"

namespace bk
{
  TokenValue token_value(int val)
  {
    TokenValue v;
    v.i = val;
    return v;
  }
  
  TokenValue token_value(unsigned int val)
  {
    TokenValue v;
    v.ui = val;
    return v;
  }
  
  TokenValue token_value(float val)
  {
    TokenValue v;
    v.f = val;
    return v;
  }
  
  TokenValue token_value(std::string const& val)
  {
    TokenValue v;
    v.s = val;
    return v;
  }
  
  TokenValue token_value(bool val)
  {
    TokenValue v;
    v.b = val;
    return v;
  }

}
