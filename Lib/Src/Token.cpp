#include <cassert>
#include <iostream>
#include <sstream>
#include "Token.hpp"

namespace bk
{
  /*explicit*/ Token::Token()
  {
  }

  void Token::set_type(TokenType type)
  {
    m_type = type;
  }
  
  TokenType Token::type() const
  {
    return m_type;
  }

  void Token::set_int_value(int value)
  {
    m_value.i = value;
  }
  
  int Token::int_value() const
  {
    return m_value.i;
  }

  void Token::set_uint_value(unsigned int value)
  {
    m_value.ui = value;
  }
  
  int Token::uint_value() const
  {
    return m_value.ui;
  }

  void Token::set_float_value(float value)
  {
    m_value.f = value;
  }
  
  float Token::float_value() const
  {
    return m_value.f;
  }

  void Token::set_string_value(std::string value)
  {
    m_value.s = value;
  }
  
  std::string Token::string_value() const
  {
    return m_value.s;
  }

  void Token::set_bool_value(bool value)
  {
    m_value.b = value;
  }
  
  bool Token::bool_value() const
  {
    return m_value.b;
  }

  TokenValue Token::value() const
  {
    return m_value;
  }
  
  /*virtual*/ Token::~Token()
  {
  }
}
