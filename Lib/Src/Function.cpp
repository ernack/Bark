#include <functional>
#include <iostream>
#include <cassert>
#include <algorithm>
#include "Function.hpp"
#include "TreeNode.hpp"
#include "TokenValue.hpp"
#include "Environment/Environment.hpp"

namespace bk
{
  /*explicit*/ Function::Function(Environment& env)
    : m_env { env }
  {
  }

  /*explicit*/ Function::Function(TreeNode const& treenode, Environment& env)
    : Function(env)
  {
    load_from_tree_node(treenode);
  }
  
  TokenType Function::return_type() const
  {
    return *m_return_type;
  }

  std::string Function::return_type_string() const
  {
    return m_return_type_string;
  }
  
  std::unique_ptr<TokenType> Function::real_return_type()
  {    
    auto res = real_return_type(m_body.get());
    return res;
  }

  bool Function::has_return() const
  {
    return m_return_type != nullptr;
  }
  
  void Function::add_param(FunctionParam fp)
  {
    m_params.push_back(fp);
  }

  void Function::set_return_type(TokenType type)
  {
    m_return_type = std::make_unique<TokenType>(type);
  }

  int Function::error() const
  {
    assert(m_error);
    return *m_error;
  }

  void Function::set_error(int err)
  {
    m_error = std::make_unique<int>(err);
  }
  
  bool Function::has_error() const
  {
    return m_error != nullptr;
  }

  size_t Function::params_count() const
  {
    return m_params.size();
  }
  
  FunctionParam Function::param(size_t index) const
  {
    assert(index < params_count());
    return m_params.at(index);
  }

  bool Function::has_body() const
  {
    return m_body != nullptr;
  }
  
  TreeNode const& Function::body() const
  {
    return *m_body;
  }
  
  void Function::set_body(std::unique_ptr<TreeNode> body)
  {    
    m_body = std::move(body);
  }

  bool Function::has_callback() const
  {
    if (m_callback) { return true; }
    return false;
  }
  
  TokenValue Function::callback()
  {
    return m_callback();
  }
  
  void Function::set_callback(FunctionCallback_t callback)
  {
    m_callback = callback;
  }

  std::string Function::type_string() const
  {
    std::string res = "(";

    if (!m_params.empty())
      {
	std::string type = token_type_to_string(m_params.front().type);

	if (type == "function") { type = m_params.back().type_string; }
	std::transform(type.begin(), type.end(), type.begin(), tolower);

	if (m_params.front().pass_by == FunctionParamType::REF)
	  {
	    res += "&";
	  }
	else if (m_params.front().pass_by == FunctionParamType::CREF)
	  {
	    res += "&&";
	  }
	
	res += type;

      }

    for (size_t i=1; i<m_params.size(); i++)
      {
	res += ",";
	
	std::string type = token_type_to_string(m_params.at(i).type);
	std::transform(type.begin(), type.end(), type.begin(), tolower);       
	if (type == "function") { type = m_params.at(i).type_string; }

	if (m_params.at(i).pass_by == FunctionParamType::REF)
	  {
	    res += "&";
	  }
	else if (m_params.at(i).pass_by == FunctionParamType::CREF)
	  {
	    res += "&&";
	  }

	res += type;
      }

    res += ")";
    
    if (has_return())
      {
	std::string type = token_type_to_string(*m_return_type);
	if (type == "FUNCTION") { type = m_return_type_string; }
	std::transform(type.begin(), type.end(), type.begin(), tolower);

	res += type;
      }
    
    return res;
  }

  std::string Function::body_string() const
  {
    return m_body->string();
  }
  
  std::string Function::string() const
  {
    std::string res = "(";
 

    if ( !m_params.empty() )
      {
	res += m_params.front().name;
	std::string type = token_type_to_string(m_params.front().type);
	std::transform(type.begin(), type.end(), type.begin(), tolower);
	res += ":" + type;
      }
    
    for(size_t i=1; i<m_params.size(); i++)
      {
	res += "," + m_params.at(i).name;
	std::string type = token_type_to_string(m_params.at(i).type);
	std::transform(type.begin(), type.end(), type.begin(), tolower);
	res += ":" + type;
      }
    
    res += ")";

    if (has_return())
      {
	res += ":";
	std::string type = token_type_to_string(*m_return_type);
	std::transform(type.begin(), type.end(), type.begin(), tolower);
	res += type;
      }
    
    return res;
  }

  TokenValue Function::call(std::vector<FunctionArg> args)
  {
    return TokenValue {};
  }
  
  /*virtual*/ Function::~Function()
  {
  }

  std::unique_ptr<TokenType> Function::real_return_type(TreeNode const* current)
  {
    
    if (current->type() == TreeNodeType::INT ||
    	current->type() == TreeNodeType::UINT ||
    	current->type() == TreeNodeType::FLOAT ||
    	current->type() == TreeNodeType::BOOL ||
    	current->type() == TreeNodeType::STRING)
      {
    	return std::make_unique<TokenType>(token_type_from_tree_node(*current));
      }
    
    if (current->type() == TreeNodeType::IDENT)
      {
    	std::string name = current->value()->s;
	
    	for (auto& param : m_params)
    	  {
    	    if (param.name == name)
    	      {
    		return std::make_unique<TokenType>(param.type);
    	      }
    	  }
      }

    
    for (size_t i=0; i<current->size(); i++)
      {
        auto c = real_return_type(&current->child(i));

	if (c)
	  {
	    return c;
	  }
      }

    return nullptr;
  }

  void Function::load_from_tree_node(TreeNode const& treenode)
  {
    	// Find the return value.
	std::function<std::unique_ptr<TokenType>(TreeNode const*, std::string*)>
	  find_type =
	  [this, &find_type]
	  (TreeNode const* node, std::string* type_str)
	  -> std::unique_ptr<TokenType>
	  {
	   if (node->type() == TreeNodeType::INT ||
	       node->type() == TreeNodeType::UINT ||
	       node->type() == TreeNodeType::FLOAT ||
	       node->type() == TreeNodeType::BOOL ||
	       node->type() == TreeNodeType::STRING)
	     {
	      return std::make_unique<TokenType>(token_type_from_tree_node(*node));
	     }
	   else if (node->type() == TreeNodeType::IDENT)
	     {
	      std::string name = node->value()->s;
	      for (auto const& param : m_params)
		{
		  if (name == param.name)
		    {
		      TokenType type = param.type;
		      std::string typestr = token_type_to_string(type);
		      
		      if (typestr != "FUNCTION")
			{
			  std::transform(typestr.begin(),
					 typestr.end(),
					 typestr.begin(),
					 tolower);
			  
			  *type_str = typestr;
			  return std::make_unique<TokenType>(param.type);
			}
		    
		    }
		}
	      return nullptr;
	     }
	   else
	     {
	      for(size_t i=0; i<node->size(); i++)
	      	{
	      	 auto t = find_type(&node->child(i), type_str);
	      	 if (t != nullptr) { return t; }
	      	}
					    
	      return nullptr;
	     }
	  };

	if (treenode.size() > 1 && treenode.type() == TreeNodeType::FUNCTION)
	  {
	    size_t index = 1;
	    if (treenode.child(0).type() == TreeNodeType::TYPE)
	      {
		index = 2;

	    	TokenType type;
		std::string type_str = treenode.child(0).value()->s;
	
		if (type_str == "int") { type = TokenType::INT; }
		else if (type_str == "uint") { type = TokenType::UINT; }
		else if (type_str == "float") { type = TokenType::FLOAT; }
		else if (type_str == "bool") { type = TokenType::BOOL; }
		else if (type_str == "string") { type = TokenType::STRING; }
		else { throw std::runtime_error("Function unknown type"); }
		m_return_type = std::make_unique<TokenType>(type);
		m_return_type_string = type_str;
	  }
        
	set_body(std::make_unique<TreeNode>(treenode.child(index)));
      }
	
    if (treenode.type() == TreeNodeType::PARAM)
      {
	std::string name = treenode.child(0).value()->s;
        TokenType type = token_type_from_tree_node(treenode.child(1));
	
	FunctionParam param;
	param.name = name;
	param.type = type;
	param.type_string = treenode.child(1).value()->s;
	add_param(param);
      }
    else if (treenode.type() == TreeNodeType::PARAM_REF)
      {
	std::string name = treenode.child(0).value()->s;
        TokenType type = token_type_from_tree_node(treenode.child(1));
	
	FunctionParam param;
	param.name = name;
	param.type = type;
	param.pass_by = FunctionParamType::REF;
	param.type_string = treenode.child(1).value()->s;
	add_param(param);
      }
    else if (treenode.type() == TreeNodeType::PARAM_CREF)
      {
	std::string name = treenode.child(0).value()->s;
        TokenType type = token_type_from_tree_node(treenode.child(1));
	
	FunctionParam param;
	param.name = name;
	param.type = type;
	param.type_string = treenode.child(1).value()->s;
	param.pass_by = FunctionParamType::CREF;
	add_param(param);
      }
    else if (treenode.type() == TreeNodeType::RETURN)
      {
	if (!m_return_type)
	  {
	    std::string type_str;
	
	    auto type = find_type(&treenode, &type_str);

	
	    m_return_type_string = type_str;
	
	    if (type)
	      {
		m_return_type = std::make_unique<TokenType>(*type);
	      }
	  }
      }
    else if (treenode.type() == TreeNodeType::ERROR)
      {
	m_error = std::make_unique<int>(treenode.child(0).value()->i);	
      }
    else
      {
	for(size_t i=0; i<treenode.size(); i++)
	  {
	    if (treenode.child(i).type() != TreeNodeType::FUNCTION)
	    load_from_tree_node(treenode.child(i));
	  }
      }
  }
}
