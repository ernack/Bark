#ifndef LEXER_HPP
#define LEXER_HPP

#include <memory>
#include <vector>
#include <string>

namespace bk
{
  class Token;
  
  class Lexer
  {
  public:
    explicit Lexer();

    std::vector<Token> operator()(std::string const& source);
    
    virtual ~Lexer();
    Lexer& operator=(Lexer const& lexer) = delete;
    Lexer(Lexer const& lexer) = delete;
  private:
    std::string m_source;
    size_t m_cursor;
    
    bool match_separator();
    bool match_int_separator();
    bool match_keyword(std::string keyword);
    bool match_char(char c);
    std::unique_ptr<char> match_alpha();
    std::unique_ptr<int> match_digit();
    std::unique_ptr<int> match_int();
    std::unique_ptr<unsigned int> match_uint();
    std::unique_ptr<float> match_float();
    std::unique_ptr<std::string> match_string();
    bool match_ident_char(char c, bool first = false);
    std::unique_ptr<std::string> match_ident();
    std::unique_ptr<std::string> match_type();
  };
}
#endif // LEXER

