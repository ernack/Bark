#ifndef TREENODETYPE_HPP
#define TREENODETYPE_HPP

#include <string>

namespace bk
{
  class Token;
  
  enum class TreeNodeType {
			   BARK,
			   WHILE,
			   IF, ELSE,
			   ERROR, RETURN,
			   ASSIGN, VAR_DECL,
			   OR,
			   AND,
			   EQ, NEQ,
			   LT, GT, LE, GE,
			   ADD, SUB,
			   MUL, DIV, MOD,
			   NOT,
			   // LITERALS
			   INT, UINT, FLOAT,
			   STRING, BOOL, IDENT,
			   BLOCK, FUNCTION, FUNCALL,
			   PARAMS, TYPE,
			   PARAM, PARAM_REF, PARAM_CREF
  };

  std::string tree_node_type_to_string(TreeNodeType type);
  TreeNodeType tree_node_type_from_token(Token const& token);
}
#endif // TREENODETYPE

