#include "TreeNodeType.hpp"
#include "TokenType.hpp"
#include "Token.hpp"
#include <stdexcept>

namespace bk
{
  std::string tree_node_type_to_string(TreeNodeType type)
  {
    std::string res;
    
    switch (type)
      {
      case TreeNodeType::BARK: res = "BARK"; break;
      case TreeNodeType::ADD: res = "ADD"; break;
      case TreeNodeType::SUB: res = "SUB"; break;
      case TreeNodeType::MUL: res = "MUL"; break;
      case TreeNodeType::DIV: res = "DIV"; break;
      case TreeNodeType::MOD: res = "MOD"; break;
      case TreeNodeType::INT: res = "INT"; break;
      case TreeNodeType::UINT: res = "UINT"; break;
      case TreeNodeType::FLOAT: res = "FLOAT"; break;
      case TreeNodeType::STRING: res = "STRING"; break;
      case TreeNodeType::BOOL: res = "BOOL"; break;
      case TreeNodeType::OR: res = "OR"; break;
      case TreeNodeType::AND: res = "AND"; break;
      case TreeNodeType::NOT: res = "NOT"; break;
      case TreeNodeType::EQ: res = "EQ"; break;
      case TreeNodeType::NEQ: res = "NEQ"; break;
      case TreeNodeType::LT: res = "LT"; break;
      case TreeNodeType::LE: res = "LE"; break;
      case TreeNodeType::GT: res = "GT"; break;
      case TreeNodeType::GE: res = "GE"; break;
      case TreeNodeType::VAR_DECL: res = "VAR_DECL"; break;
      case TreeNodeType::IDENT: res = "IDENT"; break;
      case TreeNodeType::ASSIGN: res = "ASSIGN"; break;
      case TreeNodeType::BLOCK: res = "BLOCK"; break;
      case TreeNodeType::FUNCTION: res = "FUNCTION"; break;
      case TreeNodeType::PARAMS: res = "PARAMS"; break;
      case TreeNodeType::PARAM: res = "PARAM"; break;
      case TreeNodeType::PARAM_REF: res = "PARAM_REF"; break;
      case TreeNodeType::PARAM_CREF: res = "PARAM_CREF"; break;
      case TreeNodeType::TYPE: res = "TYPE"; break;
      case TreeNodeType::RETURN: res = "RETURN"; break;
      case TreeNodeType::ERROR: res = "ERROR"; break;
      case TreeNodeType::FUNCALL: res = "FUNCALL"; break;
      case TreeNodeType::IF: res = "IF"; break;
      case TreeNodeType::ELSE: res = "ELSE"; break;
      case TreeNodeType::WHILE: res = "WHILE"; break;
      default: throw std::runtime_error("TreeNodeType unknown type."); break;
      }
    
    return res;
  }

  TreeNodeType tree_node_type_from_token(Token const& token)
  {
    switch (token.type())
      {
      case TokenType::INT: return TreeNodeType::INT;
      case TokenType::UINT: return TreeNodeType::UINT;
      case TokenType::FLOAT: return TreeNodeType::FLOAT;
      case TokenType::BOOL: return TreeNodeType::BOOL;
      case TokenType::STRING: return TreeNodeType::STRING;
      case TokenType::FUNCTION: return TreeNodeType::FUNCTION;
      default: throw std::runtime_error("tree_node_type_from_token type error");
      }

  }
}
