#ifndef TOKENTYPE_HPP
#define TOKENTYPE_HPP

#include <string>

namespace bk
{
  class TreeNode;
  enum class TokenType {
			INT,
			UINT,
			FLOAT,
			ADD,
			SUB,
			MUL,
			DIV,
			MOD,
			STRING,
			BOOL,
			OR,
			AND,
			NOT,
			OPEN_PAR,
			CLOSE_PAR,
			LT, LE,
			GT, GE,
			EQ, NEQ,
			COLON,
			IDENT,
			TYPE,
			ASSIGN,
			SEMICOLON,
			OPEN_BRACE, CLOSE_BRACE,
			RETURN, ERROR, COMMA,
			REF, FUNCTION, FUNCALL,
			IF, ELSE, INFOP, WHILE
			
  };

  std::string token_type_to_string(TokenType type);
  TokenType token_type_from_tree_node(TreeNode const& node);
}
#endif // TOKENTYPE

