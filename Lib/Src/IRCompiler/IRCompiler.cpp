#include <iostream>
#include "IRCompiler.hpp"
#include "../TreeNode.hpp"

namespace bk
{
  /*explicit*/ IRCompiler::IRCompiler()
  {
  }
  
  IRInstrs IRCompiler::operator()(TreeNode const& treenode)
  {
    compile(treenode);
    return m_instrs;
  }
  
  /*virtual*/ IRCompiler::~IRCompiler()
  {
  }

  void IRCompiler::compile(TreeNode const& treenode)
  {
    switch(treenode.type())
      {
      case TreeNodeType::BARK: compile_children(treenode); break;
      case TreeNodeType::BLOCK: compile_children(treenode); break;
	
      case TreeNodeType::INT: compile_int(treenode); break;
      case TreeNodeType::UINT: compile_uint(treenode); break;
      case TreeNodeType::FLOAT: compile_float(treenode); break;
      case TreeNodeType::BOOL:  compile_bool(treenode); break;
      case TreeNodeType::STRING: compile_string(treenode); break;

      case TreeNodeType::ADD: compile_add(treenode); break;
      case TreeNodeType::SUB: compile_sub(treenode); break;
      case TreeNodeType::MUL: compile_mul(treenode); break;
      case TreeNodeType::DIV: compile_div(treenode); break;
      case TreeNodeType::MOD: compile_mod(treenode); break;

      case TreeNodeType::AND: compile_and(treenode); break;
      case TreeNodeType::OR: compile_or(treenode); break;
      case TreeNodeType::NOT: compile_not(treenode); break;
	
      case TreeNodeType::LT: compile_lt(treenode); break;
      case TreeNodeType::LE: compile_le(treenode); break;
      case TreeNodeType::GT: compile_gt(treenode); break;
      case TreeNodeType::GE: compile_ge(treenode); break;
      case TreeNodeType::EQ: compile_eq(treenode); break;
      case TreeNodeType::NEQ: compile_ne(treenode); break;
      case TreeNodeType::VAR_DECL: compile_var_decl(treenode); break;
      case TreeNodeType::IDENT: compile_ident(treenode); break;
      case TreeNodeType::ASSIGN: compile_assign(treenode); break;	
	
      default: throw std::runtime_error("IRCompiler, unknown type in " + treenode.string() + ".");
      }
  }

  void IRCompiler::compile_children(TreeNode const& treenode)
  {
    for(size_t i=0; i<treenode.size(); i++)
      {
	compile(treenode.child(i));
      }
  }
  
  void IRCompiler::compile_int(TreeNode const& treenode)
  {
    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::PUSH, TokenType::INT, *treenode.value()));
  }

  void IRCompiler::compile_uint(TreeNode const& treenode)
  {
    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::PUSH, TokenType::UINT, *treenode.value()));
  }
  
  void IRCompiler::compile_float(TreeNode const& treenode)
  {
    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::PUSH, TokenType::FLOAT, *treenode.value()));
  }
  
  void IRCompiler::compile_bool(TreeNode const& treenode)
  {
    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::PUSH, TokenType::BOOL, *treenode.value()));
  }
  
  void IRCompiler::compile_string(TreeNode const& treenode)
  {
    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::PUSH, TokenType::STRING, *treenode.value()));
  }

  void IRCompiler::compile_add(TreeNode const& treenode)
  {
    compile(treenode.child(1));
    compile(treenode.child(0));
    
    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::ADD));
  }
  
  void IRCompiler::compile_sub(TreeNode const& treenode)
  {
    compile(treenode.child(1));
    compile(treenode.child(0));
        
    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::SUB));
  }

  void IRCompiler::compile_mul(TreeNode const& treenode)
  {
    compile(treenode.child(1));
    compile(treenode.child(0));
        
    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::MUL));
  }

  void IRCompiler::compile_div(TreeNode const& treenode)
  {
    compile(treenode.child(1));
    compile(treenode.child(0));
    
    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::DIV));
  }

  void IRCompiler::compile_mod(TreeNode const& treenode)
  {
    compile(treenode.child(1));
    compile(treenode.child(0));
    
    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::MOD));
  }

  void IRCompiler::compile_and(TreeNode const& treenode)
  {
    compile(treenode.child(1));
    compile(treenode.child(0));
    
    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::AND));
  }
  
  void IRCompiler::compile_or(TreeNode const& treenode)
  {
    compile(treenode.child(1));
    compile(treenode.child(0));
    
    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::OR));
  }
  
  void IRCompiler::compile_not(TreeNode const& treenode)
  {
    compile(treenode.child(0));
    
    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::NOT));
  }

  void IRCompiler::compile_lt(TreeNode const& treenode)
  {
    compile(treenode.child(1));
    compile(treenode.child(0));

    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::LT));
  }
  
  void IRCompiler::compile_le(TreeNode const& treenode)
  {
    compile(treenode.child(1));
    compile(treenode.child(0));

    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::LE));
  }
  
  void IRCompiler::compile_gt(TreeNode const& treenode)
  {
    compile(treenode.child(1));
    compile(treenode.child(0));

    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::GT));
  }
  
  void IRCompiler::compile_ge(TreeNode const& treenode)
  {
    compile(treenode.child(1));
    compile(treenode.child(0));

    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::GE));
  }
  
  void IRCompiler::compile_eq(TreeNode const& treenode)
  {
    compile(treenode.child(1));
    compile(treenode.child(0));

    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::EQ));
  }
  
  void IRCompiler::compile_ne(TreeNode const& treenode)
  {
    compile(treenode.child(1));
    compile(treenode.child(0));

    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::NE));
  }

  void IRCompiler::compile_var_decl(TreeNode const& treenode)
  {
    std::string name = treenode.child(0).value()->s;

    if (treenode.child(1).type() == TreeNodeType::TYPE)
      {
	compile(treenode.child(2));	
      }
    else
      {
	compile(treenode.child(1));
      }

    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::PUSH,
						      TokenType::STRING,
						      token_value(name)));
    
    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::STORE));
  }

  void IRCompiler::compile_ident(TreeNode const& treenode)
  {
    std::string name = treenode.value()->s;
    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::PUSH,
						      TokenType::STRING,
						      token_value(name)));

    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::LOAD));
  }

  void IRCompiler::compile_assign(TreeNode const& treenode)
  {
    std::string name = treenode.child(0).value()->s;
    
    compile(treenode.child(1));

    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::PUSH,
						      TokenType::STRING,
						      token_value(name)));

    m_instrs.push_back(std::make_shared<IRStackEntry>(IROpCode::ASSIGN));
  }
}
