#ifndef IRCOMPILER_HPP
#define IRCOMPILER_HPP

#include <vector>
#include <memory>
#include "../TokenValue.hpp"
#include "../TokenType.hpp"

namespace bk
{
  class TreeNode;

  enum class IROpCode
    {
     PUSH,
     AND, OR, NOT,
     ADD, SUB,
     MUL, DIV, MOD,
     LT, LE, GT, GE, EQ, NE,
     STORE, LOAD, ASSIGN
    };

  
  struct IRStackEntry
  {
    IRStackEntry(IROpCode _opcode)
      : opcode { _opcode }
    {
    }

    IRStackEntry(IROpCode _opcode, TokenType _type, TokenValue _value)
      : opcode { _opcode }
      , type { _type }
      , value { _value }
    {
    }
    
    IROpCode opcode;
    TokenType type = TokenType::INT;
    TokenValue value = token_value(0);
  };

  using IRInstrs = std::vector<std::shared_ptr<IRStackEntry>>;
  
  class IRCompiler
  {
  public:
    explicit IRCompiler();

    IRInstrs operator()(TreeNode const& treenode);
    
    virtual ~IRCompiler();
    IRCompiler& operator=(IRCompiler const& ircompiler) = delete;
    IRCompiler(IRCompiler const& ircompiler) = delete;
  private:
    IRInstrs m_instrs;
    
    void compile(TreeNode const& treenode);

    void compile_children(TreeNode const& treenode);
    
    void compile_int(TreeNode const& treenode);
    void compile_uint(TreeNode const& treenode);
    void compile_float(TreeNode const& treenode);
    void compile_bool(TreeNode const& treenode);
    void compile_string(TreeNode const& treenode);

    void compile_add(TreeNode const& treenode);
    void compile_sub(TreeNode const& treenode);
    void compile_mul(TreeNode const& treenode);
    void compile_div(TreeNode const& treenode);
    void compile_mod(TreeNode const& treenode);

    void compile_and(TreeNode const& treenode);
    void compile_or(TreeNode const& treenode);
    void compile_not(TreeNode const& treenode);

    void compile_lt(TreeNode const& treenode);
    void compile_le(TreeNode const& treenode);
    void compile_gt(TreeNode const& treenode);
    void compile_ge(TreeNode const& treenode);
    void compile_eq(TreeNode const& treenode);
    void compile_ne(TreeNode const& treenode);
    void compile_var_decl(TreeNode const& treenode);
    void compile_ident(TreeNode const& treenode);
    void compile_assign(TreeNode const& treenode);
  };
}
#endif // IRCOMPILER

