#ifndef TREENODE_HPP
#define TREENODE_HPP

#include <vector>
#include <memory>
#include <string>

#include "TreeNodeType.hpp"

namespace bk
{
  class TokenValue;
  class TreeNode
  {
  public:
    explicit TreeNode(TreeNodeType type, std::unique_ptr<TokenValue> value = nullptr);

    TreeNode(TreeNode const& treenode);
    
    TreeNodeType type() const;
    void set_type(TreeNodeType type);
    
    std::string string() const;
    void add_child(std::unique_ptr<TreeNode> node);

    TreeNode const& child(size_t index) const;
    std::unique_ptr<TreeNode> move_child(size_t index);
    
    size_t size() const;

    TokenValue const* value() const;
    
    virtual ~TreeNode();
    
    TreeNode& operator=(TreeNode const& treenode) = delete;
    
  private:
    TreeNodeType m_type;
    std::unique_ptr<TokenValue> m_value;
    std::vector<std::unique_ptr<TreeNode>> m_children;
  };
}
#endif // TREENODE

