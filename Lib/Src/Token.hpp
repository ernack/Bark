#ifndef TOKEN_HPP
#define TOKEN_HPP

#include "TokenType.hpp"
#include "TokenValue.hpp"

namespace bk
{
  class Token
  {
  public:
    explicit Token();
    
    void set_type(TokenType type);
    TokenType type() const;

    void set_int_value(int value);
    int int_value() const;

    void set_uint_value(unsigned int value);
    int uint_value() const;

    void set_float_value(float value);
    float float_value() const;

    void set_string_value(std::string value);
    std::string string_value() const;
    
    void set_bool_value(bool value);
    bool bool_value() const;

    TokenValue value() const;
    
    virtual ~Token();
    
  private:
    TokenType m_type;
    TokenValue m_value;
  };
}
#endif // TOKEN

