#ifndef FUNCTION_HPP
#define FUNCTION_HPP

#include <functional>
#include <memory>
#include <vector>
#include <string>
#include "TokenType.hpp"
#include "TokenValue.hpp"

namespace bk
{
  class Environment;
  class TreeNode;

  typedef std::function<TokenValue()> FunctionCallback_t;
  
  enum class FunctionParamType
    {
     VALUE,
     REF,
     CREF
    };
  
  struct FunctionParam
  {
    std::string name = "";
    TokenType type = TokenType::INT;
    std::string type_string = "";
    FunctionParamType pass_by = FunctionParamType::VALUE;
  };

  struct FunctionArg
  {
    std::string name = "";
    TokenType type = TokenType::INT;
    TokenValue value;
    FunctionParamType pass_by = FunctionParamType::VALUE;
  };

  class Function
  {
  public:
    explicit Function(Environment& env);
    explicit Function(TreeNode const& treenode, Environment& env);
    
    TokenType return_type() const;
    std::string return_type_string() const;
    std::unique_ptr<TokenType> real_return_type();
    bool has_return() const;
    void set_return_type(TokenType type);    

    int error() const;
    void set_error(int err);
    bool has_error() const;
    
    void add_param(FunctionParam fp);
    size_t params_count() const;
    FunctionParam param(size_t index) const;

    bool has_body() const;
    TreeNode const& body() const;
    void set_body(std::unique_ptr<TreeNode> body);

    bool has_callback() const;
    TokenValue callback();
    void set_callback(FunctionCallback_t callback);
    
    std::string type_string() const;
    std::string body_string() const;
    std::string string() const;

    TokenValue call(std::vector<FunctionArg> args);
    
    virtual ~Function();
    Function& operator=(Function const& function) = delete;
    Function(Function const& function) = delete;
  private:
    Environment& m_env;
    std::vector<FunctionParam> m_params;
    std::unique_ptr<TokenType> m_return_type;
    std::string m_return_type_string = "";
    std::unique_ptr<TreeNode> m_body;
    FunctionCallback_t m_callback;
    std::unique_ptr<int> m_error;
    
    std::unique_ptr<TokenType> real_return_type(TreeNode const* current);
    void load_from_tree_node(TreeNode const& treenode);
  };
}
#endif // FUNCTION

