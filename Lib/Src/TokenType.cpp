#include <iostream>
#include "TokenType.hpp"
#include "TreeNode.hpp"
#include "TokenValue.hpp"

namespace bk
{
  std::string token_type_to_string(TokenType type)
  {
    switch (type)
      {
      case TokenType::INT: return "INT";
      case TokenType::UINT: return "UINT";
      case TokenType::FLOAT: return "FLOAT";
      case TokenType::ADD: return "ADD";
      case TokenType::SUB: return "SUB";
      case TokenType::MUL: return "MUL";
      case TokenType::DIV: return "DIV";
      case TokenType::MOD: return "MOD";
      case TokenType::STRING: return "STRING";
      case TokenType::BOOL: return "BOOL";
      case TokenType::OR: return "OR";
      case TokenType::AND: return "AND";
      case TokenType::NOT: return "NOT";
      case TokenType::OPEN_PAR: return "OPEN_PAR";
      case TokenType::CLOSE_PAR: return "CLOSE_PAR";

      case TokenType::EQ: return "EQ";
      case TokenType::NEQ: return "NEQ";
      case TokenType::LT: return "LT";
      case TokenType::LE: return "LE";
      case TokenType::GT: return "GT";
      case TokenType::GE: return "GE";
      case TokenType::COLON: return "COLON";
      case TokenType::IDENT: return "IDENT";
      case TokenType::TYPE: return "TYPE";
      case TokenType::ASSIGN: return "ASSIGN";
      case TokenType::SEMICOLON: return "SEMICOLON";
      case TokenType::OPEN_BRACE: return "OPEN_BRACE";
      case TokenType::CLOSE_BRACE: return "CLOSE_BRACE";
      case TokenType::RETURN: return "RETURN";
      case TokenType::ERROR: return "ERROR";
      case TokenType::COMMA: return "COMMA";
      case TokenType::REF: return "REF";
      case TokenType::FUNCTION: return "FUNCTION";
      case TokenType::FUNCALL: return "FUNCALL";

      case TokenType::IF: return "IF";

      case TokenType::ELSE: return "ELSE";
      case TokenType::INFOP: return "INFOP";
      case TokenType::WHILE: return "WHILE";
      }

    return "TOKEN TYPE ERROR NOT FOUND";
  }

  TokenType token_type_from_tree_node(TreeNode const& node)
  {
    switch (node.type())
      {
      case TreeNodeType::INT: return TokenType::INT;
      case TreeNodeType::UINT: return TokenType::UINT;
      case TreeNodeType::FLOAT: return TokenType::FLOAT;
      case TreeNodeType::BOOL: return TokenType::BOOL;
      case TreeNodeType::STRING: return TokenType::STRING;
      case TreeNodeType::FUNCTION: return TokenType::FUNCTION;
      case TreeNodeType::FUNCALL: return TokenType::FUNCALL;
      case TreeNodeType::TYPE:
	{
	  std::string typestr = node.value()->s;

	  if (typestr == "int") { return TokenType::INT; }
	  else if (typestr == "uint") { return TokenType::UINT; }
	  else if (typestr == "float") { return TokenType::FLOAT; }
	  else if (typestr == "bool") { return TokenType::BOOL; }
	  else if (typestr == "string") { return TokenType::STRING; }
	  else
	    {
	      return TokenType::FUNCTION;
	    }
	}
	break;	
      default: throw std::runtime_error("token_type_from_tree_node unknown type.");
      }
    
    return TokenType::INT;
  }
}
