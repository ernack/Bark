#ifndef EVALUATOR_HPP
#define EVALUATOR_HPP

#include <unordered_map>
#include "../TokenValue.hpp"
#include "../TokenType.hpp"

namespace bk
{
  class TreeNode;
  class Environment;
  
  class Evaluator
  {
  public:
    explicit Evaluator(Environment& env);

    TokenValue operator()(TreeNode const& treenode);
    TokenType type() const;
    
    TokenValue eval(TreeNode const& treenode);
    
    virtual ~Evaluator();
    Evaluator& operator=(Evaluator const& evaluator) = delete;
    Evaluator(Evaluator const& evaluator) = delete;
  private:
    Environment& m_env;
    TokenType m_type;
    std::shared_ptr<Function> m_function;
    std::unordered_map<std::string,bool> m_errors;
    
    TokenValue eval_bark(TreeNode const& treenode);
    TokenValue eval_int(TreeNode const& treenode);
    TokenValue eval_uint(TreeNode const& treenode);
    TokenValue eval_float(TreeNode const& treenode);
    TokenValue eval_add(TreeNode const& treenode);
    TokenValue eval_sub(TreeNode const& treenode);
    TokenValue eval_mul(TreeNode const& treenode);
    TokenValue eval_div(TreeNode const& treenode);
    TokenValue eval_mod(TreeNode const& treenode);
    TokenValue eval_string(TreeNode const& treenode);
    TokenValue eval_bool(TreeNode const& treenode);
    TokenValue eval_or(TreeNode const& treenode);
    TokenValue eval_and(TreeNode const& treenode);
    TokenValue eval_not(TreeNode const& treenode);

    TokenValue eval_eq(TreeNode const& treenode);
    TokenValue eval_neq(TreeNode const& treenode);  
    TokenValue eval_lt(TreeNode const& treenode);
    TokenValue eval_le(TreeNode const& treenode);    
    TokenValue eval_gt(TreeNode const& treenode);
    TokenValue eval_ge(TreeNode const& treenode);
    TokenValue eval_var_decl(TreeNode const& treenode);
    TokenValue eval_ident(TreeNode const& treenode);
    TokenValue eval_assign(TreeNode const& treenode);
    TokenValue eval_block(TreeNode const& treenode);
    TokenValue eval_function(TreeNode const& treenode);
    TokenValue eval_funcall(TreeNode const& treenode);
    TokenValue eval_return(TreeNode const& treenode);
    TokenValue eval_if(TreeNode const& treenode);
    TokenValue eval_while(TreeNode const& treenode);
    TokenValue eval_error(TreeNode const& treenode);
  };
}
#endif // EVALUATOR

