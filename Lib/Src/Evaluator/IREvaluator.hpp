#ifndef IREVALUATOR_HPP
#define IREVALUATOR_HPP

#include <vector>
#include "../IRCompiler/IRCompiler.hpp"

namespace bk
{
  class Environment;
  
  class IREvaluator
  {
  public:
    explicit IREvaluator(Environment& env);

    TokenType type() const;
    TokenValue operator()(TreeNode const& treenode);
    
    virtual ~IREvaluator();
    IREvaluator& operator=(IREvaluator const& irevaluator) = delete;
    IREvaluator(IREvaluator const& irevaluator) = delete;
  private:
    TokenType m_type;
    Environment& m_env;
    std::vector<std::pair<TokenType, TokenValue>> m_stack;    
    
    void eval(IRStackEntry const& entry);
    void eval_push(IRStackEntry const& entry);

    void eval_add(IRStackEntry const& entry);
    void eval_sub(IRStackEntry const& entry);
    void eval_mul(IRStackEntry const& entry);
    void eval_div(IRStackEntry const& entry);
    void eval_mod(IRStackEntry const& entry);

    void eval_and(IRStackEntry const& entry);
    void eval_or(IRStackEntry const& entry);
    void eval_not(IRStackEntry const& entry);

    void eval_lt(IRStackEntry const& entry);
    void eval_le(IRStackEntry const& entry);
    void eval_gt(IRStackEntry const& entry);
    void eval_ge(IRStackEntry const& entry);
    void eval_eq(IRStackEntry const& entry);
    void eval_ne(IRStackEntry const& entry);

    void eval_store(IRStackEntry const& entry);
    void eval_load(IRStackEntry const& entry);
    void eval_assign(IRStackEntry const& entry);
  };
}
#endif // IREVALUATOR

