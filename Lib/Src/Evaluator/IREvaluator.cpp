#include <cassert>
#include <iostream>
#include "IREvaluator.hpp"
#include "../Environment/Environment.hpp"

namespace bk
{
  /*explicit*/ IREvaluator::IREvaluator(Environment& env)
    : m_env { env }
  {
  }

  TokenType IREvaluator::type() const
  {
    return m_type;
  }
  
  TokenValue IREvaluator::operator()(TreeNode const& treenode)
  {
    IRCompiler compiler;
    IRInstrs stack = compiler(treenode);
    
    for (size_t i=0; i<stack.size(); i++)
      {
	eval(*stack.at(i));
      }
        
    m_type = m_stack.back().first;
    return m_stack.back().second;
  }

  /*virtual*/ IREvaluator::~IREvaluator()
  {
  }
  
  void IREvaluator::eval(IRStackEntry const& entry)
  {
    
    switch (entry.opcode)
      {
      case IROpCode::PUSH: eval_push(entry); break;

      case IROpCode::ADD: eval_add(entry); break;
      case IROpCode::SUB: eval_sub(entry); break;
      case IROpCode::MUL: eval_mul(entry); break;
      case IROpCode::DIV: eval_div(entry); break;
      case IROpCode::MOD: eval_mod(entry); break;
	
      case IROpCode::AND: eval_and(entry); break;
      case IROpCode::OR: eval_or(entry); break;
      case IROpCode::NOT: eval_not(entry); break;

      case IROpCode::LT: eval_lt(entry); break;
      case IROpCode::LE: eval_le(entry); break;
      case IROpCode::GT: eval_gt(entry); break;
      case IROpCode::GE: eval_ge(entry); break;
      case IROpCode::EQ: eval_eq(entry); break;
      case IROpCode::NE: eval_ne(entry); break;
      case IROpCode::STORE: eval_store(entry); break;
      case IROpCode::LOAD: eval_load(entry); break;
      case IROpCode::ASSIGN: eval_assign(entry); break;
	
      default: throw std::runtime_error("IREvaluator : unknown opcode.");
      }
  }

  void IREvaluator::eval_push(IRStackEntry const& entry)
  {
    m_stack.push_back({entry.type, entry.value});
  }

  void IREvaluator::eval_add(IRStackEntry const& entry)
  {
    auto lhs = m_stack.back(); m_stack.pop_back();
    auto rhs = m_stack.back(); m_stack.pop_back();
            
    TokenValue value;

    if (lhs.first == TokenType::INT)
      {
	value.i = lhs.second.i + rhs.second.i;
      }
    else if (lhs.first == TokenType::UINT)
      {
	value.ui = lhs.second.ui + rhs.second.ui;
      }
    else if (lhs.first == TokenType::FLOAT)
      {
	value.f = lhs.second.f + rhs.second.f;
      }
    else
      {
	throw std::runtime_error("ADD : unknown type");
      }
    
    m_stack.push_back({rhs.first, value});
  }
  
  void IREvaluator::eval_sub(IRStackEntry const& entry)
  {
    auto lhs = m_stack.back(); m_stack.pop_back();
    auto rhs = m_stack.back(); m_stack.pop_back();

    TokenValue value;
    if (lhs.first == TokenType::INT)
      {
	value.i = lhs.second.i - rhs.second.i;
      }
    else if (lhs.first == TokenType::UINT)
      {
	value.ui = lhs.second.ui - rhs.second.ui;
      }
    else if (lhs.first == TokenType::FLOAT)
      {
	value.f = lhs.second.f - rhs.second.f;
      }
    else
      {
	throw std::runtime_error("SUB : unknown type");
      }
    
    m_stack.push_back({rhs.first, value});
  }
  
  void IREvaluator::eval_mul(IRStackEntry const& entry)
  {
    auto lhs = m_stack.back(); m_stack.pop_back();
    auto rhs = m_stack.back(); m_stack.pop_back();

    TokenValue value;

    if (lhs.first == TokenType::INT)
      {
	value.i = lhs.second.i * rhs.second.i;
      }
    else if (lhs.first == TokenType::UINT)
      {
	value.ui = lhs.second.ui * rhs.second.ui;
      }
    else if (lhs.first == TokenType::FLOAT)
      {
	value.f = lhs.second.f * rhs.second.f;
      }
    else
      {
	throw std::runtime_error("MUL : unknown type");
      }
    
    m_stack.push_back({rhs.first, value});
  }
  
  void IREvaluator::eval_div(IRStackEntry const& entry)
  {
    auto lhs = m_stack.back(); m_stack.pop_back();
    auto rhs = m_stack.back(); m_stack.pop_back();
    
    TokenValue value;

    if (lhs.first == TokenType::INT)
      {
	value.i = lhs.second.i / rhs.second.i;
      }
    else if (lhs.first == TokenType::UINT)
      {
	value.ui = lhs.second.ui / rhs.second.ui;
      }
    else if (lhs.first == TokenType::FLOAT)
      {
	value.f = lhs.second.f / rhs.second.f;
      }
    else
      {
	throw std::runtime_error("DIV : unknown type");
      }
    
    m_stack.push_back({rhs.first, value});
  }
  
  void IREvaluator::eval_mod(IRStackEntry const& entry)
  {
    auto lhs = m_stack.back(); m_stack.pop_back();
    auto rhs = m_stack.back(); m_stack.pop_back();

    TokenValue value;
    if (lhs.first == TokenType::INT)
      {
	value.i = lhs.second.i % rhs.second.i;
      }
    else if (lhs.first == TokenType::UINT)
      {
	value.ui = lhs.second.ui % rhs.second.ui;
      }
    else
      {
	throw std::runtime_error("MOD : unknown type");
      }
    
    m_stack.push_back({rhs.first, value});
  }

  void IREvaluator::eval_and(IRStackEntry const& entry)
  {
    auto lhs = m_stack.back(); m_stack.pop_back();
    auto rhs = m_stack.back(); m_stack.pop_back();

    TokenValue value;
    value.b = lhs.second.b && rhs.second.b;
    
    m_stack.push_back({TokenType::BOOL, value});
  }
  
  void IREvaluator::eval_or(IRStackEntry const& entry)
  {
    auto lhs = m_stack.back(); m_stack.pop_back();
    auto rhs = m_stack.back(); m_stack.pop_back();

    TokenValue value;
    value.b = lhs.second.b|| rhs.second.b;
    
    m_stack.push_back({TokenType::BOOL, value});
  }
  
  void IREvaluator::eval_not(IRStackEntry const& entry)
  {
    auto node = m_stack.back(); m_stack.pop_back();

    TokenValue value;
    value.b = !node.second.b;
    
    m_stack.push_back({TokenType::BOOL, value});
  }

  void IREvaluator::eval_lt(IRStackEntry const& entry)
  {
    auto lhs = m_stack.back(); m_stack.pop_back();
    auto rhs = m_stack.back(); m_stack.pop_back();
            
    TokenValue value;

    if (lhs.first == TokenType::INT)
      {
	value.b = lhs.second.i < rhs.second.i;
      }
    else if (lhs.first == TokenType::UINT)
      {
	value.b = lhs.second.ui < rhs.second.ui;
      }
    else if (lhs.first == TokenType::FLOAT)
      {
	value.b = lhs.second.f < rhs.second.f;
      }
    else
      {
	throw std::runtime_error("LT : unknown type");
      }
    
    m_stack.push_back({TokenType::BOOL, value});
  }
  
  void IREvaluator::eval_le(IRStackEntry const& entry)
  {
    auto lhs = m_stack.back(); m_stack.pop_back();
    auto rhs = m_stack.back(); m_stack.pop_back();
            
    TokenValue value;

    if (lhs.first == TokenType::INT)
      {
	value.b = lhs.second.i <= rhs.second.i;
      }
    else if (lhs.first == TokenType::UINT)
      {
	value.b = lhs.second.ui <= rhs.second.ui;
      }
    else if (lhs.first == TokenType::FLOAT)
      {
	value.b = lhs.second.f <= rhs.second.f;
      }
    else
      {
	throw std::runtime_error("LE : unknown type");
      }
    
    m_stack.push_back({TokenType::BOOL, value});
  }
  
  void IREvaluator::eval_gt(IRStackEntry const& entry)
  {
    auto lhs = m_stack.back(); m_stack.pop_back();
    auto rhs = m_stack.back(); m_stack.pop_back();
            
    TokenValue value;

    if (lhs.first == TokenType::INT)
      {
	value.b = lhs.second.i > rhs.second.i;
      }
    else if (lhs.first == TokenType::UINT)
      {
	value.b = lhs.second.ui > rhs.second.ui;
      }
    else if (lhs.first == TokenType::FLOAT)
      {
	value.b = lhs.second.f > rhs.second.f;
      }
    else
      {
	throw std::runtime_error("GT : unknown type");
      }
    
    m_stack.push_back({TokenType::BOOL, value});
  }
  
  void IREvaluator::eval_ge(IRStackEntry const& entry)
  {
    auto lhs = m_stack.back(); m_stack.pop_back();
    auto rhs = m_stack.back(); m_stack.pop_back();
            
    TokenValue value;

    if (lhs.first == TokenType::INT)
      {
	value.b = lhs.second.i >= rhs.second.i;
      }
    else if (lhs.first == TokenType::UINT)
      {
	value.b = lhs.second.ui >= rhs.second.ui;
      }
    else if (lhs.first == TokenType::FLOAT)
      {
	value.b = lhs.second.f >= rhs.second.f;
      }
    else
      {
	throw std::runtime_error("GE : unknown type");
      }
    
    m_stack.push_back({TokenType::BOOL, value});
  }
  
  void IREvaluator::eval_eq(IRStackEntry const& entry)
  {
    auto lhs = m_stack.back(); m_stack.pop_back();
    auto rhs = m_stack.back(); m_stack.pop_back();
            
    TokenValue value;

    if (lhs.first == TokenType::INT)
      {
	value.b = lhs.second.i == rhs.second.i;
      }
    else if (lhs.first == TokenType::UINT)
      {
	value.b = lhs.second.ui == rhs.second.ui;
      }
    else if (lhs.first == TokenType::FLOAT)
      {
	value.b = lhs.second.f == rhs.second.f;
      }
    else if (lhs.first == TokenType::STRING)
      {
	value.b = lhs.second.s == rhs.second.s;
      }
    else if (lhs.first == TokenType::BOOL)
      {
	value.b = lhs.second.b == rhs.second.b;
      }
    else
      {
	throw std::runtime_error("EQ : unknown type");
      }
    
    m_stack.push_back({TokenType::BOOL, value});
  }
  
  void IREvaluator::eval_ne(IRStackEntry const& entry)
  {
    auto lhs = m_stack.back(); m_stack.pop_back();
    auto rhs = m_stack.back(); m_stack.pop_back();
            
    TokenValue value;

    if (lhs.first == TokenType::INT)
      {
	value.b = lhs.second.i != rhs.second.i;
      }
    else if (lhs.first == TokenType::UINT)
      {
	value.b = lhs.second.ui != rhs.second.ui;
      }
    else if (lhs.first == TokenType::FLOAT)
      {
	value.b = lhs.second.f != rhs.second.f;
      }
    else if (lhs.first == TokenType::STRING)
      {
	value.b = lhs.second.s != rhs.second.s;
      }
    else if (lhs.first == TokenType::BOOL)
      {
	value.b = lhs.second.b != rhs.second.b;
      }
    else
      {
	throw std::runtime_error("NE : unknown type");
      }
    
    m_stack.push_back({TokenType::BOOL, value});
  }

  void IREvaluator::eval_store(IRStackEntry const& entry)
  {
    auto lhs = m_stack.back(); m_stack.pop_back();
    auto rhs = m_stack.back(); m_stack.pop_back();

    m_env.declare(lhs.second.s, rhs.first, rhs.second);
    
    m_stack.push_back(rhs);
  }

  void IREvaluator::eval_load(IRStackEntry const& entry)
  {
    auto node = m_stack.back(); m_stack.pop_back();

    auto value = m_env.get_value(node.second.s);
    auto type = m_env.get_type(node.second.s);

    if (!value || !type)
      {
	throw std::runtime_error("Variable " + node.second.s + " not declared.");
      }
    
    m_stack.push_back({*type, *value});
  }

  void IREvaluator::eval_assign(IRStackEntry const& entry)
  {
    auto lhs = m_stack.back(); m_stack.pop_back();
    auto rhs = m_stack.back(); m_stack.pop_back();

    m_env.set_value(lhs.second.s, rhs.second);
    
    m_stack.push_back(rhs);
  }
}
