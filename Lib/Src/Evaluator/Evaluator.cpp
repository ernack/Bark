#include <algorithm>
#include <cassert>
#include <iostream>
#include "Evaluator.hpp"
#include "../TreeNode.hpp"
#include "../Token.hpp"
#include "../Function.hpp"
#include "../Environment/Environment.hpp"

namespace bk
{
  /*explicit*/ Evaluator::Evaluator(Environment& env)
    : m_env { env }
  {
  }

  TokenValue Evaluator::operator()(TreeNode const& treenode)
  {
    return eval(treenode);
  }

  TokenType Evaluator::type() const
  {
    return m_type;
  }
  
  TokenValue Evaluator::eval(TreeNode const& treenode)
  {    
    switch (treenode.type())
      {
      case TreeNodeType::BARK: return eval_bark(treenode);
      case TreeNodeType::INT: return eval_int(treenode);
      case TreeNodeType::UINT: return eval_uint(treenode);
      case TreeNodeType::FLOAT: return eval_float(treenode);
      case TreeNodeType::ADD: return eval_add(treenode);
      case TreeNodeType::SUB: return eval_sub(treenode);
      case TreeNodeType::MUL: return eval_mul(treenode);
      case TreeNodeType::DIV: return eval_div(treenode);
      case TreeNodeType::MOD: return eval_mod(treenode);
      case TreeNodeType::STRING: return eval_string(treenode);
      case TreeNodeType::BOOL: return eval_bool(treenode);
      case TreeNodeType::OR: return eval_or(treenode);
      case TreeNodeType::AND: return eval_and(treenode);
      case TreeNodeType::NOT: return eval_not(treenode);

      case TreeNodeType::EQ: return eval_eq(treenode);
      case TreeNodeType::NEQ: return eval_neq(treenode);
      case TreeNodeType::LT: return eval_lt(treenode);
      case TreeNodeType::LE: return eval_le(treenode);
      case TreeNodeType::GT: return eval_gt(treenode);
      case TreeNodeType::GE: return eval_ge(treenode);

      case TreeNodeType::VAR_DECL: return eval_var_decl(treenode);
      case TreeNodeType::IDENT: return eval_ident(treenode);
      case TreeNodeType::ASSIGN: return eval_assign(treenode);
      case TreeNodeType::BLOCK: return eval_block(treenode);
      case TreeNodeType::FUNCTION: return eval_function(treenode);
      case TreeNodeType::FUNCALL: return eval_funcall(treenode);
      case TreeNodeType::RETURN: return eval_return(treenode);
      case TreeNodeType::IF: return eval_if(treenode);
      case TreeNodeType::WHILE: return eval_while(treenode);
      case TreeNodeType::ERROR: return eval_error(treenode);
      }
    
    throw std::runtime_error("Evaluator: unknown node.");
  }
  
  /*virtual*/ Evaluator::~Evaluator()
  {
  }

  TokenValue Evaluator::eval_bark(TreeNode const& treenode)
  {
    TokenValue val;
    
    for(size_t i=0; i < treenode.size(); i++)
      {
	val = eval(treenode.child(i));
      }
    
    return val;
  }
  
  TokenValue Evaluator::eval_int(TreeNode const& treenode)
  {
    TokenValue const* value = treenode.value();
    m_type = TokenType::INT;
    return *value;
  }
  
  TokenValue Evaluator::eval_uint(TreeNode const& treenode)
  {
    TokenValue const* value = treenode.value();
    m_type = TokenType::UINT;
    return *value;
  }
  
  TokenValue Evaluator::eval_float(TreeNode const& treenode)
  {
    TokenValue const* value = treenode.value();
    m_type = TokenType::FLOAT;
    return *value;
  }
  
  TokenValue Evaluator::eval_add(TreeNode const& treenode)
  {
    TokenValue lhs = eval(treenode.child(0));
    TokenValue rhs = eval(treenode.child(1));
    TokenValue val;

    val.i = lhs.i + rhs.i;
    val.ui = lhs.ui + rhs.ui;
    val.f = lhs.f + rhs.f;;
    
    return val;
  }
  
  TokenValue Evaluator::eval_sub(TreeNode const& treenode)
  {
    TokenValue lhs = eval(treenode.child(0));
    TokenValue rhs = eval(treenode.child(1));
    TokenValue val;

    val.i = lhs.i - rhs.i;
    val.ui = lhs.ui - rhs.ui;
    val.f = lhs.f - rhs.f;
    
    return val;
  }
  
  TokenValue Evaluator::eval_mul(TreeNode const& treenode)
  {
    TokenValue lhs = eval(treenode.child(0));
    TokenValue rhs = eval(treenode.child(1));
    TokenValue val;

    val.i = lhs.i * rhs.i;
    val.ui = lhs.ui * rhs.ui;
    val.f = lhs.f * rhs.f;
    
    return val;
  }
  
  TokenValue Evaluator::eval_div(TreeNode const& treenode)
  {
    TokenValue lhs = eval(treenode.child(0));
    TokenValue rhs = eval(treenode.child(1));
    TokenValue val;
    
    if (rhs.i != 0) { val.i = lhs.i / rhs.i; }
    if (rhs.ui != 0) { val.ui = lhs.ui / rhs.ui; }
    if (rhs.f != 0) { val.f = lhs.f / rhs.f; }

    
    return val;
  }
  
  TokenValue Evaluator::eval_mod(TreeNode const& treenode)
  {
    TokenValue lhs = eval(treenode.child(0));
    TokenValue rhs = eval(treenode.child(1));
    TokenValue val;

    val.i = lhs.i % rhs.i;
    val.ui = lhs.ui % rhs.ui;


    return val;
  }
  
  TokenValue Evaluator::eval_string(TreeNode const& treenode)
  {
    TokenValue const* value = treenode.value();
    m_type = TokenType::STRING;
    return *value;
  }
  
  TokenValue Evaluator::eval_bool(TreeNode const& treenode)
  {
    TokenValue const* value = treenode.value();
    m_type = TokenType::BOOL;
    return *value;
  }
  
  TokenValue Evaluator::eval_or(TreeNode const& treenode)
  {
    TokenValue val;

    TokenValue lhs = eval(treenode.child(0));

    if (lhs.b == true) {val.b = lhs.b; return val; }
    
    TokenValue rhs = eval(treenode.child(1));
       
    val.b = lhs.b || rhs.b;
    
    m_type = TokenType::BOOL;
   
    return val;
  }
  
  TokenValue Evaluator::eval_and(TreeNode const& treenode)
  {
    TokenValue val;

    TokenValue lhs = eval(treenode.child(0));

    if (lhs.b == false) {val.b = lhs.b; return val; }
    
    TokenValue rhs = eval(treenode.child(1));
       
    val.b = lhs.b && rhs.b;
    
    m_type = TokenType::BOOL;
   
    return val;
  }
  
  TokenValue Evaluator::eval_not(TreeNode const& treenode)
  {
    TokenValue lhs = eval(treenode.child(0));
    TokenValue val;

    val.b = !lhs.b;

    m_type = TokenType::BOOL;
    
    return val;
  }

  TokenValue Evaluator::eval_eq(TreeNode const& treenode)
  {
    TokenValue lhs = eval(treenode.child(0));
    TokenValue rhs = eval(treenode.child(1));
    TokenValue val;

    switch (m_type)
      {
      case TokenType::INT: val.b = lhs.i == rhs.i; break;
      case TokenType::UINT: val.b = lhs.ui == rhs.ui; break;
      case TokenType::FLOAT: val.b = lhs.f == rhs.f; break;
      case TokenType::STRING: val.b = lhs.s == rhs.s; break;
      case TokenType::BOOL: val.b = lhs.b == rhs.b; break;
      default: throw std::runtime_error("eval_eq error."); break;
      }

    m_type = TokenType::BOOL;
    
    return val;
  }
  
  TokenValue Evaluator::eval_neq(TreeNode const& treenode)
  {
    TokenValue lhs = eval(treenode.child(0));
    TokenValue rhs = eval(treenode.child(1));
    TokenValue val;

    switch (m_type)
      {
      case TokenType::INT: val.b = lhs.i != rhs.i; break;
      case TokenType::UINT: val.b = lhs.ui != rhs.ui; break;
      case TokenType::FLOAT: val.b = lhs.f != rhs.f; break;
      case TokenType::STRING: val.b = lhs.s != rhs.s; break;
      case TokenType::BOOL: val.b = lhs.b != rhs.b; break;
      default: throw std::runtime_error("eval_neq error."); break;
      }

    m_type = TokenType::BOOL;
    
    return val;
  }
  
  TokenValue Evaluator::eval_lt(TreeNode const& treenode)
  {
    TokenValue lhs = eval(treenode.child(0));
    TokenValue rhs = eval(treenode.child(1));

    TokenValue val;
    
    switch (m_type)
      {
      case TokenType::INT: val.b = lhs.i < rhs.i; break;
      case TokenType::UINT: val.b = lhs.ui < rhs.ui; break;
      case TokenType::FLOAT: val.b = lhs.f < rhs.f; break;
      case TokenType::STRING: val.b = lhs.s < rhs.s; break;
      case TokenType::BOOL: val.b = lhs.b < rhs.b; break;
      default:
	throw std::runtime_error("eval_lt error."); break;
      }

    m_type = TokenType::BOOL;
    
    return val;
  }
  
  TokenValue Evaluator::eval_le(TreeNode const& treenode)
  {
    TokenValue lhs = eval(treenode.child(0));
    TokenValue rhs = eval(treenode.child(1));
    TokenValue val;

    switch (m_type)
      {
      case TokenType::INT: val.b = lhs.i <= rhs.i; break;
      case TokenType::UINT: val.b = lhs.ui <= rhs.ui; break;
      case TokenType::FLOAT: val.b = lhs.f <= rhs.f; break;
      case TokenType::STRING: val.b = lhs.s <= rhs.s; break;
      case TokenType::BOOL: val.b = lhs.b <= rhs.b; break;
      default: throw std::runtime_error("eval_le error."); break;
      }

    m_type = TokenType::BOOL;
    
    return val;
  }
  
  TokenValue Evaluator::eval_gt(TreeNode const& treenode)
  {
    TokenValue lhs = eval(treenode.child(0));
    TokenValue rhs = eval(treenode.child(1));
    TokenValue val;

    switch (m_type)
      {
      case TokenType::INT: val.b = lhs.i > rhs.i; break;
      case TokenType::UINT: val.b = lhs.ui > rhs.ui; break;
      case TokenType::FLOAT: val.b = lhs.f > rhs.f; break;
      case TokenType::STRING: val.b = lhs.s > rhs.s; break;
      case TokenType::BOOL: val.b = lhs.b > rhs.b; break;
      default: throw std::runtime_error("eval_gt error."); break;
      }

    m_type = TokenType::BOOL;
    
    return val;
  }
  
  TokenValue Evaluator::eval_ge(TreeNode const& treenode)
  {
    TokenValue lhs = eval(treenode.child(0));
    TokenValue rhs = eval(treenode.child(1));
    TokenValue val;

    switch (m_type)
      {
      case TokenType::INT: val.b = lhs.i >= rhs.i; break;
      case TokenType::UINT: val.b = lhs.ui >= rhs.ui; break;
      case TokenType::FLOAT: val.b = lhs.f >= rhs.f; break;
      case TokenType::STRING: val.b = lhs.s >= rhs.s; break;
      case TokenType::BOOL: val.b = lhs.b >= rhs.b; break;
      default: throw std::runtime_error("eval_ge error."); break;
      }

    m_type = TokenType::BOOL;
    
    return val;
  }

  TokenValue Evaluator::eval_var_decl(TreeNode const& treenode)
  {

    TreeNode const* ident;
    std::string type_name;
    TreeNode const* value;
    
    if (treenode.size() == 2)
      // Type inference.
      {
	ident = &treenode.child(0);
	value = &treenode.child(1);

	type_name = tree_node_type_to_string(value->type());
	std::transform(type_name.begin(), type_name.end(),
		       type_name.begin(), tolower);
	
      }
    // Type given.
    else
      {
	ident = &treenode.child(0);
        type_name = treenode.child(1).value()->s;
	value = &treenode.child(2);
      }
    
    if (type_name == "int")      
      { m_type = TokenType::INT;
	m_env.declare(ident->value()->s, TokenType::INT, eval(*value)); }
    
    else if (type_name == "uint")
      { m_type = TokenType::UINT;
	m_env.declare(ident->value()->s, TokenType::UINT, eval(*value)); }
    
    else if (type_name == "float")
      { m_type = TokenType::FLOAT;
	m_env.declare(ident->value()->s, TokenType::FLOAT, eval(*value)); }
    
    else if (type_name == "string")
      { m_type = TokenType::STRING;
	m_env.declare(ident->value()->s, TokenType::STRING, eval(*value)); }
    
    else if (type_name == "bool")
      { m_type = TokenType::BOOL;
	m_env.declare(ident->value()->s, TokenType::BOOL, eval(*value)); }

    else
      {
	m_type = TokenType::FUNCTION;
        m_env.declare(ident->value()->s, TokenType::FUNCTION, eval(*value));
      }
    
    return eval(*value);
  }

  TokenValue Evaluator::eval_ident(TreeNode const& treenode)
  {
    std::string name = treenode.value()->s;
    
    auto value = m_env.get_value(name);

    if (!value) { throw std::runtime_error("Ident " + name + " not declared."); }

    m_type = *m_env.get_type(name);
    
    return *value;
  }

  TokenValue Evaluator::eval_assign(TreeNode const& treenode)
  {
    std::string name = treenode.child(0).value()->s;
    auto value = eval(treenode.child(1));
    
    m_env.set_value(name, value);
    
    return value;
  }

  TokenValue Evaluator::eval_block(TreeNode const& treenode)
  {
    TokenValue val;

    m_env.open_block();
    for(size_t i=0; i<treenode.size() ; i++)
      {
	val = eval(treenode.child(i));
      }
    
    m_env.close_block();
    
    return val;
  }

  TokenValue Evaluator::eval_function(TreeNode const& treenode)
  {
    TokenValue val;
    val.function = std::make_shared<Function>(treenode, m_env);
    m_type = TokenType::FUNCTION;
    return val;
  }

  TokenValue Evaluator::eval_funcall(TreeNode const& treenode)
  {
    std::string fct_name = treenode.child(0).value()->s;
    TokenValue val;

    std::shared_ptr<Function> fct = m_env.get_value(fct_name)->function;
    
    std::vector<FunctionArg> args;
    
    for(size_t i=1; i<treenode.size(); i++)
      {
	FunctionParam param = fct->param(i - 1);

	FunctionArg arg;
	arg.name = param.name;

	arg.pass_by = param.pass_by;

	if (arg.pass_by == FunctionParamType::VALUE)
	  {
	    arg.value = eval(treenode.child(i));
	    arg.type = m_type;
	  }
	else
	  {
	    arg.type = TokenType::STRING;
	    TokenValue val;
	    val.s = treenode.child(i).value()->s;
	    arg.value = val;
	  }
	
        args.push_back(arg);
      }

    // Function call
    TokenValue res;

    m_env.open_block();
    
    for(auto const& arg : args)
      {
	m_env.declare(arg.name,
		      arg.type,
		      arg.value,
		      (arg.pass_by == FunctionParamType::REF) ||
		      (arg.pass_by == FunctionParamType::CREF),
		      (arg.pass_by == FunctionParamType::CREF));
      }

    int err_code = 0;
    bool has_error = false;
    
    try
      {
	bool executed = false;
	
	if (fct->has_body())
	  {
	    val = eval(fct->body());
	    executed = true;
	  }

	if (!executed)
	  {
	    val = token_value(0);
	  }
      }
    catch(std::pair<bool, TokenValue> v)
      {
	if (v.first)
	  // error
	  {
	    m_errors[fct_name] = true;
	    err_code = v.second.i;
	    has_error = true;
	  }
	else
	  // no error
	  {
	    m_errors[fct_name] = false;
	    val = v.second;
	  }

      }

    if (fct->has_callback())
      {
	val = fct->callback();
      }
    
    m_env.close_block();

    if (has_error)
      {
	TokenValue val;
	val.i = err_code;
	m_env.declare("_error_id", TokenType::INT, val);
      }
    
    return val;
  }

  TokenValue Evaluator::eval_return(TreeNode const& treenode)
  {
    throw std::make_pair(false, eval(treenode.child(0)));
  }

  TokenValue Evaluator::eval_if(TreeNode const& treenode)
  {
    TokenValue cond_val;
    
    if (treenode.child(0).type() == TreeNodeType::ERROR)
      {
	std::string fname = treenode.child(0).child(0).value()->s;
	auto itr = m_errors.find(fname);
	
	if (itr == m_errors.end())
	  {
	    cond_val.b = true;
	  }
	else
	  {
	    cond_val.b = itr->second;
	  }
      }
    else
      {
	cond_val = eval(treenode.child(0));
      }
    
    if (cond_val.b)
      {
	return eval(treenode.child(1));
      }
    else if (treenode.size() > 2)
      {
	return eval(treenode.child(2));
      }
    else
      {
	TokenValue val;
	val.b = false;
	return val;
      }

}
  
  TokenValue Evaluator::eval_while(TreeNode const& treenode)
  {
    TokenValue val;
    
    while (true)
      {
	TokenValue cond_val = eval(treenode.child(0));

	if (cond_val.b)
	  {
	    val = eval(treenode.child(1));
	  }
	else
	  {
	    break;
	  }
      }

    return val;
  }

  TokenValue Evaluator::eval_error(TreeNode const& treenode)
  {
    TokenValue value = eval(treenode.child(0));
    throw std::make_pair(true, value);
  }

}
