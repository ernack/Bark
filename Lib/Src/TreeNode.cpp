#include <cassert>
#include <sstream>
#include <iomanip>
#include "TreeNode.hpp"
#include "Token.hpp"
#include "TokenValue.hpp"

namespace bk
{
  /*explicit*/ TreeNode::TreeNode(TreeNodeType type,
				  std::unique_ptr<TokenValue> value /* = nullptr */)
    : m_type { type }
    , m_value { std::move(value) }
  {
  }

  TreeNode::TreeNode(TreeNode const& treenode)
  {
    m_type = treenode.m_type;

    if (treenode.m_value)
      {
	m_value = std::make_unique<TokenValue>();
	*m_value = *treenode.m_value;
      }
    
    for (auto const& node : treenode.m_children)
      {
    	m_children.push_back(std::make_unique<TreeNode>(*node));
      }
  }
  
  TreeNodeType TreeNode::type() const
  {
    return m_type;
  }

  void TreeNode::set_type(TreeNodeType type)
  {
    m_type = type;
  }
  
  std::string TreeNode::string() const
  {
    std::string res = tree_node_type_to_string(m_type);

    if (!m_children.empty())
      {
	res += "(" + m_children.at(0)->string();
      }

    for (size_t i=1; i<m_children.size(); i++)
      {
	res += "," + m_children.at(i)->string();
      }

    if (m_value)
      {
    	res += "(";
	
    	switch(m_type)
    	  {
    	  case TreeNodeType::INT: res += std::to_string(m_value->i); break;
    	  case TreeNodeType::UINT: res += std::to_string(m_value->ui) + "u"; break;
	    
    	  case TreeNodeType::FLOAT:
	    {
	      std::stringstream ss;
	      ss << std::setprecision(4) << m_value->f;
	      res += ss.str();
	    }
	    break;

	  case TreeNodeType::BOOL:
	    {
	      std::stringstream ss;
	      ss << std::boolalpha << m_value->b;
	      res += ss.str();
	    }
	    break;

	  case TreeNodeType::IDENT:
	    {
	      std::stringstream ss;
	      ss << m_value->s;
	      res += ss.str();
	    }
	    break;

	  case TreeNodeType::TYPE:
	    {
	      std::stringstream ss;
	      ss << m_value->s;
	      res += ss.str();
	    }
	    break;


    	  case TreeNodeType::STRING: res += m_value->s; break;
    	  default: break;
    	  }
	
    	res += ")";
      }
    
    if (!m_children.empty())
      {
	res += ")";
      }
    
    return res;
  }

  void TreeNode::add_child(std::unique_ptr<TreeNode> node)
  {
    m_children.push_back(std::move(node));
  }

  TreeNode const& TreeNode::child(size_t index) const
  {
    if ( index >= m_children.size() )
      {
	throw std::runtime_error("Index error");
      }
    
    return *m_children.at(index);
  }

  std::unique_ptr<TreeNode> TreeNode::move_child(size_t index)
  {
    auto child = std::move(m_children.at(index));
    m_children.erase(m_children.begin() + index);
    return child;
  }

  size_t TreeNode::size() const
  {
    return m_children.size();
  }

  TokenValue const* TreeNode::value() const
  {
    return m_value.get();
  }
  
  /*virtual*/ TreeNode::~TreeNode()
  {
  }
}
