#ifndef TOKENVALUE_HPP
#define TOKENVALUE_HPP

#include <string>
#include <memory>

namespace bk
{
  class Function;
  
  struct TokenValue
  {
    int i = 0;
    unsigned int ui = 0;
    float f = 0.0f;
    std::string s = "";
    bool b = false;
    std::shared_ptr<Function> function;
  };

  TokenValue token_value(int val);
  TokenValue token_value(unsigned int val);
  TokenValue token_value(float val);
  TokenValue token_value(std::string const& val);
  TokenValue token_value(bool val);
}
#endif // TOKENVALUE

