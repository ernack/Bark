#include <iostream>
#include <cassert>
#include "Lexer.hpp"
#include "Token.hpp"

namespace bk
{
  /*explicit*/ Lexer::Lexer()
  {
  }
  
  std::vector<Token> Lexer::operator()(std::string const& source)
  {
    m_source = source;
    m_cursor = 0;
    
    std::vector<Token> tokens;
    
    while (m_cursor < m_source.size())
      {

        if ( match_separator() )
	  {
	    continue;
	  }
	
	if ( match_keyword(";;") )
	  {
	    while (m_cursor < m_source.size() &&
		   m_source.at(m_cursor) != '\n')
	      {
		m_cursor++;
	      }
	    
	    continue;
	  }
	
	auto int_value = match_int();
	if (int_value)
	  {
	    Token token;
	    token.set_type(TokenType::INT);
	    token.set_int_value(*int_value);
	    tokens.push_back(token);
	    continue;
	  }
	
	auto uint_value = match_uint();
    
	if (uint_value)
	  {
	    Token token;
	    token.set_type(TokenType::UINT);
	    token.set_uint_value(*uint_value);
	    tokens.push_back(token);
	    continue;
	  }

	auto float_value = match_float();
	
	if (float_value)
	  {
	    Token token;
	    token.set_type(TokenType::FLOAT);
	    token.set_float_value(*float_value);
	    tokens.push_back(token);
	    continue;
	  }

	auto string_value = match_string();
    
	if (string_value)
	  {
	    Token token;
	    token.set_type(TokenType::STRING);
	    token.set_string_value(*string_value);
	    tokens.push_back(token);
	    continue;
	  }
	if (match_keyword("if"))
	  {
	    Token token;
	    token.set_type(TokenType::IF);
	    tokens.push_back(token);
	    continue;
	  }
	if (match_keyword(":="))
	  {
	    Token token;
	    token.set_type(TokenType::INFOP);
	    tokens.push_back(token);
	    continue;
	  }
	if (match_keyword("else"))
	  {
	    Token token;
	    token.set_type(TokenType::ELSE);
	    tokens.push_back(token);
	    continue;
	  }
	if (match_keyword("while"))
	  {
	    Token token;
	    token.set_type(TokenType::WHILE);
	    tokens.push_back(token);
	    continue;
	  }
	if (match_keyword(";"))
	  {
	    Token token;
	    token.set_type(TokenType::SEMICOLON);
	    tokens.push_back(token);
	    continue;
	  }

	if (match_keyword(","))
	  {
	    Token token;
	    token.set_type(TokenType::COMMA);
	    tokens.push_back(token);
	    continue;
	  }
	
	if (match_keyword(":"))
	  {
	    Token token;
	    token.set_type(TokenType::COLON);
	    tokens.push_back(token);

	    auto type = match_type();
	    
	    if (type)
	      {
		Token token;
		token.set_type(TokenType::TYPE);
		token.set_string_value(*type);
		tokens.push_back(token);
	      }
	    
	    continue;
	  }
	if (match_keyword("("))
	  {
	    Token token;
	    token.set_type(TokenType::OPEN_PAR);
	    tokens.push_back(token);
	    continue;
	  }
	
	if (match_keyword("{"))
	  {
	    Token token;
	    token.set_type(TokenType::OPEN_BRACE);
	    tokens.push_back(token);
	    continue;
	  }

	if (match_keyword("}"))
	  {
	    Token token;
	    token.set_type(TokenType::CLOSE_BRACE);
	    tokens.push_back(token);
	    continue;
	  }

	if (match_keyword("<="))
	  {
	    Token token;
	    token.set_type(TokenType::LE);
	    tokens.push_back(token);
	    continue;
	  }
	
	if (match_keyword(">="))
	  {
	    Token token;
	    token.set_type(TokenType::GE);
	    tokens.push_back(token);
	    continue;
	  }

	if (match_keyword("<"))
	  {
	    Token token;
	    token.set_type(TokenType::LT);
	    tokens.push_back(token);
	    continue;
	  }
	
	if (match_keyword(">"))
	  {
	    Token token;
	    token.set_type(TokenType::GT);
	    tokens.push_back(token);
	    continue;
	  }
	
	if (match_keyword("=="))
	  {
	    Token token;
	    token.set_type(TokenType::EQ);
	    tokens.push_back(token);
	    continue;
	  }
	
	if (match_keyword("!="))
	  {
	    Token token;
	    token.set_type(TokenType::NEQ);
	    tokens.push_back(token);
	    continue;
	  }

	if (match_keyword(")"))
	  {
	    Token token;
	    token.set_type(TokenType::CLOSE_PAR);
	    tokens.push_back(token);
	    continue;
	  }

	if (match_keyword("&&"))
	  {
	    Token token;
	    token.set_type(TokenType::AND);
	    tokens.push_back(token);

	    auto type = match_type();
	    
	    if (type)
	      {
		Token token;
		token.set_type(TokenType::TYPE);
		token.set_string_value(*type);
		tokens.push_back(token);
	      }

	    continue;
	  }

	if (match_keyword("||"))
	  {
	    Token token;
	    token.set_type(TokenType::OR);
	    tokens.push_back(token);
	    continue;
	  }
	
	if (match_keyword("!"))
	  {
	    Token token;
	    token.set_type(TokenType::NOT);
	    tokens.push_back(token);
	    continue;
	  }

	if (match_keyword("true"))
	  {
	    Token token;
	    token.set_type(TokenType::BOOL);
	    token.set_bool_value(true);
	    tokens.push_back(token);
	    continue;
	  }
	
	if (match_keyword("false"))
	  {
	    Token token;
	    token.set_type(TokenType::BOOL);
	    token.set_bool_value(false);
	    tokens.push_back(token);
	    continue;
	  }

	if (match_keyword("return"))
	  {
	    Token token;
	    token.set_type(TokenType::RETURN);
	    tokens.push_back(token);
	    continue;
	  }

	if (match_keyword("error"))
	  {
	    Token token;
	    token.set_type(TokenType::ERROR);
	    tokens.push_back(token);
	    continue;
	  }
	
	if (match_keyword("="))
	  {
	    Token token;
	    token.set_type(TokenType::ASSIGN);
	    tokens.push_back(token);
	    continue;
	  }

	if (match_char('+'))
	  {
	    Token token;
	    token.set_type(TokenType::ADD);
	    tokens.push_back(token);
	    continue;
	  }

	if (match_char('-'))
	  {
	    Token token;
	    token.set_type(TokenType::SUB);
	    tokens.push_back(token);
	    continue;
	  }

	if (match_keyword("&"))
	  {
	    Token token;
	    token.set_type(TokenType::REF);
	    tokens.push_back(token);

	    auto type = match_type();
	    
	    if (type)
	      {
		Token token;
		token.set_type(TokenType::TYPE);
		token.set_string_value(*type);
		tokens.push_back(token);
	      }

	    continue;
	  }

	if (match_char('*'))
	  {
	    Token token;
	    token.set_type(TokenType::MUL);
	    tokens.push_back(token);
	    continue;
	  }

	if (match_char('%'))
	  {
	    Token token;
	    token.set_type(TokenType::MOD);
	    tokens.push_back(token);
	    continue;
	  }

	if (match_char('/'))
	  {
	    Token token;
	    token.set_type(TokenType::DIV);
	    tokens.push_back(token);
	    continue;
	  }

	auto type_value = match_type();
	
	if (type_value)
	  {
	    Token token;
	    token.set_type(TokenType::TYPE);
	    token.set_string_value(*type_value);
	    tokens.push_back(token);
	    continue;
	  }
	
	auto ident_value = match_ident();
	if (ident_value)
	  {
	    Token token;
	    token.set_type(TokenType::IDENT);
	    token.set_string_value(*ident_value);
	    tokens.push_back(token);
	    continue;
	  }

	throw std::runtime_error("Invalid token " + std::to_string(m_cursor) + m_source );
      }

    
    return tokens;
  }
  
  /*virtual*/ Lexer::~Lexer()
  {
  }

  bool Lexer::match_separator()
  {
    if (m_cursor >= m_source.size())
      {
	return true;
      }

    char c = m_source.at(m_cursor);
    
    if (c == ' ' || c == '\n' || c == '\t' || c == '\r')
      {
	m_cursor++;
	return true;
      }
    
    return false;
  }

  bool Lexer::match_int_separator()
  {
    if ( match_separator() ) { return true; }
    
    char c = m_source.at(m_cursor);
    
    if (c == '+' || c == '-' ||
	c == '*' || c == '/' || c == '%' ||
	c == '(' || c == ')' ||
	c == '!' || c == '=' ||
	c == '<' || c == '>' ||
	c == '!' || c == ';' ||
	c == '}' || c == ',' ||
	c == '{')
      {
	return true;
      }
    
    return false;
  }

  bool Lexer::match_keyword(std::string keyword)
  {
    size_t saved = m_cursor;
    
    for (auto const& c : keyword)
      {
	if (!match_char(c))
	  {
	    m_cursor = saved;
	    return false;
	  }
      }

    return true;
  }
  
  bool Lexer::match_char(char c)
  {
    if (m_cursor >= m_source.size()) { return false; }
    
    if (m_source.at(m_cursor) == c)
      {
	m_cursor++;
	return true;
      }

    return false;
  }

  std::unique_ptr<char> Lexer::match_alpha()
  {
    char c = m_source.at(m_cursor);
    
    if ( (c >= 'a' && c <= 'z') ||
	 (c >= 'A' && c <= 'Z') )
      {
	return std::make_unique<char>(c);
      }

    return nullptr;
  }
  
  std::unique_ptr<int> Lexer::match_digit()
  {
    if (m_cursor >= m_source.size()) { return nullptr; }

    size_t saved = m_cursor;
    
    if (m_source.at(m_cursor) >= '0' &&
	m_source.at(m_cursor) <= '9')
      {
	m_cursor++;
	return std::make_unique<int>(m_source.at(m_cursor - 1) - '0');
      }

    m_cursor = saved;
    return nullptr;
  }

  std::unique_ptr<int> Lexer::match_int()
  {    
    if (m_cursor >= m_source.size()) { return nullptr; }
    
    std::string number = "";
    std::unique_ptr<int> digit;    
    size_t saved = m_cursor;
    
    if ( match_char('-') ) { number = "-"; }
    
    while ( (digit = match_digit()) )
      {
  	number += std::to_string(*digit);
      }

    if (number == "-" || number.empty() || !match_int_separator()) { m_cursor = saved; return nullptr; }

    return std::make_unique<int>(std::stoi(number));
  }

  std::unique_ptr<unsigned int> Lexer::match_uint()
  {
    if (m_cursor >= m_source.size()) { return nullptr; }
    
    std::string number = "";

    std::unique_ptr<int> digit;
    size_t saved = m_cursor;

    while ( (digit = match_digit()) )
      {
	number += std::to_string(*digit);
      }

    if ( !match_char('u') || number.empty()  || !match_int_separator() )
      {
	m_cursor = saved;
	return nullptr;
      }

    return std::make_unique<unsigned int>( std::stoi(number) );
  }

  std::unique_ptr<float> Lexer::match_float()
  {
    std::string number = "";
    size_t saved = m_cursor;
    
    std::unique_ptr<int> digit;

    if (match_char('-'))
      {
	number = "-";
      }

    while ( (digit = match_digit()) )
      {
	number += std::to_string(*digit);
      }

    if (number.empty() || !match_char('.'))
      {
	m_cursor = saved;
	return nullptr;
      }
    
    number += '.';
    
    while ( (digit = match_digit()) )
      {
	number += std::to_string(*digit);
      }

    if (number.empty() || !match_int_separator())
      {
	m_cursor = saved;
	return nullptr;
      }

    return std::make_unique<float>(std::stof(number));
  }

  std::unique_ptr<std::string> Lexer::match_string()
  {
    if (m_cursor >= m_source.size()) { return nullptr; }

    std::string value = "";
    size_t saved = m_cursor;
    
    if (!match_char('\"')) { m_cursor = saved; return nullptr; }

    char c;
    while ( m_cursor < m_source.size() &&
	    (c = m_source.at(m_cursor)) &&
	    c != '\"')
      {
	value += c;
	m_cursor++;
      }

    if (m_cursor >= m_source.size()) { return nullptr; }
    
    if (!match_char('\"')) { m_cursor = saved; return nullptr; }
    
    return std::make_unique<std::string>(value);
  }

  bool Lexer::match_ident_char(char c, bool first /* = false */)
  {
    return (c >= 'a' && c <= 'z') ||
      (c >= 'A' && c <= 'Z') ||
      (c == '_') ||
      (!first && c >= '0' && c <= '9');
  }
  
  std::unique_ptr<std::string> Lexer::match_ident()
  {
    std::string identstr = "";

    char c = m_source.at(m_cursor);

    if (match_ident_char(c, true))
      {
	identstr += m_source.at(m_cursor);
	m_cursor++;
      }
    else
      {
	return nullptr;
      }
    
    while ( m_cursor < m_source.size() &&
	    match_ident_char(m_source.at(m_cursor)) )
      {
	identstr += m_source.at(m_cursor);
	m_cursor++;
      }

    if ( identstr.empty() )
      {
	return nullptr;
      }
    
    return std::make_unique<std::string>(identstr);
  }
  
  std::unique_ptr<std::string> Lexer::match_type()
  {
    if (match_keyword("int")) { return std::make_unique<std::string>("int"); }
    if (match_keyword("uint")) { return std::make_unique<std::string>("uint"); }
    if (match_keyword("float")) { return std::make_unique<std::string>("float"); }
    if (match_keyword("string")) { return std::make_unique<std::string>("string"); }
    if (match_keyword("bool")) { return std::make_unique<std::string>("bool"); }

    size_t saved = m_cursor;
    while(match_separator()) {}
    std::string type = "";
    
    if (!match_keyword("(")) { m_cursor = saved; return nullptr; }
    type += "(";

    if (match_keyword("&&")) { type += "&&"; }
    else if (match_keyword("&")) { type += "&"; }

    auto t = match_type();

    if (t)
      {
	type += *t;
      }

    while ( match_keyword(",") )
      {
	std::string ref = "";
	
	if (match_keyword("&&")) { ref = "&&"; }
	else if (match_keyword("&")) { ref = "&"; }

	auto ty = match_type();
	
	assert(ty);
	type += "," + ref + *ty;
      }
    
    if (!match_keyword(")")) { m_cursor = saved; return nullptr; }
    type += ")";

    auto t1 = match_type();

    if (t1)
      {
	type += *t1;
      }
    
    return std::make_unique<std::string>(type);
  }
}
