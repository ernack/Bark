;;;;;;;;;;
;; BARK ;;
;;;;;;;;;;

(setq bark-highlights
      '(
	(";;.*$" . font-lock-warning-face)
	("\\(if\\|return\\|_error_id\\|error\\|else\\)" . font-lock-keyword-face)
	("\\(^\\|[ \t\n]+\\)_[_a-zA-Z]+" . font-lock-builtin-face)
	("[ \n\t,(){]\\(int\\|uint\\|float\\|string\\|bool\\)[}]?" . font-lock-type-face)
	("\".+\"" . font-lock-string-face)
	))

(define-derived-mode bark-mode prog-mode
  "Bark"
  (setq font-lock-defaults '(bark-highlights))
  (use-local-map orkam-mode-map)
  )

(add-to-list 'auto-mode-alist '("\\.bk" . bark-mode))

(provide 'bark-mode)
