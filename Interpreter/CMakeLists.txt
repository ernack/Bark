cmake_minimum_required(VERSION 3.5)

project(BarkInterpreter)

add_executable(barki Src/interpreter.cpp)

if(MINGW)
  target_link_libraries(barki "${CMAKE_BINARY_DIR}/Lib/libbark.dll")
else(MINGW)
  target_link_libraries(barki "${CMAKE_BINARY_DIR}/Lib/libbark.so")
endif(MINGW)

install(TARGETS barki)


project(BarkIRInterpreter)

add_executable(barkir Src/irinterpreter.cpp)

if(MINGW)
  target_link_libraries(barkir "${CMAKE_BINARY_DIR}/Lib/libbark.dll")
else(MINGW)
  target_link_libraries(barkir "${CMAKE_BINARY_DIR}/Lib/libbark.so")
endif(MINGW)

install(TARGETS barkir)


