#include <sstream>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include "../../Lib/Src/Lexer.hpp"
#include "../../Lib/Src/Parser.hpp"
#include "../../Lib/Src/Natives/NativeEnvironment.hpp"
#include "../../Lib/Src/Evaluator/Evaluator.hpp"
#include "../../Lib/Src/TreeNode.hpp"
#include "../../Lib/Src/TypeController/TypeController.hpp"

int main(int argc, char** argv)
{
  std::istream* stream = &std::cin;
  std::ifstream file;
  
  if (argc > 1)
    {
      file = std::ifstream(argv[1]);
      stream = &file;
    }

  std::string source;
  std::string line;
  
  while ( std::getline(*stream, line) )
    {
      source += line;

      if (!stream->eof()) { source += '\n'; }
    }
  
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  bk::NativeEnvironment env;
  bk::NativeEnvironment tc_env;
  bk::TypeController type_controller { tc_env };
  bk::Evaluator eval { env };

  std::unique_ptr<bk::TreeNode> root = parser(source);

  bool error = !type_controller.check(*root);

  if (error)
    {
      std::cerr << "Type Error." << std::endl;
      std::cerr << type_controller.message() << std::endl;
      return -1;
    }
  
  bk::TokenValue val = eval(*root);
  
  return 0;	
}
