#include <sstream>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include "../../Lib/Src/Lexer.hpp"
#include "../../Lib/Src/Parser.hpp"
#include "../../Lib/Src/Natives/NativeEnvironment.hpp"
#include "../../Lib/Src/Evaluator/IREvaluator.hpp"
#include "../../Lib/Src/IRCompiler/IRCompiler.hpp"
#include "../../Lib/Src/TreeNode.hpp"
#include "../../Lib/Src/TypeController/TypeController.hpp"

int main(int argc, char** argv)
{
  std::istream* stream = &std::cin;
  std::ifstream file;
  
  if (argc > 1)
    {
      file = std::ifstream(argv[1]);
      stream = &file;
    }

  std::string source;
  std::string line;
  
  while ( std::getline(*stream, line) )
    {
      source += line;

      if (!stream->eof()) { source += '\n'; }
    }
  
  bk::Lexer lexer;
  bk::Parser parser { lexer };
  bk::NativeEnvironment env;
  bk::NativeEnvironment tc_env;
  bk::TypeController type_controller { tc_env };
  bk::IRCompiler compiler;
  bk::IREvaluator eval { env };

  std::unique_ptr<bk::TreeNode> root = parser(source);

  bool error = !type_controller.check(*root);

  if (error)
    {
      std::cerr << "Type Error." << std::endl;
      std::cerr << type_controller.message() << std::endl;
      return -1;
    }

  bk::TokenValue val = eval(*root); 

  switch(eval.type())
    {
    case bk::TokenType::INT: std::cout << val.i << std::endl;; break;
    case bk::TokenType::UINT: std::cout << val.ui << std::endl;; break;
    case bk::TokenType::FLOAT: std::cout << val.f << std::endl;; break;
    case bk::TokenType::BOOL: std::cout << val.b << std::endl;; break;
    case bk::TokenType::STRING: std::cout << val.s << std::endl;; break;
    default: std::cout << "?" << std::endl;
    }
  
  return 0;	
}
